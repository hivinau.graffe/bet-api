<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;

$entityManager = require __DIR__ . DIRECTORY_SEPARATOR . 'doctrine' . DIRECTORY_SEPARATOR . 'bootstrap.php';

return ConsoleRunner::createHelperSet($entityManager);
