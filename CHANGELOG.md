# Change Log
All notable changes to this project will be documented in this file.

## [WIP]

## [1.8.0]

 ### Added
 - Handle error message #663131a



## [1.7.2]



## [1.7.1]

 ### Fix
 - Replace `UuidV4Generator` by `UuidGenerator` #73b65f0



## [1.7.0]

 ### Added
 - Enable ssl #062192f
 - Add `home` page #b68914b

### Fix
 - [Misc] - Fix namespace convention #df936ad

### Changed
 - Remove APCu usage #a719676



## [1.6.1]

 ### Fix
 - Check colors count before converting int to hex #d2a3f48



## [1.6.0]

 ### Added
 - Extract colors from image when uploading #3c1412d
 - List team colors #fd0e12f
 - Extract colors from image when uploading #6843292



## [1.5.1]

 ### Added
 - Add validators tests #193dc8c

### Fix
 - Add `type` when adding new pro #0998a77

### Changed
 - Add `teams` when adding new `match` #846ca8f
 - Add `pros` when adding new `match` #5e0f5af
 - Add `pros` when adding new `match` #c0fd200
 - Add `matches` when adding new `bet` #f6bd18f
 - [Misc] - Add `Changed` keyword #6210d13



## [1.5.0]

 ### Added
 - Setup route for adding a new goal and listing goals #88e72c7
 - Add APC cache monitoring #52a1870
 - Add `type` property #5e12120
 - Add `type` property #81a4c43

### Fix
 - Add getters on interface #70d69e8
 - [Misc] - Fix CI issues when installing dependencies #7c84b6b



## [1.4.0]

 ### Added
 - Add `pros_count` property #1131e12
 - Add `matches_count` property #6087f59



## [1.3.2]

 ### Fix
 - Replace `Ba` by `BA` #20040dc
 - Replace `Ba` by `BA` #f7b4f44



## [1.3.1]

 ### Fix
 - Replace `Ba` by `BA` #e2679a6



## [1.3.0]

 ### Added
 - Add route to register a player #3b3a7a2
 - Add route for player authentication #1e1d54b

### Fix
 - Add bcmath extension #8f8386a
 - Add bcmath extension #d68c34e
 - Add a response content-type header with `text/plain` #2eceb86
 - Add `won` property for Pro #b5bedf4



## [1.2.0]

 ### Added
 - Add tests for matches and pros routes #b6f6d93
 - Add routes for matches and pros #0081266

### Fix
 - Use Apcu for cache when environment is in production #2321d0e



## [1.1.1]

 ### Added
 - Setup route for downloading an image #70f0a7c
 - Setup route for uploading a new image #c960a7f

### Fix
 - Set minimum length validation to 3 characters at least #58c2627
 - Reinit minor or patch to zero #012a4d4

### Removed
 - [Misc] - Add `Removed` keyword #08da04c
 - Remove `release` phony #02afa35



## [1.1.0]

 ### Added
 - Setup route for adding a new team #cc27668

### Fix
 - Remove error and access #128384d



## [1.0.7]

 ### Added
 - Add job to bump version (major, minor, patch) #71ff13e



## [1.0.6]



## [1.0.5]

 ### Fix
 - Change permission access private key #16c55a9



## [1.0.4]

 ### Fix
 - Use cache globally #7e3b22a
 - Use cache instead of artifacts #343c148
 - Set xdebug mode to coverage #7fb216b
 - Fix loading key #ead65b9



## [1.0.3]

 ### Added
 - Setup deployment #065cc17



## [1.0.2]

 ### Added
 - Add project version in composer.json #f7931f9

### Fix
 - Remove temporary files #a403ab1



## [1.0.1]

### Added
 - Setup release candidate #2d62e69
 - Setup CI-CD for gitlab #64fa9b4

### Fix
 - Add changes before commit #7205509
 - Setup release candidate #dc397ae
 - Distinguish mysql config for amd64 and armv7 #28dedef
 - Change phpunit output #e47bc0c
 - Get path for environment file #0b34adb
 - Set path for nginx logs #e3218cf

