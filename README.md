<h1 align="center">
Bet Api
</h1>

[![pipeline status](https://gitlab.com/hivinau.graffe/bet-api/badges/develop/pipeline.svg)](https://gitlab.com/hivinau.graffe/bet-api/-/commits/develop)
[![coverage report](https://gitlab.com/hivinau.graffe/bet-api/badges/develop/coverage.svg)](https://gitlab.com/hivinau.graffe/bet-api/-/commits/develop)

# Contents

[[_TOC_]]

# Features

## List bets

You can find the dedicated documentation here: [/bets](docs/list-bets.md).

## Add bet

You can find the dedicated documentation here: [/bets/add](docs/add-bet.md).

## Add match

You can find the dedicated documentation here: [/matches/add](docs/add-match.md).

## Add pro

You can find the dedicated documentation here: [/pros/add](docs/add-pro.md).

# Supported OS & SDK version

- PhpStorm 2018.2
- PHP 7.3
- Composer 1.9.3
- Docker 19.03.5

# How to ?

__Docker must be running.__

Install Dependencies:
```bash
make install
```

Update dependencies:
```bash
make update
```

Prepare database:
```bash
make database
```

Start server on [address](http://localhost:8000):
```bash
make up
```

Abort server:
```bash
make down
```

Launch unit tests:
```bash
make test
```

# About

## Author

Hivinau GRAFFE [hivinau.graffe@hotmail.fr](mailto:hivinau.graffe@hotmail.fr)

## License

This project is released under the MIT license. [See LICENSE](LICENSE) for details.
