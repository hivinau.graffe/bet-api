# MARK: - Shortcuts

dc := docker-compose -f docker-compose.yml
dc-dev := docker-compose -f docker-compose.dev.yml
dc-armv7 := docker-compose -f docker-compose.armv7.yml
php := $(dc) exec php
php-dev := $(dc-dev) exec php
php-armv7 := $(dc-armv7) exec php
doctrine := $(php) vendor/bin/doctrine
doctrine-dev := $(php-dev) vendor/bin/doctrine
doctrine-armv7 := $(php-armv7) vendor/bin/doctrine
phpunit := $(php) vendor/bin/phpunit
phpunit-dev := $(php-dev) vendor/bin/phpunit
phpunit-armv7 := $(php-armv7) vendor/bin/phpunit
sh :=  $(php) sh

# MARK: - Commands

.PHONY: install update up down database test install-armv7 update-armv7 up-armv7 down-armv7 database-armv7 test-armv7

# MARK: - Definitions

install:
	$(php) composer install --no-interaction
	$(php) composer dumpautoload -o

install-dev:
	$(php-dev) composer install --no-interaction
	$(php-dev) composer dumpautoload -o

install-armv7:
	$(php-armv7) composer install --no-interaction
	$(php-armv7) composer dumpautoload -o

update:
	$(php) composer update --no-interaction
	$(php) composer dumpautoload -o

update-dev:
	$(php-dev) composer update --no-interaction
	$(php-dev) composer dumpautoload -o

update-armv7:
	$(php-armv7) composer update --no-interaction
	$(php-armv7) composer dumpautoload -o

up:
	$(dc) up -d --build

up-dev:
	$(dc-dev) up -d --build

up-armv7:
	$(dc-armv7) up -d --build

database:
	$(doctrine) orm:clear-cache:metadata
	$(doctrine) orm:schema-tool:update --force
	$(doctrine) orm:validate-schema
	$(doctrine) orm:generate-proxies

database-dev:
	$(doctrine-dev) orm:clear-cache:metadata
	$(doctrine-dev) orm:schema-tool:update --force
	$(doctrine-dev) orm:validate-schema
	$(doctrine-dev) orm:generate-proxies

database-armv7:
	$(doctrine-armv7) orm:clear-cache:metadata
	$(doctrine-armv7) orm:schema-tool:update --force
	$(doctrine-armv7) orm:validate-schema
	$(doctrine-armv7) orm:generate-proxies

down:
	$(dc) down

down-dev:
	$(dc-dev) down

down-armv7:
	$(dc-armv7) down

test:
	$(phpunit) --coverage-html build --colors=never

test-dev:
	$(phpunit-dev) --coverage-html build --colors=never

test-armv7:
	$(phpunit-armv7) --coverage-html build --colors=never



