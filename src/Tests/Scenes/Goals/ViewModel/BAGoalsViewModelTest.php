<?php

namespace Tests\Scenes\Goals\ViewModel;

use App\Foundation\Components\EntityManager\GoalsEntityManager\GoalsEntityManager;
use App\Foundation\Components\EntityManager\MatchesEntityManager\MatchesEntityManager;
use App\Foundation\Components\EntityManager\TeamsEntityManager\TeamsEntityManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Scenes\Goals\ViewModel\BAGoalsViewModel;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\GoalMock;
use Tests\Mocks\Models\MatchMock;
use Tests\Mocks\Models\TeamMock;

final class BAGoalsViewModelTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAGoalsViewModel
     */
    private $tested;

    /**
     * @var MockObject|GoalsEntityManager
     */
    private $goalsEntityManager;

    /**
     * @var MockObject|TeamsEntityManager
     */
    private $teamsEntityManager;

    /**
     * @var MockObject|MatchesEntityManager
     */
    private $matchesEntityManager;

    /**
     * @var MockObject|ValidatorManager
     */
    private $validatorManager;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->goalsEntityManager = $this->getMockBuilder(GoalsEntityManager::class)->getMock();
        $this->teamsEntityManager = $this->getMockBuilder(TeamsEntityManager::class)->getMock();
        $this->matchesEntityManager = $this->getMockBuilder(MatchesEntityManager::class)->getMock();
        $this->validatorManager = $this->getMockBuilder(ValidatorManager::class)->getMock();
        $this->tested = new BAGoalsViewModel(
            $this->goalsEntityManager,
            $this->teamsEntityManager,
            $this->matchesEntityManager,
            $this->validatorManager);
    }

    // MARK: - Tests

    public function testGetMatchGoals_shouldReturnEntityManagerFindMatchGoalsResults_whenMatchIdIsNotNullAndValidatorManagerValidateReturnsTrue(): void
    {
        $expected = [new GoalMock()];
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->goalsEntityManager
            ->method('findMatchGoals')
            ->willReturn($expected);

        $this->assertEquals($expected, $this->tested->getMatchGoals('match_id'));
    }

    public function testGetMatchGoals_shouldReturnAEmptyArray_whenMatchIdIsNotNullAndValidatorManagerValidateReturnsFalse(): void
    {
        $this->validatorManager
            ->method('validate')
            ->willReturn(false);

        $this->assertEquals([], $this->tested->getMatchGoals('match_id'));
    }

    public function testGetMatchGoals_shouldReturnAEmptyArray_whenMatchIdIsNotNull(): void
    {
        $this->assertEquals([], $this->tested->getMatchGoals(null));
    }

    public function testEntityManagerGetMatchById_shouldBeCalled_whenAddGoalMatchIdAndValidatorManagerValidateReturnsTrue(): void
    {
        $matchId = 'matchId';
        $teamId = 'teamId';
        $scorer = 'scorer';
        $time = 0;
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->teamsEntityManager
            ->method('getTeamById')
            ->willReturn(new TeamMock());
        $this->matchesEntityManager
            ->expects($this->once())
            ->method('getMatchById')
            ->willReturn(new MatchMock());

        $this->tested->addGoalMatchId($matchId, $teamId, $scorer, $time);
    }

    public function testAddGoalMatchId_shouldReturnNull_whenEntityManagerGetTeamByIdReturnsNull(): void
    {
        $matchId = 'matchId';
        $teamId = 'teamId';
        $scorer = 'scorer';
        $time = 0;
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->teamsEntityManager
            ->method('getTeamById')
            ->willReturn(null);
        $this->matchesEntityManager
            ->expects($this->once())
            ->method('getMatchById')
            ->willReturn(new MatchMock());

        $this->assertNull($this->tested->addGoalMatchId($matchId, $teamId, $scorer, $time));
    }

    public function testAddGoalMatchId_shouldReturnNull_whenEntityManagerGetMatchByIdReturnsNull(): void
    {
        $matchId = 'matchId';
        $teamId = 'teamId';
        $scorer = 'scorer';
        $time = 0;
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->teamsEntityManager
            ->method('getTeamById')
            ->willReturn(new TeamMock());
        $this->matchesEntityManager
            ->expects($this->once())
            ->method('getMatchById')
            ->willReturn(null);

        $this->assertNull($this->tested->addGoalMatchId($matchId, $teamId, $scorer, $time));
    }

    public function testEntityManagerGetTeamById_shouldBeCalled_whenAddGoalMatchIdAndValidatorManagerValidateReturnsTrue(): void
    {
        $matchId = 'matchId';
        $teamId = 'teamId';
        $scorer = 'scorer';
        $time = 0;
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->matchesEntityManager
            ->method('getMatchById')
            ->willReturn(new MatchMock());
        $this->teamsEntityManager
            ->expects($this->once())
            ->method('getTeamById')
            ->willReturn(new TeamMock());

        $this->tested->addGoalMatchId($matchId, $teamId, $scorer, $time);
    }

    public function testEntityManagerAddGoal_shouldBeCalled_whenAddGoalMatchIdAndValidatorManagerValidateReturnsTrue(): void
    {
        $matchId = 'matchId';
        $teamId = 'teamId';
        $scorer = 'scorer';
        $time = 0;
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->matchesEntityManager
            ->method('getMatchById')
            ->willReturn(new MatchMock());
        $this->teamsEntityManager
            ->method('getTeamById')
            ->willReturn(new TeamMock());
        $this->goalsEntityManager
            ->expects($this->once())
            ->method('addGoal');

        $this->tested->addGoalMatchId($matchId, $teamId, $scorer, $time);
    }

    public function testAddGoalMatchId_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndMatchIdIsNull(): void
    {
        $matchId = null;
        $teamId = 'teamId';
        $scorer = 'scorer';
        $time = 0;
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->addGoalMatchId($matchId, $teamId, $scorer, $time));
    }

    public function testAddGoalMatchId_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndTeamIdIsNull(): void
    {
        $matchId = 'matchId';
        $teamId = null;
        $scorer = 'scorer';
        $time = 0;
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->addGoalMatchId($matchId, $teamId, $scorer, $time));
    }

    public function testAddGoalMatchId_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndScorerIsNull(): void
    {
        $matchId = 'matchId';
        $teamId = 'teamId';
        $scorer = null;
        $time = 0;
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->addGoalMatchId($matchId, $teamId, $scorer, $time));
    }

    public function testAddGoalMatchId_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndTimeIsNull(): void
    {
        $matchId = 'matchId';
        $teamId = 'teamId';
        $scorer = 'scorer';
        $time = null;
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->addGoalMatchId($matchId, $teamId, $scorer, $time));
    }

    public function testAddGoalMatchId_shouldReturnNull_whenValidatorManagerValidateReturnsFalse(): void
    {
        $matchId = 'matchId';
        $teamId = 'teamId';
        $scorer = 'scorer';
        $time = 0;
        $this->validatorManager
            ->method('validate')
            ->willReturn(false);

        $this->assertNull($this->tested->addGoalMatchId($matchId, $teamId, $scorer, $time));
    }
}