<?php

namespace Tests\Scenes\Goals;

use App\Foundation\Routers\Router;
use App\Scenes\Goals\BAGoalsCoordinator;
use App\Scenes\Goals\ViewModel\GoalsViewModel;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\RouteInterface;
use Tests\Mocks\Models\GoalMock;

final class BAGoalsCoordinatorTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAGoalsCoordinator
     */
    private $tested;

    /**
     * @var MockObject|GoalsViewModel
     */
    private $goalsViewModel;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->goalsViewModel = $this->getMockBuilder(GoalsViewModel::class)->getMock();
        $this->tested = new BAGoalsCoordinator($this->goalsViewModel);
    }

    // MARK: - Tests

    public function testRouterGet_shouldBeCalledOnce_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('get');

        $this->tested->startRouter($router);
    }

    public function testRouterPost_shouldBeCalledOnce_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('post');

        $this->tested->startRouter($router);
    }

    public function testListMatchGoals_shouldReturnResponseWithBodyContainingBetsViewModelGetMatchGoalsResult(): void
    {
        $goals = [
            new GoalMock(),
            new GoalMock(),
            new GoalMock()
        ];
        $this->goalsViewModel
            ->method('getMatchGoals')
            ->willReturn($goals);
        /** @var MockObject|RouteInterface $route */
        $route = $this->getMockBuilder(RouteInterface::class)->getMock();
        $route
            ->method('getArguments')
            ->willReturn([
                'match' => 'match_id'
            ]);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getAttribute')
            ->willReturn($route);

        $response = $this->tested->listMatchGoals($request);
        $this->assertEquals(json_encode($goals), (string) $response->getBody());
        $this->assertEquals(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
    }

    public function testListMatchGoals_shouldReturnResponseWithStatusCode404_whenBetsViewModelGetMatchGoalsReturnsEmptyResult(): void
    {
        $goals = [];
        $this->goalsViewModel
            ->method('getMatchGoals')
            ->willReturn($goals);
        /** @var MockObject|RouteInterface $route */
        $route = $this->getMockBuilder(RouteInterface::class)->getMock();
        $route
            ->method('getArguments')
            ->willReturn([
                'match' => 'match_id'
            ]);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getAttribute')
            ->willReturn($route);

        $response = $this->tested->listMatchGoals($request);
        $this->assertEquals(StatusCodeInterface::STATUS_NOT_FOUND, $response->getStatusCode());
    }

    public function testAddGoalFromRequest_shouldReturnResponseWithBodyContainingBetsViewModelAddGoalMatchIdResult(): void
    {
        $goal = new GoalMock();
        $this->goalsViewModel
            ->method('addGoalMatchId')
            ->willReturn($goal);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getParsedBody')
            ->willReturn([
                'match' => 'match_id',
                'team' => 'team_id',
                'scorer' => 'scorer',
                'time' => 0
            ]);

        $response = $this->tested->addGoal($request);
        $this->assertEquals(json_encode($goal), (string) $response->getBody());
        $this->assertEquals(StatusCodeInterface::STATUS_CREATED, $response->getStatusCode());
    }

    public function testAddGoal_shouldReturnResponseWithStatusCode406_whenBetsViewModelAddGoalMatchIdReturnsNull(): void
    {
        $this->goalsViewModel
            ->method('addGoalMatchId')
            ->willReturn(null);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getParsedBody')
            ->willReturn([
                'match' => 'match_id',
                'team' => 'team_id',
                'scorer' => 'scorer',
                'time' => 0
            ]);

        $response = $this->tested->addGoal($request);
        $this->assertEquals(StatusCodeInterface::STATUS_NOT_ACCEPTABLE, $response->getStatusCode());
    }
}