<?php

namespace Tests\Scenes\Auth\ViewModel;

use App\Foundation\Components\CertificateInspector\CertificateInspector;
use App\Foundation\Components\TokenGenerator\TokenGenerator;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Scenes\Auth\ViewModel\BAAuthViewModel;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class BAAuthViewModelTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAAuthViewModel
     */
    private $tested;

    /**
     * @var MockObject|ValidatorManager
     */
    private $validatorManager;

    /**
     * @var MockObject|CertificateInspector
     */
    private $certificateInspector;

    /**
     * @var MockObject|TokenGenerator
     */
    private $tokenGenerator;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->validatorManager = $this->getMockBuilder(ValidatorManager::class)->getMock();
        $this->certificateInspector = $this->getMockBuilder(CertificateInspector::class)->getMock();
        $this->tokenGenerator = $this->getMockBuilder(TokenGenerator::class)->getMock();
        $this->tested = new BAAuthViewModel($this->validatorManager, $this->certificateInspector, $this->tokenGenerator);
    }

    // MARK: - Tests

    public function testCertificateInspectorVerifyAppleSignatureUrl_shouldBeCalled_whenAuthenticateUrlCalledAndValidatorManagerValidateReturnsTrue(): void
    {
        $url = 'url';
        $signature = 'signature';
        $salt = 'salt';
        $timestamp = 'timestamp';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->certificateInspector
            ->expects($this->once())
            ->method('verifyAppleSignatureUrl');

        $this->tested->authenticateUrl($url, $signature, $salt, $timestamp);
    }

    public function testAuthenticateUrl_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndUrlIsNull(): void
    {
        $url = null;
        $signature = 'signature';
        $salt = 'salt';
        $timestamp = 'timestamp';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->authenticateUrl($url, $signature, $salt, $timestamp));
    }

    public function testAuthenticateUrl_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndSignatureIsNull(): void
    {
        $url = 'url';
        $signature = null;
        $salt = 'salt';
        $timestamp = 'timestamp';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->authenticateUrl($url, $signature, $salt, $timestamp));
    }

    public function testAuthenticateUrl_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndSaltIsNull(): void
    {
        $url = 'url';
        $signature = 'signature';
        $salt = null;
        $timestamp = 'timestamp';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->authenticateUrl($url, $signature, $salt, $timestamp));
    }

    public function testAuthenticateUrl_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndTimestampIsNull(): void
    {
        $url = 'url';
        $signature = 'signature';
        $salt = 'salt';
        $timestamp = null;
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->authenticateUrl($url, $signature, $salt, $timestamp));
    }

    public function testAuthenticateUrl_shouldReturnNull_whenValidatorManagerValidateReturnsFalse(): void
    {
        $url = 'url';
        $signature = 'signature';
        $salt = 'salt';
        $timestamp = 'timestamp';
        $this->validatorManager
            ->method('validate')
            ->willReturn(false);

        $this->assertNull($this->tested->authenticateUrl($url, $signature, $salt, $timestamp));
    }
}