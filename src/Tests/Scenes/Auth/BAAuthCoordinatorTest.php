<?php

namespace Tests\Scenes\Auth;

use App\Foundation\Routers\Router;
use App\Scenes\Auth\BAAuthCoordinator;
use App\Scenes\Auth\ViewModel\AuthViewModel;
use App\Scenes\Players\PlayersCoordinator;
use App\Scenes\Players\ViewModel\PlayersViewModel;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Tests\Mocks\Models\PlayerMock;

final class BAAuthCoordinatorTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAAuthCoordinator
     */
    private $tested;

    /**
     * @var MockObject|AuthViewModel
     */
    private $authViewModel;

    /**
     * @var MockObject|PlayersCoordinator
     */
    private $playersCoordinator;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->authViewModel = $this->getMockBuilder(AuthViewModel::class)->getMock();
        $playersViewModel = $this->getMockBuilder(PlayersViewModel::class)->getMock();
        $this->playersCoordinator = $this->getMockBuilder(PlayersCoordinator::class)
            ->setConstructorArgs([$playersViewModel])
            ->getMockForAbstractClass();
        $this->tested = new BAAuthCoordinator($this->authViewModel, $this->playersCoordinator);
    }

    // MARK: - Tests

    public function testRouterPost_shouldBeCalled_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('post');

        $this->tested->startRouter($router);
    }

    public function testAuthenticateIosPlayerFromRequest_shouldReturnResponseWithBodyContainingAuthViewModelAuthenticateUrlResult(): void
    {
        $token = 'token';
        $player = new PlayerMock();
        $response = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $this->authViewModel
            ->method('authenticateUrl')
            ->willReturn($token);
        $response
            ->method('getBody')
            ->willReturn(json_encode($player));
        $this->playersCoordinator
            ->method('addPlayer')
            ->willReturn($response);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getParsedBody')
            ->willReturn([
                'url' => 'url',
                'signature' => 'signature',
                'salt' => 'salt',
                'timestamp' => 'timestamp'
            ]);

        $response = $this->tested->authenticateIosPlayer($request);
        $this->assertEquals(json_encode($player), (string) $response->getBody());
    }

    public function testAuthenticateIosPlayerFromRequest_shouldReturnResponseWithStatusCode406_whenAuthViewModelAuthenticateUrlReturnsNull(): void
    {
        $token = null;
        $this->authViewModel
            ->method('authenticateUrl')
            ->willReturn($token);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getParsedBody')
            ->willReturn([
                'token' => 'token'
            ]);

        $response = $this->tested->authenticateIosPlayer($request);
        $this->assertEquals(StatusCodeInterface::STATUS_NOT_ACCEPTABLE, $response->getStatusCode());
    }
}