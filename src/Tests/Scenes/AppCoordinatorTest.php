<?php

namespace Tests\Scenes;

use App\Foundation\Coordinators\Coordinator;
use App\Foundation\Routers\Router;
use App\Scenes\AppCoordinator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class AppCoordinatorTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var AppCoordinator
     */
    private $tested;

    /**
     * @var MockObject|Coordinator
     */
    private $childCoordinator;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->childCoordinator = $this->getMockBuilder(Coordinator::class)->getMock();
        $this->tested = new AppCoordinator([$this->childCoordinator]);
    }

    // MARK: - Tests

    public function testChildCoordinatorStartRouter_shouldBeCalled_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();

        $this->childCoordinator
            ->expects($this->once())
            ->method('startRouter');

        $this->tested->startRouter($router);
    }
}