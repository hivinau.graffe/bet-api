<?php

namespace Tests\Scenes\Teams\ViewModel;

use App\Foundation\Components\EntityManager\BetsEntityManager\BetsEntityManager;
use App\Foundation\Components\EntityManager\TeamsEntityManager\TeamsEntityManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Scenes\Teams\ViewModel\BATeamsViewModel;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class BATeamsViewModelTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BATeamsViewModel
     */
    private $tested;

    /**
     * @var MockObject|TeamsEntityManager
     */
    private $entityManager;

    /**
     * @var MockObject|ValidatorManager
     */
    private $validatorManager;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->getMockBuilder(TeamsEntityManager::class)->getMock();
        $this->validatorManager = $this->getMockBuilder(ValidatorManager::class)->getMock();
        $this->tested = new BATeamsViewModel($this->entityManager, $this->validatorManager);
    }

    // MARK: - Tests

    public function testEntityManagerAddTeam_shouldBeCalled_whenAddTeamNameAndValidatorManagerValidateReturnsTrue(): void
    {
        $name = 'name';
        $image = 'image';
        $backgroundColor = 'backgroundColor';
        $primaryColor = 'primaryColor';
        $secondaryColor = 'secondaryColor';
        $detailColor = 'detailColor';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->entityManager
            ->expects($this->once())
            ->method('addTeam');

        $this->tested->addTeamName($name,
            $image,
            $backgroundColor,
            $primaryColor,
            $secondaryColor,
            $detailColor);
    }

    public function testAddTeamName_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndNameIsNull(): void
    {
        $name = null;
        $image = 'image';
        $backgroundColor = 'backgroundColor';
        $primaryColor = 'primaryColor';
        $secondaryColor = 'secondaryColor';
        $detailColor = 'detailColor';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->addTeamName($name,
            $image,
            $backgroundColor,
            $primaryColor,
            $secondaryColor,
            $detailColor));
    }

    public function testAddTeamName_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndBackgroundColorIsNull(): void
    {
        $name = 'name';
        $image = 'image';
        $backgroundColor = null;
        $primaryColor = 'primaryColor';
        $secondaryColor = 'secondaryColor';
        $detailColor = 'detailColor';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->addTeamName($name,
            $image,
            $backgroundColor,
            $primaryColor,
            $secondaryColor,
            $detailColor));
    }

    public function testAddTeamName_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndPrimaryColorIsNull(): void
    {
        $name = 'name';
        $image = 'image';
        $backgroundColor = 'backgroundColor';
        $primaryColor = null;
        $secondaryColor = 'secondaryColor';
        $detailColor = 'detailColor';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->addTeamName($name,
            $image,
            $backgroundColor,
            $primaryColor,
            $secondaryColor,
            $detailColor));
    }

    public function testAddTeamName_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndSecondaryColorIsNull(): void
    {
        $name = 'name';
        $image = 'image';
        $backgroundColor = 'backgroundColor';
        $primaryColor = 'primaryColor';
        $secondaryColor = null;
        $detailColor = 'detailColor';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->addTeamName($name,
            $image,
            $backgroundColor,
            $primaryColor,
            $secondaryColor,
            $detailColor));
    }

    public function testAddTeamName_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndDetailColorIsNull(): void
    {
        $name = 'name';
        $image = 'image';
        $backgroundColor = 'backgroundColor';
        $primaryColor = 'primaryColor';
        $secondaryColor = 'secondaryColor';
        $detailColor = null;
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->addTeamName($name,
            $image,
            $backgroundColor,
            $primaryColor,
            $secondaryColor,
            $detailColor));
    }

    public function testAddTeamName_shouldReturnNull_whenValidatorManagerValidateReturnsFalse(): void
    {
        $name = 'name';
        $image = 'image';
        $backgroundColor = 'backgroundColor';
        $primaryColor = 'primaryColor';
        $secondaryColor = 'secondaryColor';
        $detailColor = 'detailColor';
        $this->validatorManager
            ->method('validate')
            ->willReturn(false);

        $this->assertNull($this->tested->addTeamName($name,
            $image,
            $backgroundColor,
            $primaryColor,
            $secondaryColor,
            $detailColor));
    }
}