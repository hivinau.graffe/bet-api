<?php

namespace Tests\Scenes\Teams;

use App\Foundation\Routers\Router;
use App\Scenes\Teams\BATeamsCoordinator;
use App\Scenes\Teams\ViewModel\TeamsViewModel;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Tests\Mocks\Models\TeamMock;

final class BATeamsCoordinatorTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BATeamsCoordinator
     */
    private $tested;

    /**
     * @var MockObject|TeamsViewModel
     */
    private $teamsViewModel;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->teamsViewModel = $this->getMockBuilder(TeamsViewModel::class)->getMock();
        $this->tested = new BATeamsCoordinator($this->teamsViewModel);
    }

    // MARK: - Tests

    public function testRouterGet_shouldNeverBeCalled_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->never())
            ->method('get');

        $this->tested->startRouter($router);
    }

    public function testRouterPost_shouldBeCalledOnce_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('post');

        $this->tested->startRouter($router);
    }

    public function testAddTeamFromRequest_shouldReturnResponseWithBodyContainingBetsViewModelAddTeamNameResult(): void
    {
        $team = new TeamMock();
        $this->teamsViewModel
            ->method('addTeamName')
            ->willReturn($team);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getParsedBody')
            ->willReturn([
                'name' => 'name',
                'image' => 'image'
            ]);

        $response = $this->tested->addTeam($request);
        $this->assertEquals(json_encode($team), (string) $response->getBody());
        $this->assertEquals(StatusCodeInterface::STATUS_CREATED, $response->getStatusCode());
    }

    public function testAddTeam_shouldReturnResponseWithStatusCode406_whenBetsViewModelAddTeamNameReturnsNull(): void
    {
        $this->teamsViewModel
            ->method('addTeamName')
            ->willReturn(null);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getParsedBody')
            ->willReturn([
                'name' => 'name',
                'image' => 'image'
            ]);

        $response = $this->tested->addTeam($request);
        $this->assertEquals(StatusCodeInterface::STATUS_NOT_ACCEPTABLE, $response->getStatusCode());
    }
}