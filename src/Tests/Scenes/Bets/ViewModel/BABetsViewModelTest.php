<?php

namespace Tests\Scenes\Bets\ViewModel;

use App\Foundation\Components\EntityManager\BetsEntityManager\BetsEntityManager;
use App\Foundation\Components\EntityManager\MatchesEntityManager\MatchesEntityManager;
use App\Foundation\Components\StringExploder\StringExploder;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Scenes\Bets\ViewModel\BABetsViewModel;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\BetMock;
use Tests\Mocks\Models\MatchMock;

final class BABetsViewModelTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BABetsViewModel
     */
    private $tested;

    /**
     * @var MockObject|BetsEntityManager
     */
    private $betsEntityManager;

    /**
     * @var MockObject|MatchesEntityManager
     */
    private $matchesEntityManager;
    /**
     * @var MockObject|ValidatorManager
     */
    private $validatorManager;

    /**
     * @var MockObject|StringExploder
     */
    private $stringExploder;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->betsEntityManager = $this->getMockBuilder(BetsEntityManager::class)->getMock();
        $this->matchesEntityManager = $this->getMockBuilder(MatchesEntityManager::class)->getMock();
        $this->validatorManager = $this->getMockBuilder(ValidatorManager::class)->getMock();
        $this->stringExploder = $this->getMockBuilder(StringExploder::class)->getMock();
        $this->tested = new BABetsViewModel(
            $this->betsEntityManager,
            $this->matchesEntityManager,
            $this->validatorManager,
            $this->stringExploder);
    }

    // MARK: - Tests

    public function testGetBets_shouldReturnEntityManagerFindBetsResult(): void
    {
        $expected = [new BetMock()];
        $this->betsEntityManager
            ->method('findBets')
            ->willReturn($expected);

        $this->assertEquals($expected, $this->tested->getBets());
    }

    public function testAddBetMatches_shouldReturnEntityManagerAddBetResult_whenMatchIdIsValid(): void
    {
        $expected = new BetMock();
        $this->betsEntityManager
            ->method('addBet')
            ->willReturn($expected);
        $this->matchesEntityManager
            ->method('getMatchesByIds')
            ->willReturn([new MatchMock()]);
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->stringExploder
            ->method('explode')
            ->willReturn(['matchId']);

        $this->assertEquals($expected, $this->tested->addBetMatches('match_id'));
    }

    public function testAddBetMatches_shouldReturnNull_whenMatchIdIsNull(): void
    {
        $this->assertNull($this->tested->addBetMatches(null));
    }

    public function testAddBetMatches_shouldReturnNull_whenStringExploderExplodeReturnsEmptyArray(): void
    {
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->stringExploder
            ->method('explode')
            ->willReturn([]);

        $this->assertNull($this->tested->addBetMatches('match_id'));
    }

    public function testAddBetMatches_shouldReturnNull_whenValidatorManagerValidateReturnsFalseOnInvalidMatchesIdChecking(): void
    {
        $this->validatorManager
            ->method('validate')
            ->willReturnOnConsecutiveCalls(true, false);
        $this->stringExploder
            ->method('explode')
            ->willReturn(['match_id']);

        $this->assertNull($this->tested->addBetMatches('match_id'));
    }

    public function testAddBetMatches_shouldReturnNull_whenMatchIdIsValidAndValidatorManagerValidateReturnsFalse(): void
    {
        $this->validatorManager
            ->method('validate')
            ->willReturn(false);

        $this->assertNull($this->tested->addBetMatches('match_id'));
    }
}