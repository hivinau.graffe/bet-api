<?php

namespace Tests\Scenes\Bets;

use App\Foundation\Routers\Router;
use App\Scenes\Bets\BABetsCoordinator;
use App\Scenes\Bets\ViewModel\BetsViewModel;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Tests\Mocks\Models\BetMock;

final class BABetsCoordinatorTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BABetsCoordinator
     */
    private $tested;

    /**
     * @var MockObject|BetsViewModel
     */
    private $betsViewModel;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->betsViewModel = $this->getMockBuilder(BetsViewModel::class)->getMock();
        $this->tested = new BABetsCoordinator($this->betsViewModel);
    }

    // MARK: - Tests

    public function testRouterGet_shouldBeCalledOnce_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('get');

        $this->tested->startRouter($router);
    }

    public function testRouterPost_shouldBeCalledOnce_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('post');

        $this->tested->startRouter($router);
    }

    public function testListBets_shouldReturnResponseWithBodyContainingBetsViewModelGetBetsResult(): void
    {
        $bets = [
            new BetMock(),
            new BetMock(),
            new BetMock()
        ];
        $this->betsViewModel
            ->method('getBets')
            ->willReturn($bets);

        $response = $this->tested->listBets();
        $this->assertEquals(json_encode($bets), (string) $response->getBody());
        $this->assertEquals(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
    }

    public function testListBets_shouldReturnResponseWithStatusCode404_whenBetsViewModelGetBetsReturnsEmptyResult(): void
    {
        $bets = [];
        $this->betsViewModel
            ->method('getBets')
            ->willReturn($bets);

        $response = $this->tested->listBets();
        $this->assertEquals(StatusCodeInterface::STATUS_NOT_FOUND, $response->getStatusCode());
    }

    public function testAddBetFromRequest_shouldReturnResponseWithBodyContainingBetsViewModelAddBetMatchesResult(): void
    {
        $bet = new BetMock();
        $this->betsViewModel
            ->method('addBetMatches')
            ->willReturn($bet);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getParsedBody')
            ->willReturn([
                'match' => 'match'
            ]);

        $response = $this->tested->addBet($request);
        $this->assertEquals(json_encode($bet), (string) $response->getBody());
        $this->assertEquals(StatusCodeInterface::STATUS_CREATED, $response->getStatusCode());
    }

    public function testAddBetFromRequest_shouldReturnResponseWithStatusCode406_whenBetsViewModelAddBetMatchesReturnsNull(): void
    {
        $this->betsViewModel
            ->method('addBetMatches')
            ->willReturn(null);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getParsedBody')
            ->willReturn([
                'match' => 'match'
            ]);

        $response = $this->tested->addBet($request);
        $this->assertEquals(StatusCodeInterface::STATUS_NOT_ACCEPTABLE, $response->getStatusCode());
    }
}