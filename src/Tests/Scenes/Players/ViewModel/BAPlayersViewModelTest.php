<?php

namespace Tests\Scenes\Players\ViewModel;

use App\Foundation\Components\EntityManager\PlayersEntityManager\PlayersEntityManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Scenes\Players\ViewModel\BAPlayersViewModel;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class BAPlayersViewModelTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAPlayersViewModel
     */
    private $tested;

    /**
     * @var MockObject|PlayersEntityManager
     */
    private $entityManager;

    /**
     * @var MockObject|ValidatorManager
     */
    private $validatorManager;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->getMockBuilder(PlayersEntityManager::class)->getMock();
        $this->validatorManager = $this->getMockBuilder(ValidatorManager::class)->getMock();
        $this->tested = new BAPlayersViewModel($this->entityManager, $this->validatorManager);
    }

    // MARK: - Tests

    public function testEntityManagerAddPlayer_shouldBeCalled_whenAddPlayerTokenCalledAndValidatorManagerValidateReturnsTrue(): void
    {
        $token = 'token';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->entityManager
            ->expects($this->once())
            ->method('addPlayer');

        $this->tested->addPlayerToken($token);
    }

    public function testAddPlayerToken_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndTokenIsNull(): void
    {
        $token = null;
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->addPlayerToken($token));
    }

    public function testAddPlayerToken_shouldReturnNull_whenValidatorManagerValidateReturnsFalse(): void
    {
        $token = 'token';
        $this->validatorManager
            ->method('validate')
            ->willReturn(false);

        $this->assertNull($this->tested->addPlayerToken($token));
    }
}