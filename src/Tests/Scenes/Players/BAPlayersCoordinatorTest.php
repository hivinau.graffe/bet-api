<?php

namespace Tests\Scenes\Players;

use App\Foundation\Routers\Router;
use App\Scenes\Players\BAPlayersCoordinator;
use App\Scenes\Players\ViewModel\PlayersViewModel;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Tests\Mocks\Models\PlayerMock;

final class BAPlayersCoordinatorTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAPlayersCoordinator
     */
    private $tested;

    /**
     * @var MockObject|PlayersViewModel
     */
    private $playersViewModel;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->playersViewModel = $this->getMockBuilder(PlayersViewModel::class)->getMock();
        $this->tested = new BAPlayersCoordinator($this->playersViewModel);
    }

    // MARK: - Tests

    public function testRouterPost_shouldBeCalled_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('post');

        $this->tested->startRouter($router);
    }

    public function testAddTeamFromRequest_shouldReturnResponseWithBodyContainingBetsViewModelAddTeamNameResult(): void
    {
        $player = new PlayerMock();
        $this->playersViewModel
            ->method('addPlayerToken')
            ->willReturn($player);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getParsedBody')
            ->willReturn([
                'token' => 'token'
            ]);

        $response = $this->tested->addPlayer($request);
        $this->assertEquals(json_encode($player), (string) $response->getBody());
        $this->assertEquals(StatusCodeInterface::STATUS_CREATED, $response->getStatusCode());
    }

    public function testAddTeam_shouldReturnResponseWithStatusCode406_whenBetsViewModelAddTeamNameReturnsNull(): void
    {
        $this->playersViewModel
            ->method('addPlayerToken')
            ->willReturn(null);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getParsedBody')
            ->willReturn([
                'token' => 'token'
            ]);

        $response = $this->tested->addPlayer($request);
        $this->assertEquals(StatusCodeInterface::STATUS_NOT_ACCEPTABLE, $response->getStatusCode());
    }
}