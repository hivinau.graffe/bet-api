<?php

namespace Tests\Scenes\Matches;

use App\Foundation\Routers\Router;
use App\Scenes\Matches\BAMatchesCoordinator;
use App\Scenes\Matches\ViewModel\MatchesViewModel;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\RouteInterface;
use Tests\Mocks\Models\MatchMock;

final class BAMatchesCoordinatorTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAMatchesCoordinator
     */
    private $tested;

    /**
     * @var MockObject|MatchesViewModel
     */
    private $matchesViewModel;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->matchesViewModel = $this->getMockBuilder(MatchesViewModel::class)->getMock();
        $this->tested = new BAMatchesCoordinator($this->matchesViewModel);
    }

    // MARK: - Tests

    public function testRouterGet_shouldBeCalledOnce_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('get');

        $this->tested->startRouter($router);
    }

    public function testRouterPost_shouldBeCalledOnce_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('post');

        $this->tested->startRouter($router);
    }

    public function testListBetMatches_shouldReturnResponseWithBodyContainingBetsViewModelGetBetMatchesResult(): void
    {
        $matches = [
            new MatchMock(),
            new MatchMock(),
            new MatchMock()
        ];
        $this->matchesViewModel
            ->method('getBetMatches')
            ->willReturn($matches);
        /** @var MockObject|RouteInterface $route */
        $route = $this->getMockBuilder(RouteInterface::class)->getMock();
        $route
            ->method('getArguments')
            ->willReturn([
                'bet' => 'bet_id'
            ]);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getAttribute')
            ->willReturn($route);

        $response = $this->tested->listBetMatches($request);
        $this->assertEquals(json_encode($matches), (string) $response->getBody());
        $this->assertEquals(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
    }

    public function testListBetMatches_shouldReturnResponseWithStatusCode404_whenBetsViewModelGetBetMatchesReturnsEmptyResult(): void
    {
        $matches = [];
        $this->matchesViewModel
            ->method('getBetMatches')
            ->willReturn($matches);
        /** @var MockObject|RouteInterface $route */
        $route = $this->getMockBuilder(RouteInterface::class)->getMock();
        $route
            ->method('getArguments')
            ->willReturn([
                'bet' => 'bet_id'
            ]);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getAttribute')
            ->willReturn($route);

        $response = $this->tested->listBetMatches($request);
        $this->assertEquals(StatusCodeInterface::STATUS_NOT_FOUND, $response->getStatusCode());
    }

    public function testAddMatchFromRequest_shouldReturnResponseWithBodyContainingBetsViewModelAddMatchTeamsResult(): void
    {
        $match = new MatchMock();
        $this->matchesViewModel
            ->method('addMatchTeams')
            ->willReturn($match);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getParsedBody')
            ->willReturn([
                'pros' => 'pros',
                'date' => 'date',
                'time' => 'time'
            ]);

        $response = $this->tested->addMatch($request);
        $this->assertEquals(json_encode($match), (string) $response->getBody());
        $this->assertEquals(StatusCodeInterface::STATUS_CREATED, $response->getStatusCode());
    }

    public function testAddMatch_shouldReturnResponseWithStatusCode406_whenBetsViewModelAddMatchTeamsReturnsNull(): void
    {
        $this->matchesViewModel
            ->method('addMatchTeams')
            ->willReturn(null);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getParsedBody')
            ->willReturn([
                'pros' => 'pros',
                'date' => 'date',
                'time' => 'time'
            ]);

        $response = $this->tested->addMatch($request);
        $this->assertEquals(StatusCodeInterface::STATUS_NOT_ACCEPTABLE, $response->getStatusCode());
    }
}