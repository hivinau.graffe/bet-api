<?php

namespace Tests\Scenes\Matches\ViewModel;

use App\Foundation\Components\DatetimeManager\DatetimeManager;
use App\Foundation\Components\EntityManager\MatchesEntityManager\MatchesEntityManager;
use App\Foundation\Components\EntityManager\ProsEntityManager\ProsEntityManager;
use App\Foundation\Components\EntityManager\TeamsEntityManager\TeamsEntityManager;
use App\Foundation\Components\StringExploder\StringExploder;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Scenes\Matches\ViewModel\BAMatchesViewModel;
use DateTime;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\MatchMock;
use Tests\Mocks\Models\ProMock;
use Tests\Mocks\Models\TeamMock;

final class BAMatchesViewModelTest extends TestCase
{

    // MARK: - Private properties

    /**
     * @var BAMatchesViewModel
     */
    private $tested;

    /**
     * @var MockObject|MatchesEntityManager
     */
    private $matchesEntityManager;

    /**
     * @var MockObject|TeamsEntityManager
     */
    private $teamsEntityManager;

    /**
     * @var MockObject|ProsEntityManager
     */
    private $prosEntityManager;

    /**
     * @var MockObject|ValidatorManager
     */
    private $validatorManager;

    /**
     * @var MockObject|DatetimeManager
     */
    private $datetimeManager;

    /**
     * @var MockObject|StringExploder
     */
    private $stringExploder;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->matchesEntityManager = $this->getMockBuilder(MatchesEntityManager::class)->getMock();
        $this->teamsEntityManager = $this->getMockBuilder(TeamsEntityManager::class)->getMock();
        $this->prosEntityManager = $this->getMockBuilder(ProsEntityManager::class)->getMock();
        $this->validatorManager = $this->getMockBuilder(ValidatorManager::class)->getMock();
        $this->datetimeManager = $this->getMockBuilder(DatetimeManager::class)->getMock();
        $datetime = new DateTime();
        $this->datetimeManager
            ->method('createDate')
            ->willReturn($datetime);
        $this->datetimeManager
            ->method('createTime')
            ->willReturn($datetime);
        $this->stringExploder = $this->getMockBuilder(StringExploder::class)->getMock();
        $this->tested = new BAMatchesViewModel(
            $this->matchesEntityManager,
            $this->teamsEntityManager,
            $this->prosEntityManager,
            $this->validatorManager,
            $this->datetimeManager,
            $this->stringExploder);
    }

    // MARK: - Tests

    public function testGetBetMatches_shouldReturnEntityManagerFindBetMatchesResults_whenBetIdIsNotNullAndValidatorManagerValidateReturnsTrue(): void
    {
        $expected = [new MatchMock()];
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->matchesEntityManager
            ->method('findBetMatches')
            ->willReturn($expected);

        $this->assertEquals($expected, $this->tested->getBetMatches('bet_id'));
    }

    public function testGetBetMatches_shouldReturnAEmptyArray_whenBetIdIsNotNullAndValidatorManagerValidateReturnsFalse(): void
    {
        $this->validatorManager
            ->method('validate')
            ->willReturn(false);

        $this->assertEquals([], $this->tested->getBetMatches('bet_id'));
    }

    public function testGetBetMatches_shouldReturnAEmptyArray_whenBetIdIsNotNull(): void
    {
        $this->assertEquals([], $this->tested->getBetMatches(null));
    }

    public function testAddBetPros_shouldReturnNull_whenStringExploderExplodeReturnsEmptyArray(): void
    {
        $teams = 'team_id1, team_id_2';
        $pros = 'pro_id1, pro_id_2';
        $date = 'date';
        $time = 'time';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->stringExploder
            ->method('explode')
            ->willReturn([]);

        $this->assertNull($this->tested->addMatchTeams($teams, $pros, $date, $time));
    }

    public function testEntityManagerAddMatch_shouldBeCalled_whenAddMatchProsAndValidatorManagerValidateReturnsTrue(): void
    {
        $teams = 'team_id1, team_id_2';
        $pros = 'pro_id1, pro_id_2';
        $date = 'date';
        $time = 'time';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->matchesEntityManager
            ->expects($this->once())
            ->method('addMatch');
        $this->teamsEntityManager
            ->method('getTeamsByIds')
            ->willReturn([new TeamMock()]);
        $this->prosEntityManager
            ->method('getProsByIds')
            ->willReturn([new ProMock()]);
        $this->stringExploder
            ->method('explode')
            ->willReturnOnConsecutiveCalls(['team_id1, team_id_2'], ['pro_id1, pro_id_2']);

        $this->tested->addMatchTeams($teams, $pros, $date, $time);
    }

    public function testAddMatchPros_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndTeamsIsNull(): void
    {
        $teams = null;
        $pros = 'pro_id1, pro_id_2';
        $date = 'date';
        $time = 'time';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->stringExploder
            ->method('explode')
            ->willReturnOnConsecutiveCalls(['team_id1, team_id_2'], ['pro_id1, pro_id_2']);

        $this->assertNull($this->tested->addMatchTeams($teams, $pros, $date, $time));
    }

    public function testAddMatchPros_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndProsIsNull(): void
    {
        $teams = 'team_id1, team_id_2';
        $pros = null;
        $date = 'date';
        $time = 'time';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->stringExploder
            ->method('explode')
            ->willReturnOnConsecutiveCalls(['team_id1, team_id_2'], ['pro_id1, pro_id_2']);

        $this->assertNull($this->tested->addMatchTeams($teams, $pros, $date, $time));
    }

    public function testAddMatchPros_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndDateIsNull(): void
    {
        $teams = 'team_id1, team_id_2';
        $pros = 'pro_id1, pro_id_2';
        $date = null;
        $time = 'time';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->stringExploder
            ->method('explode')
            ->willReturnOnConsecutiveCalls(['team_id1, team_id_2'], ['pro_id1, pro_id_2']);

        $this->assertNull($this->tested->addMatchTeams($teams, $pros, $date, $time));
    }

    public function testAddMatchPros_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndTimeIsNull(): void
    {
        $teams = 'team_id1, team_id_2';
        $pros = 'pro_id1, pro_id_2';
        $date = 'date';
        $time = null;
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->stringExploder
            ->method('explode')
            ->willReturnOnConsecutiveCalls(['team_id1, team_id_2'], ['pro_id1, pro_id_2']);

        $this->assertNull($this->tested->addMatchTeams($teams, $pros, $date, $time));
    }

    public function testAddMatchPros_shouldReturnNull_whenValidatorManagerValidateReturnsFalse(): void
    {
        $teams = 'team_id1, team_id_2';
        $pros = 'pro_id1, pro_id_2';
        $date = 'date';
        $time = 'time';
        $this->validatorManager
            ->method('validate')
            ->willReturn(false);
        $this->stringExploder
            ->method('explode')
            ->willReturnOnConsecutiveCalls(['team_id1, team_id_2'], ['pro_id1, pro_id_2']);

        $this->assertNull($this->tested->addMatchTeams($teams, $pros, $date, $time));
    }
}