<?php

namespace Tests\Scenes\Home\ViewModel;

use App\Foundation\Components\HtmlRenderer\HtmlRenderer;
use App\Scenes\Home\ViewModel\BAHomeViewModel;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

final class BAHomeViewModelTest extends TestCase {

    // MARK: - Private properties

    private BAHomeViewModel $tested;

    /**
     * @var MockObject|HtmlRenderer
     */
    private $htmlRenderer;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->htmlRenderer = $this->getMockBuilder(HtmlRenderer::class)->getMock();
        $this->tested = new BAHomeViewModel($this->htmlRenderer);
    }

    // MARK: - Tests

    public function testRenderHomePage_shouldReturnHtmlRendererRenderResponseResult(): void
    {
        /** @var ResponseInterface $expected */
        $expected = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $this->htmlRenderer
            ->method('renderResponse')
            ->willReturn($expected);

        $this->assertEquals($expected, $this->tested->renderHomePage());
    }
}