<?php

namespace Tests\Scenes\Home;

use App\Foundation\Routers\Router;
use App\Scenes\Home\BAHomeCoordinator;
use App\Scenes\Home\ViewModel\HomeViewModel;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

final class BAHomeCoordinatorTest extends TestCase {

    // MARK: - Private properties

    private BAHomeCoordinator $tested;

    /**
     * @var MockObject|HomeViewModel
     */
    private $homeViewModel;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->homeViewModel = $this->getMockBuilder(HomeViewModel::class)->getMock();
        $this->tested = new BAHomeCoordinator($this->homeViewModel);
    }

    // MARK: - Tests

    public function testRouterGet_shouldBeCalledOnce_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('get');

        $this->tested->startRouter($router);
    }

    public function testIndex_shouldReturnHomeViewModelRenderHomePageResult(): void
    {
        /** @var ResponseInterface $expected */
        $expected = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $this->homeViewModel
            ->method('renderHomePage')
            ->willReturn($expected);

        $this->assertEquals($expected, $this->tested->index());
    }
}