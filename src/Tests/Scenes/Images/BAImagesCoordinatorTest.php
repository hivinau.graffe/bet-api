<?php

namespace Tests\Scenes\Images;

use App\Foundation\Routers\Router;
use App\Scenes\Images\BAImagesCoordinator;
use App\Scenes\Images\ViewModel\ImagesViewModel;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;
use Slim\Interfaces\RouteInterface;
use Slim\Psr7\Response;

final class BAImagesCoordinatorTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAImagesCoordinator
     */
    private $tested;

    /**
     * @var MockObject|ImagesViewModel
     */
    private $imagesViewModel;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->imagesViewModel = $this->getMockBuilder(ImagesViewModel::class)->getMock();
        $this->tested = new BAImagesCoordinator($this->imagesViewModel);
    }

    // MARK: - Tests

    public function testRouterGet_shouldBeCalled_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('get');

        $this->tested->startRouter($router);
    }

    public function testRouterPost_shouldBeCalled_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('post');

        $this->tested->startRouter($router);
    }

    public function testUploadImageFromRequest_shouldReturnResponseWithBodyContainingImagesViewModelUploadImageResult(): void
    {
        $filename = 'filename';
        $this->imagesViewModel
            ->method('uploadImage')
            ->willReturn([
                'filename' => $filename,
                'colors' => []
            ]);
        /** @var MockObject|UploadedFileInterface $request */
        $uploadedFile = $this->getMockBuilder(UploadedFileInterface::class)->getMock();
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getUploadedFiles')
            ->willReturn([
                'image' => $uploadedFile
            ]);

        $response = $this->tested->uploadImage($request);
        $json = json_decode((string) $response->getBody());
        $this->assertEquals($filename, $json->{'filename'});
        $this->assertEquals(StatusCodeInterface::STATUS_CREATED, $response->getStatusCode());
    }

    public function testUploadImageFromRequest_shouldReturnResponseWithStatusCode406_whenImagesViewModelUploadImageReturnsEmptyArray(): void
    {
        $this->imagesViewModel
            ->method('uploadImage')
            ->willReturn([]);
        /** @var MockObject|UploadedFileInterface $request */
        $uploadedFile = $this->getMockBuilder(UploadedFileInterface::class)->getMock();
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getUploadedFiles')
            ->willReturn([
                'image' => $uploadedFile
            ]);

        $response = $this->tested->uploadImage($request);
        $this->assertEquals(StatusCodeInterface::STATUS_NOT_ACCEPTABLE, $response->getStatusCode());
    }

    public function testGetImageFromRequest_shouldReturnResponseWithBodyContainingImagesViewModelGetImageResult(): void
    {
        $filename = 'filename';
        $this->imagesViewModel
            ->method('getImage')
            ->willReturn(new Response());
        /** @var MockObject|RouteInterface $route */
        $route = $this->getMockBuilder(RouteInterface::class)->getMock();
        $route
            ->method('getArguments')
            ->willReturn([
                'image' => $filename
            ]);

        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getAttribute')
            ->willReturn($route);
        $request
            ->method('getQueryParams')
            ->willReturn([]);

        $response = $this->tested->getImage($request);
        $this->assertNotNull($response);
        $this->assertEquals(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
    }

    public function testGetImageFromRequest_shouldReturnResponseWithStatusCode404_whenImagesViewModelGetImageReturnsNull(): void
    {
        $filename = 'filename';
        $this->imagesViewModel
            ->method('getImage')
            ->willReturn(null);
        /** @var MockObject|RouteInterface $route */
        $route = $this->getMockBuilder(RouteInterface::class)->getMock();
        $route
            ->method('getArguments')
            ->willReturn([
                'image' => $filename
            ]);

        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getAttribute')
            ->willReturn($route);
        $request
            ->method('getQueryParams')
            ->willReturn([]);

        $response = $this->tested->getImage($request);
        $this->assertNotNull($response);
        $this->assertEquals(StatusCodeInterface::STATUS_NOT_FOUND, $response->getStatusCode());
    }
}