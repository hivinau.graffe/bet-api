<?php

namespace Tests\Scenes\Images;

use App\Foundation\Components\ImagesManager\ImagesManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Scenes\Images\ViewModel\BAImagesViewModel;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\UploadedFileInterface;
use Slim\Psr7\Response;

final class BAImagesViewModelTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAImagesViewModel
     */
    private $tested;

    /**
     * @var MockObject|ImagesManager
     */
    private $imagesManager;

    /**
     * @var MockObject|ValidatorManager
     */
    private $validatorManager;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->imagesManager = $this->getMockBuilder(ImagesManager::class)->getMock();
        $this->validatorManager = $this->getMockBuilder(ValidatorManager::class)->getMock();
        $this->tested = new BAImagesViewModel($this->imagesManager, $this->validatorManager);
    }

    // MARK: - Tests

    public function testImagesManagerUploadImage_shouldBeCalled_whenUploadImageAndValidatorManagerValidateReturnsTrue(): void
    {
        /** @var MockObject|UploadedFileInterface $file */
        $file = $this->getMockBuilder(UploadedFileInterface::class)->getMock();
        $file
            ->method('getClientFilename')
            ->willReturn('filename');
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->imagesManager
            ->expects($this->once())
            ->method('uploadImage');

        $this->tested->uploadImage($file);
    }

    public function testUploadImage_shouldReturnEmptyArray_whenValidatorManagerValidateReturnsTrueAndFileIsNull(): void
    {
        /** @var MockObject|UploadedFileInterface|null $file */
        $file = null;
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertEmpty($this->tested->uploadImage($file));
    }

    public function testUploadImage_shouldReturnEmptyArray_whenValidatorManagerValidateReturnsFalse(): void
    {
        /** @var MockObject|UploadedFileInterface $file */
        $file = $this->getMockBuilder(UploadedFileInterface::class)->getMock();
        $file
            ->method('getClientFilename')
            ->willReturn('filename');
        $this->validatorManager
            ->method('validate')
            ->willReturn(false);

        $this->assertEmpty($this->tested->uploadImage($file));
    }

    public function testGetImage_shouldReturnNull_whenFilenameEqualsNull(): void
    {
        $filename = null;
        $width = -1;
        $height = -1;

        $this->assertNull($this->tested->getImage($filename, $width, $height));
    }

    public function testGetImage_shouldReturnNull_whenImagesManagerGetImageReturnsNull(): void
    {
        $filename = 'filename';
        $width = -1;
        $height = -1;
        $this->imagesManager
            ->method('getImage')
            ->willReturn(null);

        $this->assertNull($this->tested->getImage($filename, $width, $height));
    }

    public function testGetImage_shouldReturnImagesManagerGetImageResult(): void
    {
        $response = new Response();
        $filename = 'filename';
        $width = -1;
        $height = -1;
        $this->imagesManager
            ->method('getImage')
            ->willReturn($response);

        $this->assertEquals($response, $this->tested->getImage($filename, $width, $height));
    }
}