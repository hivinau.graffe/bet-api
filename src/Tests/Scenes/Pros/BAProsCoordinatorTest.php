<?php

namespace Tests\Scenes\Pros;

use App\Foundation\Routers\Router;
use App\Scenes\Pros\BAProsCoordinator;
use App\Scenes\Pros\ViewModel\ProsViewModel;
use Fig\Http\Message\StatusCodeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Interfaces\RouteInterface;
use Tests\Mocks\Models\ProMock;

final class BAProsCoordinatorTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAProsCoordinator
     */
    private $tested;

    /**
     * @var MockObject|ProsViewModel
     */
    private $prosViewModel;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->prosViewModel = $this->getMockBuilder(ProsViewModel::class)->getMock();
        $this->tested = new BAProsCoordinator($this->prosViewModel);
    }

    // MARK: - Tests

    public function testRouterGet_shouldBeCalledOnce_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('get');

        $this->tested->startRouter($router);
    }

    public function testRouterPost_shouldBeCalledOnce_whenStartRouterCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('post');

        $this->tested->startRouter($router);
    }

    public function testListMatchPros_shouldReturnResponseWithBodyContainingBetsViewModelGetMatchProsResult(): void
    {
        $pros = [
            new ProMock(),
            new ProMock(),
            new ProMock()
        ];
        $this->prosViewModel
            ->method('getMatchPros')
            ->willReturn($pros);
        /** @var MockObject|RouteInterface $route */
        $route = $this->getMockBuilder(RouteInterface::class)->getMock();
        $route
            ->method('getArguments')
            ->willReturn([
                'match' => 'match_id'
            ]);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getAttribute')
            ->willReturn($route);

        $response = $this->tested->listMatchPros($request);
        $this->assertEquals(json_encode($pros), (string) $response->getBody());
        $this->assertEquals(StatusCodeInterface::STATUS_OK, $response->getStatusCode());
    }

    public function testListMatchPros_shouldReturnResponseWithStatusCode404_whenBetsViewModelGetMatchProsReturnsEmptyResult(): void
    {
        $pros = [];
        $this->prosViewModel
            ->method('getMatchPros')
            ->willReturn($pros);
        /** @var MockObject|RouteInterface $route */
        $route = $this->getMockBuilder(RouteInterface::class)->getMock();
        $route
            ->method('getArguments')
            ->willReturn([
                'match' => 'match_id'
            ]);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getAttribute')
            ->willReturn($route);

        $response = $this->tested->listMatchPros($request);
        $this->assertEquals(StatusCodeInterface::STATUS_NOT_FOUND, $response->getStatusCode());
    }


    public function testAddProFromRequest_shouldReturnResponseWithBodyContainingBetsViewModelAddProLabelResult(): void
    {
        $pro = new ProMock();
        $this->prosViewModel
            ->method('addProLabel')
            ->willReturn($pro);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getParsedBody')
            ->willReturn([
                'label' => 'label'
            ]);

        $response = $this->tested->addPro($request);
        $this->assertEquals(json_encode($pro), (string) $response->getBody());
        $this->assertEquals(StatusCodeInterface::STATUS_CREATED, $response->getStatusCode());
    }

    public function testAddPro_shouldReturnResponseWithStatusCode406_whenBetsViewModelAddProLabelReturnsNull(): void
    {
        $this->prosViewModel
            ->method('addProLabel')
            ->willReturn(null);
        /** @var MockObject|ServerRequestInterface $request */
        $request = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $request
            ->method('getParsedBody')
            ->willReturn([
                'label' => 'label'
            ]);

        $response = $this->tested->addPro($request);
        $this->assertEquals(StatusCodeInterface::STATUS_NOT_ACCEPTABLE, $response->getStatusCode());
    }
}