<?php

namespace Tests\Scenes\Pros\ViewModel;

use App\Foundation\Components\EntityManager\ProsEntityManager\ProsEntityManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Scenes\Pros\ViewModel\BAProsViewModel;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\ProMock;

final class BAProsViewModelTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAProsViewModel
     */
    private $tested;

    /**
     * @var MockObject|ProsEntityManager
     */
    private $entityManager;

    /**
     * @var MockObject|ValidatorManager
     */
    private $validatorManager;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->getMockBuilder(ProsEntityManager::class)->getMock();
        $this->validatorManager = $this->getMockBuilder(ValidatorManager::class)->getMock();
        $this->tested = new BAProsViewModel($this->entityManager, $this->validatorManager);
    }

    // MARK: - Tests

    public function testGetMatchPros_shouldReturnEntityManagerFindMatchProsResults_whenMatchIdIsNotNullAndValidatorManagerValidateReturnsTrue(): void
    {
        $expected = [new ProMock()];
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->entityManager
            ->method('findMatchPros')
            ->willReturn($expected);

        $this->assertEquals($expected, $this->tested->getMatchPros('match_id'));
    }

    public function testGetMatchPros_shouldReturnAEmptyArray_whenMatchIdIsNotNullAndValidatorManagerValidateReturnsFalse(): void
    {
        $this->validatorManager
            ->method('validate')
            ->willReturn(false);

        $this->assertEquals([], $this->tested->getMatchPros('match_id'));
    }

    public function testGetMatchPros_shouldReturnAEmptyArray_whenMatchIdIsNotNull(): void
    {
        $this->assertEquals([], $this->tested->getMatchPros(null));
    }

    public function testEntityManagerAddPro_shouldBeCalled_whenAddProLabelAndValidatorManagerValidateReturnsTrue(): void
    {
        $label = 'label';
        $type = 'type';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);
        $this->entityManager
            ->expects($this->once())
            ->method('addPro');

        $this->tested->addProLabel($label, $type);
    }

    public function testAddProLabel_shouldReturnNull_whenValidatorManagerValidateReturnsTrueAndLabelIsNull(): void
    {
        $label = null;
        $type = 'type';
        $this->validatorManager
            ->method('validate')
            ->willReturn(true);

        $this->assertNull($this->tested->addProLabel($label, $type));
    }

    public function testAddProLabel_shouldReturnNull_whenValidatorManagerValidateReturnsFalse(): void
    {
        $label = 'label';
        $type = 'type';
        $this->validatorManager
            ->method('validate')
            ->willReturn(false);

        $this->assertNull($this->tested->addProLabel($label, $type));
    }
}