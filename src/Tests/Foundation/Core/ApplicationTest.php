<?php

namespace Tests\Foundation\Core;

use App\Foundation\Core\Application;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;

final class ApplicationTest extends TestCase {

    // MARK: - Private properties

    private Application $tested;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        /** @var MockObject|ResponseFactoryInterface $responseFactory */
        $responseFactory = $this->getMockBuilder(ResponseFactoryInterface::class)->getMock();
        $this->tested = new Application($responseFactory);
    }

    // MARK: - Tests

    public function testSetContainer_shouldUpdateInternalContainer(): void
    {
        $expectedValue = 1;
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockBuilder(ContainerInterface::class)->getMock();
        $container
            ->method('has')
            ->willReturn(true);
        $container
            ->method('get')
            ->willReturn($expectedValue);
        $this->tested->setContainer($container);

        $this->assertEquals($expectedValue, $this->tested->getContainer()->get('test'));
    }
}