<?php

namespace Tests\Foundation\Core;

use App\Foundation\Core\AppFactory;
use App\Foundation\Core\Application;
use PHPUnit\Framework\TestCase;

final class AppFactoryTest extends TestCase {

    // MARK: - Tests

    public function testAppFactoryCreate_shouldReturnAnInstanceOfApplication(): void
    {
        $this->assertInstanceOf(Application::class, AppFactory::create());
    }
}