<?php

namespace Tests\Foundation\Repositories\ProsRepository;

use App\Foundation\Repositories\ProsRepository\BAProsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\NamingStrategy;
use Doctrine\ORM\ORMException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\ProMock;

final class BAProsRepositoryTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAProsRepository
     */
    private $tested;

    /**
     * @var MockObject|EntityManagerInterface
     */
    private $entityManager;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        /** @var MockObject|NamingStrategy $namingStrategy */
        $namingStrategy = $this->getMockBuilder(NamingStrategy::class)->getMock();
        $classMetadata = new ClassMetadata(ProMock::class, $namingStrategy);
        $this->entityManager
            ->method('getClassMetadata')
            ->willReturn($classMetadata);

        $this->tested = new BAProsRepository($this->entityManager);
    }

    // MARK: - Tests

    public function testEntityManagerPersist_shouldBeCalled_whenAddProCalled(): void
    {
        $this->entityManager
            ->expects($this->once())
            ->method('persist');

        $this->tested->addPro(new ProMock());
    }

    public function testAddPro_shouldReturnNull_whenEntityManagerPersistThrowsAnORMException(): void
    {
        $this->entityManager
            ->method('persist')
            ->will($this->throwException(new ORMException()));

        $this->assertNull($this->tested->addPro(new ProMock()));
    }

    public function testEntityManagerFlush_shouldBeCalled_whenAddProCalled(): void
    {
        $this->entityManager
            ->expects($this->once())
            ->method('flush');

        $this->tested->addPro(new ProMock());
    }

    public function testAddPro_shouldReturnNull_whenEntityManagerFlushThrowsAnORMException(): void
    {
        $this->entityManager
            ->method('flush')
            ->will($this->throwException(new ORMException()));

        $this->assertNull($this->tested->addPro(new ProMock()));
    }
}