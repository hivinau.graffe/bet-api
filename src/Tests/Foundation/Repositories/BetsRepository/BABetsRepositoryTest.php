<?php

namespace Tests\Foundation\Repositories\BetsRepository;

use App\Foundation\Repositories\BetsRepository\BABetsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\NamingStrategy;
use Doctrine\ORM\ORMException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\BetMock;

final class BABetsRepositoryTest extends TestCase
{

    // MARK: - Private properties

    /**
     * @var BABetsRepository
     */
    private $tested;

    /**
     * @var MockObject|EntityManagerInterface
     */
    private $entityManager;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        /** @var MockObject|NamingStrategy $namingStrategy */
        $namingStrategy = $this->getMockBuilder(NamingStrategy::class)->getMock();
        $classMetadata = new ClassMetadata(BetMock::class, $namingStrategy);
        $this->entityManager
            ->method('getClassMetadata')
            ->willReturn($classMetadata);

        $this->tested = new BABetsRepository($this->entityManager);
    }

    // MARK: - Tests

    public function testEntityManagerPersist_shouldBeCalled_whenAddBetCalled(): void
    {
        $this->entityManager
            ->expects($this->once())
            ->method('persist');

        $this->tested->addBet(new BetMock());
    }

    public function testAddBet_shouldReturnNull_whenEntityManagerPersistThrowsAnORMException(): void
    {
        $this->entityManager
            ->method('persist')
            ->will($this->throwException(new ORMException()));

        $this->assertNull($this->tested->addBet(new BetMock()));
    }

    public function testEntityManagerFlush_shouldBeCalled_whenAddBetCalled(): void
    {
        $this->entityManager
            ->expects($this->once())
            ->method('flush');

        $this->tested->addBet(new BetMock());
    }

    public function testAddBet_shouldReturnNull_whenEntityManagerFlushThrowsAnORMException(): void
    {
        $this->entityManager
            ->method('flush')
            ->will($this->throwException(new ORMException()));

        $this->assertNull($this->tested->addBet(new BetMock()));
    }
}