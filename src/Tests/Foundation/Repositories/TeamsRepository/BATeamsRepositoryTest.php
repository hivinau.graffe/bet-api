<?php

namespace Tests\Foundation\Repositories\TeamsRepository;

use App\Foundation\Repositories\TeamsRepository\BATeamsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\NamingStrategy;
use Doctrine\ORM\ORMException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\TeamMock;

final class BATeamsRepositoryTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BATeamsRepository
     */
    private $tested;

    /**
     * @var MockObject|EntityManagerInterface
     */
    private $entityManager;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        /** @var MockObject|NamingStrategy $namingStrategy */
        $namingStrategy = $this->getMockBuilder(NamingStrategy::class)->getMock();
        $classMetadata = new ClassMetadata(TeamMock::class, $namingStrategy);
        $this->entityManager
            ->method('getClassMetadata')
            ->willReturn($classMetadata);

        $this->tested = new BATeamsRepository($this->entityManager);
    }

    // MARK: - Tests

    public function testEntityManagerPersist_shouldBeCalled_whenAddTeamCalled(): void
    {
        $this->entityManager
            ->expects($this->once())
            ->method('persist');

        $this->tested->addTeam(new TeamMock());
    }

    public function testAddTeam_shouldReturnNull_whenEntityManagerPersistThrowsAnORMException(): void
    {
        $this->entityManager
            ->method('persist')
            ->will($this->throwException(new ORMException()));

        $this->assertNull($this->tested->addTeam(new TeamMock()));
    }

    public function testEntityManagerFlush_shouldBeCalled_whenAddTeamCalled(): void
    {
        $this->entityManager
            ->expects($this->once())
            ->method('flush');

        $this->tested->addTeam(new TeamMock());
    }

    public function testAddTeam_shouldReturnNull_whenEntityManagerFlushThrowsAnORMException(): void
    {
        $this->entityManager
            ->method('flush')
            ->will($this->throwException(new ORMException()));

        $this->assertNull($this->tested->addTeam(new TeamMock()));
    }
}