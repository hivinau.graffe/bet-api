<?php

namespace Tests\Foundation\Repositories\PlayersRepository;

use App\Foundation\Repositories\PlayersRepository\BAPlayersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\NamingStrategy;
use Doctrine\ORM\ORMException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\PlayerMock;

final class BAPlayersRepositoryTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAPlayersRepository
     */
    private $tested;

    /**
     * @var MockObject|EntityManagerInterface
     */
    private $entityManager;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        /** @var MockObject|NamingStrategy $namingStrategy */
        $namingStrategy = $this->getMockBuilder(NamingStrategy::class)->getMock();
        $classMetadata = new ClassMetadata(PlayerMock::class, $namingStrategy);
        $this->entityManager
            ->method('getClassMetadata')
            ->willReturn($classMetadata);

        $this->tested = new BAPlayersRepository($this->entityManager);
    }

    // MARK: - Tests

    public function testEntityManagerPersist_shouldBeCalled_whenAddPlayerCalled(): void
    {
        $this->entityManager
            ->expects($this->once())
            ->method('persist');

        $this->tested->addPlayer(new PlayerMock());
    }

    public function testAddPlayer_shouldReturnNull_whenEntityManagerPersistThrowsAnORMException(): void
    {
        $this->entityManager
            ->method('persist')
            ->will($this->throwException(new ORMException()));

        $this->assertNull($this->tested->addPlayer(new PlayerMock()));
    }

    public function testEntityManagerFlush_shouldBeCalled_whenAddPlayerCalled(): void
    {
        $this->entityManager
            ->expects($this->once())
            ->method('flush');

        $this->tested->addPlayer(new PlayerMock());
    }

    public function testAddPlayer_shouldReturnNull_whenEntityManagerFlushThrowsAnORMException(): void
    {
        $this->entityManager
            ->method('flush')
            ->will($this->throwException(new ORMException()));

        $this->assertNull($this->tested->addPlayer(new PlayerMock()));
    }
}