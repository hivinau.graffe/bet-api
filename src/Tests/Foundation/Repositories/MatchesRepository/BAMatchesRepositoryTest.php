<?php

namespace Tests\Foundation\Repositories\MatchesRepository;

use App\Foundation\Repositories\MatchesRepository\BAMatchesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\NamingStrategy;
use Doctrine\ORM\ORMException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\MatchMock;

final class BAMatchesRepositoryTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAMatchesRepository
     */
    private $tested;

    /**
     * @var MockObject|EntityManagerInterface
     */
    private $entityManager;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        /** @var MockObject|NamingStrategy $namingStrategy */
        $namingStrategy = $this->getMockBuilder(NamingStrategy::class)->getMock();
        $classMetadata = new ClassMetadata(MatchMock::class, $namingStrategy);
        $this->entityManager
            ->method('getClassMetadata')
            ->willReturn($classMetadata);

        $this->tested = new BAMatchesRepository($this->entityManager);
    }

    // MARK: - Tests


    public function testEntityManagerPersist_shouldBeCalled_whenAddMatchCalled(): void
    {
        $this->entityManager
            ->expects($this->once())
            ->method('persist');

        $this->tested->addMatch(new MatchMock());
    }

    public function testAddMatch_shouldReturnNull_whenEntityManagerPersistThrowsAnORMException(): void
    {
        $this->entityManager
            ->method('persist')
            ->will($this->throwException(new ORMException()));

        $this->assertNull($this->tested->addMatch(new MatchMock()));
    }

    public function testEntityManagerFlush_shouldBeCalled_whenAddMatchCalled(): void
    {
        $this->entityManager
            ->expects($this->once())
            ->method('flush');

        $this->tested->addMatch(new MatchMock());
    }

    public function testAddMatch_shouldReturnNull_whenEntityManagerFlushThrowsAnORMException(): void
    {
        $this->entityManager
            ->method('flush')
            ->will($this->throwException(new ORMException()));

        $this->assertNull($this->tested->addMatch(new MatchMock()));
    }
}