<?php

namespace Tests\Foundation\Repositories\GoalsRepository;

use App\Foundation\Repositories\GoalsRepository\BAGoalsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\NamingStrategy;
use Doctrine\ORM\ORMException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\GoalMock;

final class BAGoalsRepositoryTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAGoalsRepository
     */
    private $tested;

    /**
     * @var MockObject|EntityManagerInterface
     */
    private $entityManager;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        /** @var MockObject|NamingStrategy $namingStrategy */
        $namingStrategy = $this->getMockBuilder(NamingStrategy::class)->getMock();
        $classMetadata = new ClassMetadata(GoalMock::class, $namingStrategy);
        $this->entityManager
            ->method('getClassMetadata')
            ->willReturn($classMetadata);

        $this->tested = new BAGoalsRepository($this->entityManager);
    }

    // MARK: - Tests

    public function testEntityManagerPersist_shouldBeCalled_whenAddGoalCalled(): void
    {
        $this->entityManager
            ->expects($this->once())
            ->method('persist');

        $this->tested->addGoal(new GoalMock());
    }

    public function testAddGoal_shouldReturnNull_whenEntityManagerPersistThrowsAnORMException(): void
    {
        $this->entityManager
            ->method('persist')
            ->will($this->throwException(new ORMException()));

        $this->assertNull($this->tested->addGoal(new GoalMock()));
    }

    public function testEntityManagerFlush_shouldBeCalled_whenAddGoalCalled(): void
    {
        $this->entityManager
            ->expects($this->once())
            ->method('flush');

        $this->tested->addGoal(new GoalMock());
    }

    public function testAddGoal_shouldReturnNull_whenEntityManagerFlushThrowsAnORMException(): void
    {
        $this->entityManager
            ->method('flush')
            ->will($this->throwException(new ORMException()));

        $this->assertNull($this->tested->addGoal(new GoalMock()));
    }
}