<?php

namespace Tests\Foundation\Helpers;

use App\Foundation\Helpers\InitHelper;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\CustomClasses\ClassWithConstructorMock;
use Tests\Mocks\CustomClasses\ClassWithoutConstructorMock;

final class InitHelperTest extends TestCase {

    // MARK: - Tests

    public function testInitHelperInit_shouldReturnNull_whenClassNameIsNotValid(): void
    {
        $this->assertNull(InitHelper::init(''));
    }

    public function testInitHelperInit_shouldReturnSomeInstance_whenPassedSomeObject(): void
    {
        $object = new ClassWithConstructorMock();

        $this->assertEquals($object, InitHelper::init($object));
    }

    public function testInitHelperInit_shouldReturnSomeInstance_whenClassConstructorExists(): void
    {
        $this->assertInstanceOf(ClassWithConstructorMock::class, InitHelper::init(ClassWithConstructorMock::class));
    }

    public function testInitHelperInit_shouldReturnSomeInstance_whenClassConstructorDoesNotExist(): void
    {
        $this->assertInstanceOf(ClassWithoutConstructorMock::class, InitHelper::init(ClassWithoutConstructorMock::class));
    }
}