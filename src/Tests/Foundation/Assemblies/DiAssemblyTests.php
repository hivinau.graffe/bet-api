<?php

namespace Tests\Foundation\Assemblies;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Routers\Router;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Assemblies\AssemblyMock;

final class DiAssemblyTests extends TestCase {

    // MARK: - Private properties

    /**
     * @var DiAssembly
     */
    protected $tested;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->tested = new DiAssembly();
    }

    // MARK: - Tests

    public function testRouterSetContainer_shouldBeCalled_whenInjectDependenciesToRouter(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('setContainer');

        $this->tested->injectDependenciesToRouter($router);
    }

    public function testSomeAssemblyInjectDependenciesToRouter_shouldBeCalled_whenInjectSubAssembliesToRouter(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $assembly = new AssemblyMock();

        $this->tested->injectSubAssembliesToRouter([$assembly], $router);

        $this->assertTrue($assembly->injectDependenciesToRouterCalled);
    }
}