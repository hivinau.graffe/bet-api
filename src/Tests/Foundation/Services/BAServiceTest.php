<?php

namespace Tests\Foundation\Services;

use App\Foundation\Coordinators\Coordinator;
use App\Foundation\Routers\Router;
use App\Foundation\Services\BAService;
use App\Scenes\AppCoordinator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

final class BAServiceTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var BAService
     */
    private $tested;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->tested = new BAService();
    }

    // MARK: - Tests

    public function testRouterGetContainer_shouldBeCalled_whenRouterDidLoadRequestCalled(): void
    {
        /** @var MockObject|Coordinator $router */
        $coordinator = $this->getMockBuilder(Coordinator::class)->getMock();
        /** @var MockObject|ContainerInterface $router */
        $container = $this->getMockBuilder(ContainerInterface::class)->getMock();
        $container
            ->method('get')
            ->will(
                $this->returnCallback(function ($argument) use($coordinator) {
                    if($argument == AppCoordinator::class) {
                        return $coordinator;
                    }

                    return '';
                })
            );

        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->atLeast(1))
            ->method('getContainer')
            ->willReturn($container);

        $this->tested->routerDidLoadRequest($router);
    }
}