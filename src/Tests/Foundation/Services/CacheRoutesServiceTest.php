<?php

namespace Tests\Foundation\Services;

use App\Foundation\Routers\Router;
use App\Foundation\Services\CacheRoutesService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class CacheRoutesServiceTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var CacheRoutesService
     */
    private $tested;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->tested = new CacheRoutesService();
    }

    // MARK: - Tests

    public function testRouterGetRouteCollector_shouldBeCalled_whenRouterDidLoadRequestCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('getRouteCollector');

        $this->tested->routerDidLoadRequest($router);
    }
}