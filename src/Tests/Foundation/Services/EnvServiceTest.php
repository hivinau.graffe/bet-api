<?php

namespace Tests\Foundation\Services;

use App\Foundation\Routers\Router;
use App\Foundation\Services\EnvService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class EnvServiceTest extends TestCase {

    // MARK: - Private properties

    /**
     * @var EnvService
     */
    private $tested;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->tested = new EnvService();
    }

    // MARK: - Tests

    public function testRouterSetContainer_shouldBeCalled_whenRouterDidLoadRequestCalled(): void
    {
        /** @var MockObject|Router $router */
        $router = $this->getMockBuilder(Router::class)->getMock();
        $router
            ->expects($this->once())
            ->method('setContainer');

        $this->tested->routerDidLoadRequest($router);
    }
}