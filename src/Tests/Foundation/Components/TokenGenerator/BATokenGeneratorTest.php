<?php

namespace Tests\Foundation\Components\TokenGenerator;

use App\Foundation\Components\TokenGenerator\BATokenGenerator;
use App\Foundation\Components\TokenGenerator\Jwt\Jwt;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class BATokenGeneratorTest extends TestCase {

    // MARK: Private properties

    /**
     * @var BATokenGenerator
     */
    private $tested;

    /**
     * @var MockObject|Jwt
     */
    private $jwt;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->jwt = $this->getMockBuilder(Jwt::class)->getMock();
        $this->tested = new BATokenGenerator($this->jwt);
    }

    // MARK: Tests

    public function testJwtEncode_shouldBeCalled_whenTokenizeSignatureCalled(): void
    {
        $this->jwt
            ->expects($this->once())
            ->method('encode');

        $this->tested->tokenizeSignature('signature', 'salt', 'timestamp');
    }
}