<?php

namespace Tests\Foundation\Components\ImagesManager;

use App\Foundation\Components\ImagesManager\BAImagesManager;
use App\Foundation\Components\ImagesManager\ColorExtractor\ColorExtractor;
use App\Foundation\Components\ImagesManager\Server\Server;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\UploadedFileInterface;
use Exception;
use Slim\Psr7\Response;

final class BAImagesManagerTest extends TestCase {

    // MARK: Private properties

    /**
     * @var BAImagesManager
     */
    private $tested;

    /**
     * @var MockObject|Server
     */
    private $server;

    /**
     * @var MockObject|ColorExtractor
     */
    private $colorExtractor;

    // MARK: Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->server = $this->getMockBuilder(Server::class)->getMock();
        $this->colorExtractor = $this->getMockBuilder(ColorExtractor::class)->getMock();
        $this->tested = new BAImagesManager('/images', $this->server, $this->colorExtractor);
    }

    // MARK: Tests

    public function testUploadImage_shouldReturnImageName_withFileExtension(): void
    {
        /** @var MockObject|UploadedFileInterface $file */
        $file = $this->getMockBuilder(UploadedFileInterface::class)->getMock();
        $file
            ->method('getClientFilename')
            ->willReturn('filename.png');
        $this->colorExtractor
            ->method('extractHexColorFromImagePath')
            ->willReturn([
                'background' => 'background',
                'primary' => 'primary',
                'secondary' => 'secondary',
                'detail' => 'detail',
            ]);

        $image = $this->tested->uploadImage($file);
        $extensions = explode('.', $image['filename']);

        $this->assertFalse(empty($extensions));
        $this->assertEquals('png', $extensions[1]);
    }

    public function testUploadImage_shouldReturnEmptyArray_whenFileMoveToThrowsException(): void
    {
        /** @var MockObject|UploadedFileInterface $file */
        $file = $this->getMockBuilder(UploadedFileInterface::class)->getMock();
        $file
            ->method('getClientFilename')
            ->willReturn('filename.png');
        $file
            ->method('moveTo')
            ->will($this->throwException(new Exception()));

        $this->assertEmpty($this->tested->uploadImage($file));
    }

    public function testExtractHexColorFromImagePath_shouldBeCalled_whenUploadImage(): void
    {
        /** @var MockObject|UploadedFileInterface $file */
        $file = $this->getMockBuilder(UploadedFileInterface::class)->getMock();
        $file
            ->method('getClientFilename')
            ->willReturn('filename.png');
        $this->colorExtractor
            ->expects($this->once())
            ->method('extractHexColorFromImagePath');

        $this->tested->uploadImage($file);
    }

    public function testExtractHexColorFromImagePath_shouldNeverBeCalled_whenUploadImageAndFileMoveToThrowsException(): void
    {
        /** @var MockObject|UploadedFileInterface $file */
        $file = $this->getMockBuilder(UploadedFileInterface::class)->getMock();
        $file
            ->method('getClientFilename')
            ->willReturn('filename.png');
        $file
            ->method('moveTo')
            ->will($this->throwException(new Exception()));
        $this->colorExtractor
            ->expects($this->never())
            ->method('extractHexColorFromImagePath');

        $this->tested->uploadImage($file);
    }

    public function testGetImage_shouldReturnServerGetImageResponseResult(): void
    {
        $filename = 'test.png';
        $width = -1;
        $height = -1;
        $this->server
            ->method('getImageResponse')
            ->willReturn(new Response());

        $this->assertNotNull($this->tested->getImage($filename, $width, $height));
    }

    public function testGetImage_shouldReturnNull_whenServerGetImageResponseThrowsException(): void
    {
        $filename = 'test.png';
        $width = -1;
        $height = -1;
        $this->server
            ->method('getImageResponse')
            ->will($this->throwException(new Exception()));

        $this->assertNull($this->tested->getImage($filename, $width, $height));
    }
}