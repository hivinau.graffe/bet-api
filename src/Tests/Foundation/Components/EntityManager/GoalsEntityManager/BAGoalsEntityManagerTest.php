<?php

namespace Tests\Foundation\Components\EntityManager\GoalsEntityManager;

use App\Foundation\Components\EntityManager\GoalsEntityManager\BAGoalsEntityManager;
use App\Foundation\Repositories\GoalsRepository\GoalsRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\GoalMock;

final class BAGoalsEntityManagerTest extends TestCase {

    // MARK: Private properties

    /**
     * @var BAGoalsEntityManager
     */
    private $tested;

    /**
     * @var MockObject|GoalsRepository
     */
    private $goalsRepository;

    // MARK: Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->goalsRepository = $this->getMockBuilder(GoalsRepository::class)->getMock();

        /** @var MockObject|EntityManagerInterface $entityManager */
        $entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $entityManager
            ->method('getRepository')
            ->willReturn($this->goalsRepository);

        $this->tested = new BAGoalsEntityManager($entityManager);
    }

    // MARK: Tests

    public function testGoalsRepositoryFindMatchGoals_shouldBeCalled_whenFindMatchGoalsCalled(): void
    {
        $this->goalsRepository
            ->expects($this->once())
            ->method('findMatchGoals');

        $this->tested->findMatchGoals('match_id');
    }

    public function testAddGoal_shouldReturnGoalsRepositoryAddGoalResult(): void
    {
        $expected = new GoalMock();
        $this->goalsRepository
            ->method('addGoal')
            ->willReturn($expected);

        $this->assertEquals($expected, $this->tested->addGoal($expected));
    }
}