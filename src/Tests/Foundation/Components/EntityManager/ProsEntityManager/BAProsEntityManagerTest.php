<?php

namespace Tests\Foundation\Components\EntityManager\ProsEntityManager;

use App\Foundation\Components\EntityManager\ProsEntityManager\BAProsEntityManager;
use App\Foundation\Repositories\ProsRepository\ProsRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\ProMock;

final class BAProsEntityManagerTest extends TestCase {

    // MARK: Private properties

    /**
     * @var BAProsEntityManager
     */
    private $tested;

    /**
     * @var MockObject|ProsRepository
     */
    private $prosRepository;

    // MARK: Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->prosRepository = $this->getMockBuilder(ProsRepository::class)->getMock();

        /** @var MockObject|EntityManagerInterface $entityManager */
        $entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $entityManager
            ->method('getRepository')
            ->willReturn($this->prosRepository);

        $this->tested = new BAProsEntityManager($entityManager);
    }

    // MARK: Tests

    public function testProsRepositoryFindMatchPros_shouldBeCalled_whenFindMatchProsCalled(): void
    {
        $this->prosRepository
            ->expects($this->once())
            ->method('findMatchPros');

        $this->tested->findMatchPros('match_id');
    }

    public function testProsRepositoryGetProsByIds_shouldBeCalled_whenGetProsByIdsCalled(): void
    {
        $this->prosRepository
            ->expects($this->once())
            ->method('getProsByIds');

        $this->tested->getProsByIds(['pro_id']);
    }

    public function testAddPro_shouldReturnProsRepositoryAddProResult(): void
    {
        $expected = new ProMock();
        $this->prosRepository
            ->method('addPro')
            ->willReturn($expected);

        $this->assertEquals($expected, $this->tested->addPro($expected));
    }
}