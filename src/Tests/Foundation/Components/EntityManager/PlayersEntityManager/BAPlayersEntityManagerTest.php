<?php

namespace Tests\Foundation\Components\EntityManager\PlayersEntityManager;

use App\Foundation\Components\EntityManager\PlayersEntityManager\BAPlayersEntityManager;
use App\Foundation\Repositories\PlayersRepository\PlayersRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\PlayerMock;

final class BAPlayersEntityManagerTest extends TestCase {

    // MARK: Private properties

    /**
     * @var BAPlayersEntityManager
     */
    private $tested;

    /**
     * @var MockObject|PlayersRepository
     */
    private $playersRepository;

    // MARK: Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->playersRepository = $this->getMockBuilder(PlayersRepository::class)->getMock();

        /** @var MockObject|EntityManagerInterface $entityManager */
        $entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $entityManager
            ->method('getRepository')
            ->willReturn($this->playersRepository);

        $this->tested = new BAPlayersEntityManager($entityManager);
    }

    // MARK: Tests

    public function testAddPlayer_shouldReturnPlayersRepositoryAddPlayerResult(): void
    {
        $expected = new PlayerMock();
        $this->playersRepository
            ->method('addPlayer')
            ->willReturn($expected);

        $this->assertEquals($expected, $this->tested->addPlayer($expected));
    }
}