<?php

namespace Tests\Foundation\Components\EntityManager\TeamsEntityManager;

use App\Foundation\Components\EntityManager\TeamsEntityManager\BATeamsEntityManager;
use App\Foundation\Repositories\TeamsRepository\TeamsRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\TeamMock;

final class BATeamsEntityManagerTest extends TestCase {

    // MARK: Private properties

    /**
     * @var BATeamsEntityManager
     */
    private $tested;

    /**
     * @var MockObject|TeamsRepository
     */
    private $teamsRepository;

    // MARK: Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->teamsRepository = $this->getMockBuilder(TeamsRepository::class)->getMock();

        /** @var MockObject|EntityManagerInterface $entityManager */
        $entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $entityManager
            ->method('getRepository')
            ->willReturn($this->teamsRepository);

        $this->tested = new BATeamsEntityManager($entityManager);
    }

    // MARK: Tests

    public function testTeamsRepositoryGetTeamById_shouldBeCalled_whenGetTeamByIdCalled(): void
    {
        $this->teamsRepository
            ->expects($this->once())
            ->method('getTeamById');

        $this->tested->getTeamById('match_id');
    }

    public function testTeamsRepositoryGetTeamsByIds_shouldBeCalled_whenGetTeamsByIdsCalled(): void
    {
        $this->teamsRepository
            ->expects($this->once())
            ->method('getTeamsByIds');

        $this->tested->getTeamsByIds(['team_id']);
    }

    public function testAddTeam_shouldReturnTeamsRepositoryAddTeamResult(): void
    {
        $expected = new TeamMock();
        $this->teamsRepository
            ->method('addTeam')
            ->willReturn($expected);

        $this->assertEquals($expected, $this->tested->addTeam($expected));
    }
}