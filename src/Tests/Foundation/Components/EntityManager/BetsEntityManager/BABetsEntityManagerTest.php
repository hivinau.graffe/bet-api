<?php

namespace Tests\Foundation\Components\EntityManager\BetsEntityManager;

use App\Foundation\Components\EntityManager\BetsEntityManager\BABetsEntityManager;
use App\Foundation\Repositories\BetsRepository\BetsRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\BetMock;

final class BABetsEntityManagerTest extends TestCase {

    // MARK: Private properties

    /**
     * @var BABetsEntityManager
     */
    private $tested;

    /**
     * @var MockObject|BetsRepository
     */
    private $betsRepository;

    // MARK: Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->betsRepository = $this->getMockBuilder(BetsRepository::class)->getMock();

        /** @var MockObject|EntityManagerInterface $entityManager */
        $entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $entityManager
            ->method('getRepository')
            ->willReturn($this->betsRepository);

        $this->tested = new BABetsEntityManager($entityManager);
    }

    // MARK: Tests

    public function testBetsRepositoryFindBets_shouldBeCalled_whenFindBetsCalled(): void
    {
        $this->betsRepository
            ->expects($this->once())
            ->method('findBets');

        $this->tested->findBets();
    }

    public function testAddBet_shouldReturnBetsRepositoryAddBetResult(): void
    {
        $expected = new BetMock();
        $this->betsRepository
            ->method('addBet')
            ->willReturn($expected);

        $this->assertEquals($expected, $this->tested->addBet($expected));
    }
}