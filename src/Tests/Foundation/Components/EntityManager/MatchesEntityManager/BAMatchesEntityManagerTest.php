<?php

namespace Tests\Foundation\Components\EntityManager\MatchesEntityManager;

use App\Foundation\Components\EntityManager\MatchesEntityManager\BAMatchesEntityManager;
use App\Foundation\Repositories\MatchesRepository\MatchesRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Tests\Mocks\Models\MatchMock;

final class BAMatchesEntityManagerTest extends TestCase {

    // MARK: Private properties

    /**
     * @var BAMatchesEntityManager
     */
    private $tested;

    /**
     * @var MockObject|MatchesRepository
     */
    private $matchesRepository;

    // MARK: Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->matchesRepository = $this->getMockBuilder(MatchesRepository::class)->getMock();

        /** @var MockObject|EntityManagerInterface $entityManager */
        $entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $entityManager
            ->method('getRepository')
            ->willReturn($this->matchesRepository);

        $this->tested = new BAMatchesEntityManager($entityManager);
    }

    // MARK: Tests

    public function testMatchesRepositoryFindBetMatches_shouldBeCalled_whenFindBetMatchesCalled(): void
    {
        $this->matchesRepository
            ->expects($this->once())
            ->method('findBetMatches');

        $this->tested->findBetMatches('bet_id');
    }

    public function testMatchesRepositoryGetMatchById_shouldBeCalled_whenGetMatchByIdCalled(): void
    {
        $this->matchesRepository
            ->expects($this->once())
            ->method('getMatchById');

        $this->tested->getMatchById('match_id');
    }

    public function testMatchesRepositoryGetMatchesByIds_shouldBeCalled_whenGetMatchesByIdsCalled(): void
    {
        $this->matchesRepository
            ->expects($this->once())
            ->method('getMatchesByIds');

        $this->tested->getMatchesByIds(['match_id']);
    }

    public function testAddMatch_shouldReturnMatchesRepositoryAddMatchResult(): void
    {
        $expected = new MatchMock();
        $this->matchesRepository
            ->method('addMatch')
            ->willReturn($expected);

        $this->assertEquals($expected, $this->tested->addMatch($expected));
    }
}