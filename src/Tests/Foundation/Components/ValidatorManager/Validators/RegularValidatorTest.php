<?php

namespace Tests\Foundation\Components\ValidatorManager;

use App\Foundation\Components\ValidatorManager\Validators\RegularValidator;
use PHPUnit\Framework\TestCase;

final class RegularValidatorTest extends TestCase {

    // MARK: Tests

    public function testValidate_shouldReturnFalse_whenContentIsLowerThanMinLength(): void
    {
        $tested = new RegularValidator(10);
        $this->assertFalse($tested->validate(''));
        $this->assertFalse($tested->validate('123456789'));
    }

    public function testValidate_shouldReturnTruewhenContentIsGreatherOrEqualThanMinLength(): void
    {
        $tested = new RegularValidator(9);
        $this->assertTrue($tested->validate('1234567890'));
    }
}