<?php

namespace Tests\Foundation\Components\ValidatorManager;

use App\Foundation\Components\ValidatorManager\Validators\DateValidator;
use PHPUnit\Framework\TestCase;

final class DateValidatorTest extends TestCase {

    // MARK: Private properties

    /**
     * @var DateValidator
     */
    private $tested;

    // MARK: Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->tested = new DateValidator();
    }

    // MARK: Tests

    public function testValidate_shouldReturnFalse_whenContentIsEmpty(): void
    {
        $this->assertFalse($this->tested->validate(''));
    }

    public function testValidate_shouldReturnFalse_whenContentIsNotAValidDate(): void
    {
        $this->assertFalse($this->tested->validate('2020/10-10'));
    }

    public function testValidate_shouldReturnTrue_whenContentIsAValidDate(): void
    {
        $this->assertTrue($this->tested->validate('2020-10-10'));
    }
}