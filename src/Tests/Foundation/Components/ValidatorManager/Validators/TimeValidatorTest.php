<?php

namespace Tests\Foundation\Components\ValidatorManager;

use App\Foundation\Components\ValidatorManager\Validators\TimeValidator;
use PHPUnit\Framework\TestCase;

final class TimeValidatorTest extends TestCase {

    // MARK: Private properties

    /**
     * @var TimeValidator
     */
    private $tested;

    // MARK: Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->tested = new TimeValidator();
    }

    // MARK: Tests

    public function testValidate_shouldReturnFalse_whenContentIsEmpty(): void
    {
        $this->assertFalse($this->tested->validate(''));
    }

    public function testValidate_shouldReturnFalse_whenContentIsNotAValidTime(): void
    {
        $this->assertFalse($this->tested->validate('18:3'));
    }

    public function testValidate_shouldReturnTrue_whenContentIsAValidTime(): void
    {
        $this->assertTrue($this->tested->validate('18:39'));
    }
}