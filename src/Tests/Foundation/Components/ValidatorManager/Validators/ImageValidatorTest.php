<?php

namespace Tests\Foundation\Components\ValidatorManager;

use App\Foundation\Components\ValidatorManager\Validators\ImageValidator;
use PHPUnit\Framework\TestCase;

final class ImageValidatorTest extends TestCase {

    // MARK: Private properties

    /**
     * @var ImageValidator
     */
    private $tested;

    // MARK: Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->tested = new ImageValidator();
    }

    // MARK: Tests

    public function testValidate_shouldReturnFalse_whenContentIsEmpty(): void
    {
        $this->assertFalse($this->tested->validate(''));
    }

    public function testValidate_shouldReturnFalse_whenContentIsNotAValidTime(): void
    {
        $this->assertFalse($this->tested->validate('azergredarz'));
    }

    public function testValidate_shouldReturnTrue_whenContentIsAValidImage(): void
    {
        $this->assertTrue($this->tested->validate('5c2c542ad787c444.png'));
    }
}