<?php

namespace Tests\Foundation\Components\ValidatorManager;

use App\Foundation\Components\ValidatorManager\Validators\UuidValidator;
use PHPUnit\Framework\TestCase;

final class UuidValidatorTest extends TestCase {

    // MARK: Private properties

    /**
     * @var UuidValidator
     */
    private $tested;

    // MARK: Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->tested = new UuidValidator();
    }

    // MARK: Tests

    public function testValidate_shouldReturnFalse_whenContentIsEmpty(): void
    {
        $this->assertFalse($this->tested->validate(''));
    }

    public function testValidate_shouldReturnFalse_whenContentIsNotAValidTime(): void
    {
        $this->assertFalse($this->tested->validate('azergredarz'));
    }

    public function testValidate_shouldReturnTrue_whenContentIsAValidUuid(): void
    {
        $this->assertTrue($this->tested->validate('c70c7f28-2ce9-4fa4-bd1f-36f8574faea7'));
    }
}