<?php

namespace Tests\Foundation\Components\ValidatorManager;

use App\Foundation\Components\ValidatorManager\BAValidatorManager;
use App\Foundation\Components\ValidatorManager\Validators\Validator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

final class BAValidatorManagerTest extends TestCase {

    // MARK: Private properties

    /**
     * @var BAValidatorManager
     */
    private $tested;

    // MARK: Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->tested = new BAValidatorManager();
    }

    // MARK: Tests

    public function testValidate_shouldReturnFalse_whenInitOrNoValidatorIsAdded(): void
    {
        $this->assertFalse($this->tested->validate(''));
    }

    public function testValidate_shouldReturnFalse_whenAddedValidatorValidateReturnsFalse(): void
    {
        /** @var MockObject|Validator $validator */
        $validator = $this->getMockBuilder(Validator::class)->getMock();
        $validator
            ->method('validate')
            ->willReturn(false);
        $this->tested->addValidator($validator);

        $this->assertFalse($this->tested->validate(''));
    }

    public function testValidate_shouldReturnTrue_whenAddedValidatorValidateReturnsTrue(): void
    {
        /** @var MockObject|Validator $validator */
        $validator = $this->getMockBuilder(Validator::class)->getMock();
        $validator
            ->method('validate')
            ->willReturn(true);
        $this->tested->addValidator($validator);

        $this->assertTrue($this->tested->validate(''));
    }
}