<?php

namespace Tests\Foundation\Components\StringExploder;

use App\Foundation\Components\StringExploder\BAStringExploder;
use PHPUnit\Framework\TestCase;

final class BAStringExploderTest extends TestCase {

    // MARK: Private properties

    /**
     * @var BAStringExploder
     */
    private $tested;

    // MARK: Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->tested = new BAStringExploder();
    }

    // MARK: Tests

    public function testExplode_shouldExplodeStringByComma(): void
    {
        $matchesIds = 'matchId1, matchId2';

        $this->assertEquals(['matchId1', 'matchId2'], $this->tested->explode($matchesIds));
    }

    public function testExplode_shouldExplodeStringByCommaAndIgnoreEmpty(): void
    {
        $matchesIds = 'matchId1, ,matchId2';

        $this->assertEquals(['matchId1', 'matchId2'], $this->tested->explode($matchesIds));
    }
}