<?php

namespace Tests\Foundation\Components\DatetimeManager;

use App\Foundation\Components\DatetimeManager\BADatetimeManager;
use PHPUnit\Framework\TestCase;

final class BADatetimeManagerTest extends TestCase {

    // MARK: Private properties

    /**
     * @var BADatetimeManager
     */
    private $tested;

    // MARK: Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->tested = new BADatetimeManager();
    }

    // MARK: Tests

    public function testDate_shouldBeEqualToDateString_whenCreateDateCalled(): void
    {
        $dateString = '2020-01-01';
        $date = $this->tested->createDate($dateString);

        $this->assertEquals($dateString, $date->format('Y-m-d'));
    }

    public function testTime_shouldBeEqualToTimeString_whenCreateDateCalled(): void
    {
        $timeString = '20:00';
        $time = $this->tested->createTime($timeString);

        $this->assertEquals($timeString, $time->format('H:i'));
    }
}