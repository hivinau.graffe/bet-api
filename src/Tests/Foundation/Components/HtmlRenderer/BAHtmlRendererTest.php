<?php

namespace Tests\Foundation\Components\HtmlRenderer;

use App\Foundation\Components\HtmlRenderer\BAHtmlRenderer;
use App\Foundation\Components\HtmlRenderer\Twig\View;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

final class BAHtmlRendererTest extends TestCase {

    // MARK: Private properties

    private BAHtmlRenderer $tested;

    /**
     * @var MockObject|View
     */
    private $view;

    // MARK: - Lifecycle

    protected function setUp(): void
    {
        parent::setUp();

        $this->view = $this->getMockBuilder(View::class)->getMock();
        $this->tested = new BAHtmlRenderer($this->view);
    }

    // MARK: Tests

    public function testViewRenderResponse_shouldBeCalled_whenRenderResponseCalled(): void
    {
        $response = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $name = '';
        $context = [];
        $this->view
            ->expects($this->once())
            ->method('renderResponse');

        $this->tested->renderResponse($response, $name, $context);
    }
}