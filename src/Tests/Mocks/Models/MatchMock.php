<?php

namespace Tests\Mocks\Models;

use App\Foundation\Repositories\BetsRepository\Models\Bet;
use App\Foundation\Repositories\GoalsRepository\Models\Goal;
use App\Foundation\Repositories\MatchesRepository\Models\Match;
use App\Foundation\Repositories\ProsRepository\Models\Pro;
use App\Foundation\Repositories\TeamsRepository\Models\Team;
use DateTimeInterface;

final class MatchMock implements Match {

    // MARK: Public properties

    /**
     * @var array
     */
    public $prosMockValue;

    /**
     * @var array
     */
    public $betsMockValue;

    /**
     * @var bool
     */
    public $liveMockValue;

    /**
     * @var bool
     */
    public $startedMockValue;

    /**
     * @var string
     */
    public $idMockValue;

    /**
     * @var DateTimeInterface
     */
    public $dateMockValue;

    /**
     * @var DateTimeInterface
     */
    public $timeMockValue;

    /**
     * @var array
     */
    public $teamsMockValue;

    /**
     * @var array
     */
    public $jsonSerializeMockResult;

    /**
     * @var int
     */
    public $addTeamCallsCount = 0;

    /**
     * @var bool
     */
    public $addTeamCalled = false;

    /**
     * @var int
     */
    public $addProCallsCount = 0;

    /**
     * @var bool
     */
    public $addProCalled = false;

    /**
     * @var int
     */
    public $addGoalCallsCount = 0;

    /**
     * @var bool
     */
    public $addGoalCalled = false;

    /**
     * @var int
     */
    public $setLiveCallsCount = 0;

    /**
     * @var bool
     */
    public $setLiveCalled = false;

    /**
     * @var int
     */
    public $setStartedCallsCount = 0;

    /**
     * @var bool
     */
    public $setStartedCalled = false;

    /**
     * @var int
     */
    public $setDateCallsCount = 0;

    /**
     * @var bool
     */
    public $setDateCalled = false;

    /**
     * @var int
     */
    public $setTimeCallsCount = 0;

    /**
     * @var bool
     */
    public $setTimeCalled = false;

    /**
     * @var int
     */
    public $setHomeCallsCount = 0;

    /**
     * @var bool
     */
    public $setHomeCalled = false;

    /**
     * @var int
     */
    public $setVisitorCallsCount = 0;

    /**
     * @var bool
     */
    public $setVisitorCalled = false;

    /**
     * @var int
     */
    public $addBetCallsCount = 0;

    /**
     * @var bool
     */
    public $addBetCalled = false;

    // MARK: Pro methods

    function getPros(): array
    {
        return $this->prosMockValue ?? [];
    }

    function getBets(): array
    {
        return $this->betsMockValue ?? [];
    }

    function isLive(): bool
    {
        return $this->liveMockValue;
    }

    function isStarted(): bool
    {
        return $this->startedMockValue;
    }

    function getDate(): DateTimeInterface
    {
        return $this->dateMockValue;
    }

    function getTime(): DateTimeInterface
    {
        return $this->timeMockValue;
    }

    function getId(): ?string
    {
        return $this->idMockValue;
    }

    public function getTeams(): array
    {
        return $this->teamsMockValue;
    }

    public function jsonSerialize()
    {
        return $this->jsonSerializeMockResult ?? [];
    }

    function addPro(Pro $pro): Match
    {
        $this->addProCallsCount++;
        $this->addProCalled = true;
        return $this;
    }

    public function addTeam(Team $team): Match
    {
        $this->addTeamCallsCount++;
        $this->addTeamCalled = true;
        return $this;
    }

    function addGoal(Goal $goal): Match
    {
        $this->addGoalCallsCount++;
        $this->addGoalCalled = true;
        return $this;
    }

    function setLive(bool $live): Match
    {
        $this->setLiveCallsCount++;
        $this->setLiveCalled = true;
        return $this;
    }

    function setStarted(bool $started): Match
    {
        $this->setStartedCallsCount++;
        $this->setStartedCalled = true;
        return $this;
    }

    function setDate(DateTimeInterface $date): Match
    {
        $this->setDateCallsCount++;
        $this->setDateCalled = true;
        return $this;
    }

    function setTime(DateTimeInterface $time): Match
    {
        $this->setTimeCallsCount++;
        $this->setTimeCalled = true;
        return $this;
    }

    function setHome(Team $home): Match
    {
        $this->setHomeCallsCount++;
        $this->setHomeCalled = true;
        return $this;
    }

    function setVisitor(Team $visitor): Match
    {
        $this->setVisitorCallsCount++;
        $this->setVisitorCalled = true;
        return $this;
    }

    function addBet(Bet $bet): Match
    {
        $this->addBetCallsCount++;
        $this->addBetCalled = true;
       return $this;
    }
}

