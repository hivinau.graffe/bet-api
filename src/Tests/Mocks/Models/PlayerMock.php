<?php

namespace Tests\Mocks\Models;

use App\Foundation\Repositories\PlayersRepository\Models\Player;

final class PlayerMock implements Player {

    // MARK: Public properties

    /**
     * @var null|string
     */
    public $tokenMockValue;

    /**
     * @var null|string
     */
    public $idMockValue;

    /**
     * @var boolean
     */
    public $isRevokedMockValue;

    /**
     * @var array
     */
    public $jsonSerializeMockResult;

    // MARK: Pro methods

    public function getToken(): ?string
    {
        return $this->tokenMockValue;
    }

    public function getId(): ?string
    {
        return $this->idMockValue;
    }

    public function isRevoked(): bool
    {
        return $this->isRevokedMockValue;
    }

    public function jsonSerialize(): array
    {
        return $this->jsonSerializeMockResult ?? [];
    }
}

