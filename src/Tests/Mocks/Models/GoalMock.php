<?php

namespace Tests\Mocks\Models;

use App\Foundation\Repositories\GoalsRepository\Models\Goal;
use App\Foundation\Repositories\MatchesRepository\Models\Match;
use App\Foundation\Repositories\TeamsRepository\Models\Team;

final class GoalMock implements Goal {

    // MARK: Public properties

    /**
     * @var int
     */
    public $setMatchCallsCount = 0;

    /**
     * @var int
     */
    public $setTeamCallsCount = 0;

    /**
     * @var int
     */
    public $setScorerCallsCount = 0;

    /**
     * @var int
     */
    public $setTimeCallsCount = 0;

    /**
     * @var bool
     */
    public $setMatchCalled = false;

    /**
     * @var bool
     */
    public $setTeamCalled = false;

    /**
     * @var bool
     */
    public $setScorerCalled = false;

    /**
     * @var bool
     */
    public $setTimeCalled = false;

    /**
     * @var string
     */
    public $idMockValue;

    /**
     * @var Match|null
     */
    public $matchMockValue;

    /**
     * @var Team|null
     */
    public $teamMockValue;

    /**
     * @var string|null
     */
    public $scorerMockValue;

    /**
     * @var float
     */
    public $timeMockValue;

    /**
     * @var array
     */
    public $jsonSerializeMockResult;

    // MARK: Pro methods

    public function getId(): ?string
    {
        return $this->idMockValue;
    }

    function getMatch(): ?Match
    {
        return $this->matchMockValue;
    }

    function getTeam(): ?Team
    {
        return $this->teamMockValue;
    }

    function getScorer(): ?string
    {
        return $this->scorerMockValue;
    }

    function getTime(): float
    {
        return $this->timeMockValue;
    }

    public function jsonSerialize(): array
    {
        return $this->jsonSerializeMockResult ?? [];
    }

    function setMatch(Match $match): Goal
    {
        $this->setMatchCallsCount++;
        $this->setMatchCalled = true;
        return $this;
    }

    function setTeam(Team $team): Goal
    {
        $this->setTeamCallsCount++;
        $this->setTeamCalled = true;
        return $this;
    }

    function setScorer(string $scorer): Goal
    {
        $this->setScorerCallsCount++;
        $this->setScorerCalled = true;
        return $this;
    }

    function setTime(float $time): Goal
    {
        $this->setTimeCallsCount++;
        $this->setTimeCalled = true;
        return $this;
    }
}

