<?php

namespace Tests\Mocks\Models;

use App\Foundation\Repositories\MatchesRepository\Models\Match;
use App\Foundation\Repositories\ProsRepository\Models\Pro;

final class ProMock implements Pro {

    // MARK: Public properties

    /**
     * @var array
     */
    public $matchesMockValue;

    /**
     * @var string
     */
    public $labelMockValue;

    /**
     * @var string
     */
    public $idMockValue;

    /**
     * @var string
     */
    public $typeMockValue;

    /**
     * @var array
     */
    public $jsonSerializeMockResult;

    /**
     * @var bool
     */
    public $wonMockValue;

    /**
     * @var int
     */
    public $addMatchCallsCount = 0;

    /**
     * @var bool
     */
    public $addMatchCalled = false;

    /**
     * @var int
     */
    public $setLabelCallsCount = 0;

    /**
     * @var bool
     */
    public $setLabelCalled = false;

    /**
     * @var int
     */
    public $setTypeCallsCount = 0;

    /**
     * @var bool
     */
    public $setTypeCalled = false;

    /**
     * @var int
     */
    public $setWonCallsCount = 0;

    /**
     * @var bool
     */
    public $setWonCalled = false;

    // MARK: Pro methods

    public function getMatches(): array
    {
        return $this->matchesMockValue ?? [];
    }

    public function getLabel(): string
    {
        return $this->labelMockValue;
    }

    public function getId(): ?string
    {
        return $this->idMockValue;
    }

    public function getType(): ?string
    {
        return $this->idMockValue;
    }

    public function jsonSerialize(): array
    {
        return $this->jsonSerializeMockResult ?? [];
    }

    function isWon(): bool
    {
        return $this->wonMockValue;
    }

    function addMatch(Match $match): Pro
    {
        $this->addMatchCallsCount++;
        $this->addMatchCalled = true;
        return $this;
    }

    function setLabel(string $label): Pro
    {
        $this->setLabelCallsCount++;
        $this->setLabelCalled = true;
        return $this;
    }

    function setType(string $type): Pro
    {
        $this->setTypeCallsCount++;
        $this->setTypeCalled = true;
        return $this;
    }

    function setWon(bool $won): Pro
    {
        $this->setWonCallsCount++;
        $this->setWonCalled = true;
        return $this;
    }
}

