<?php

namespace Tests\Mocks\Models;

use App\Foundation\Repositories\BetsRepository\Models\Bet;
use App\Foundation\Repositories\MatchesRepository\Models\Match;

final class BetMock implements Bet {

    // MARK: Public properties

    /**
     * @var int
     */
    public $addMatchCallsCount = 0;

    /**
     * @var bool
     */
    public $addMatchCalled = false;

    /**
     * @var array
     */
    public $matchesMockValue;

    /**
     * @var bool
     */
    public $lockedMockValue;

    /**
     * @var float
     */
    public $priceToUnlockMockValue;

    /**
     * @var bool
     */
    public $postedMockValue;

    /**
     * @var string
     */
    public $idMockValue;

    /**
     * @var array
     */
    public $jsonSerializeMockResult;

    // MARK: Pro methods

    public function getMatches(): array
    {
        return $this->matchesMockValue ?? [];
    }

    public function getId(): ?string
    {
        return $this->idMockValue;
    }

    public function isLocked(): bool
    {
        return $this->lockedMockValue;
    }

    public function getPriceToUnlock(): float
    {
        return $this->priceToUnlockMockValue;
    }

    public function isPosted(): bool
    {
        return $this->postedMockValue;
    }

    public function jsonSerialize()
    {
        return $this->jsonSerializeMockResult ?? [];
    }

    function addMatch(Match $match): Bet
    {
        $this->addMatchCallsCount++;
        $this->addMatchCalled = true;
        return $this;
    }
}

