<?php

namespace Tests\Mocks\Models;

use App\Foundation\Repositories\GoalsRepository\Models\Goal;
use App\Foundation\Repositories\MatchesRepository\Models\Match;
use App\Foundation\Repositories\TeamsRepository\Models\Team;

final class TeamMock implements Team {

    // MARK: Public properties

    /**
     * @var array
     */
    public $matchesMockValue;

    // MARK: Public properties

    /**
     * @var array
     */
    public $goalsMockValue;

    /**
     * @var string
     */
    public $nameMockValue;

    /**
     * @var null|string
     */
    public $imageMockValue;

    /**
     * @var null|string
     */
    public $imageBackgroundColorMockValue;

    /**
     * @var null|string
     */
    public $imagePrimaryColorMockValue;

    /**
     * @var null|string
     */
    public $imageSecondaryColorMockValue;

    /**
     * @var null|string
     */
    public $imageDetailColorMockValue;

    /**
     * @var string
     */
    public $idMockValue;

    /**
     * @var array
     */
    public $jsonSerializeMockResult;

    /**
     * @var int
     */
    public $addMatchCallsCount = 0;

    /**
     * @var bool
     */
    public $addMatchCalled = false;

    /**
     * @var int
     */
    public $addGoalCallsCount = 0;

    /**
     * @var bool
     */
    public $addGoalCalled = false;

    /**
     * @var int
     */
    public $setNameCallsCount = 0;

    /**
     * @var bool
     */
    public $setNameCalled = false;

    /**
     * @var int
     */
    public $setImageCallsCount = 0;

    /**
     * @var bool
     */
    public $setImageCalled = false;

    /**
     * @var int
     */
    public $setImageBackgroundColorCallsCount = 0;

    /**
     * @var bool
     */
    public $setImageBackgroundColorCalled = false;

    /**
     * @var int
     */
    public $setImagePrimaryColorCallsCount = 0;

    /**
     * @var bool
     */
    public $setImagePrimaryColorCalled = false;

    /**
     * @var int
     */
    public $setImageSecondaryColorCallsCount = 0;

    /**
     * @var bool
     */
    public $setImageSecondaryColorCalled = false;

    /**
     * @var int
     */
    public $setImageDetailColorCallsCount = 0;

    /**
     * @var bool
     */
    public $setImageDetailColorCalled = false;

    // MARK: Pro methods

    public function getMatches(): array
    {
        return $this->matchesMockValue ?? [];
    }

    public function getName(): string
    {
        return $this->nameMockValue;
    }

    public function getImage(): ?string
    {
        return $this->imageMockValue;
    }

    public function getId(): ?string
    {
        return $this->idMockValue;
    }

    function getGoals(): array
    {
        return $this->goalsMockValue ?? [];
    }

    function getImageBackgroundColor(): ?string
    {
        return $this->imageBackgroundColorMockValue;
    }

    function getImagePrimaryColor(): ?string
    {
        return $this->imagePrimaryColorMockValue;
    }

    function getImageSecondaryColor(): ?string
    {
        return $this->imageSecondaryColorMockValue;
    }

    function getImageDetailColor(): ?string
    {
        return $this->imageDetailColorMockValue;
    }

    public function jsonSerialize(): array
    {
        return $this->jsonSerializeMockResult ?? [];
    }

    function addMatch(Match $match): Team
    {
        $this->addMatchCallsCount++;
        $this->addMatchCalled = true;
        return $this;
    }

    function addGoal(Goal $goal): Team
    {
        $this->addGoalCallsCount++;
        $this->addGoalCalled = true;
        return $this;
    }

    function setName(string $name): Team
    {
        $this->setNameCallsCount++;
        $this->setNameCalled = true;
        return $this;
    }

    function setImage(?string $image): Team
    {
        $this->setImageCallsCount++;
        $this->setImageCalled = true;
        return $this;
    }

    function setImageBackgroundColor(?string $color): Team
    {
        $this->setImageBackgroundColorCallsCount++;
        $this->setImageBackgroundColorCalled = true;
        return $this;
    }

    function setImagePrimaryColor(?string $color): Team
    {
        $this->setImagePrimaryColorCallsCount++;
        $this->setImagePrimaryColorCalled = true;
        return $this;
    }

    function setImageSecondaryColor(?string $color): Team
    {
        $this->setImageSecondaryColorCallsCount++;
        $this->setImageSecondaryColorCalled = true;
        return $this;
    }

    function setImageDetailColor(?string $color): Team
    {
        $this->setImageDetailColorCallsCount++;
        $this->setImageDetailColorCalled = true;
        return $this;
    }
}

