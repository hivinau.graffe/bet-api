<?php

namespace Tests\Mocks\Assemblies;

use App\Foundation\Routers\Router;

final class AssemblyMock {

    /**
     * @var bool
     */
    public $injectDependenciesToRouterCalled = false;

    public function injectDependenciesToRouter(Router $router)
    {
        $this->injectDependenciesToRouterCalled = true;
    }
}