<?php

namespace App\Foundation\Coordinators;

use App\Foundation\Routers\Router;

interface Coordinator {
    function startRouter(Router $router): void;
}
