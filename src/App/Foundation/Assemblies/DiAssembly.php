<?php

namespace App\Foundation\Assemblies;

use App\Foundation\Helpers\InitHelper;
use App\Foundation\Routers\Router;
use DI\ContainerBuilder;
use Exception;

class DiAssembly implements Assembly {

    // MARK: - Private properties

    /**
     * @var ContainerBuilder
     */
    protected $builder;

    // MARK: - Init

    public function __construct()
    {
        $this->builder = new ContainerBuilder();
    }

    // MARK: - Assembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        try {
            $router->setContainer($this->builder->build());
        } catch (Exception $ignored) {}
    }

    public function injectSubAssembliesToRouter(array $assemblies, Router $router): void
    {
        foreach ($assemblies as $assemblyName) {
            /** @var null|Assembly $assembly */
            $assembly = InitHelper::init($assemblyName);

            if(is_null($assembly)) {
                return;
            }

            $assembly->injectDependenciesToRouter($router);
        }
    }
}