<?php

namespace App\Foundation\Assemblies;

use App\Foundation\Routers\Router;

interface Assembly {
    function injectDependenciesToRouter(Router $router): void;

    /**
     * @param string[] $assemblies
     * @param Router $router
     */
    function injectSubAssembliesToRouter(array $assemblies, Router $router): void;
}
