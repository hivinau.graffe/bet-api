<?php

namespace App\Foundation\Core;

use Acclimate\Container\CompositeContainer;
use Acclimate\Container\ContainerAcclimator;
use App\Foundation\Helpers\InitHelper;
use App\Foundation\Routers\Router;
use App\Foundation\Services\BAService;
use App\Foundation\Services\CacheRoutesService;
use App\Foundation\Services\EnvService;
use App\Foundation\Services\Service;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;

final class Application extends App implements Router {

    // MARK: - Constants

    /**
     * @var Service[]
     */
    private $services = [
        EnvService::class,
        BAService::class,
        CacheRoutesService::class
    ];

    /**
     * @var ContainerAcclimator
     */
    private $acclimator;

    public function __construct(ResponseFactoryInterface $responseFactory)
    {
        parent::__construct($responseFactory, new CompositeContainer());

        $this->acclimator = new ContainerAcclimator();
    }

    // MARK: - App methods

    public function run(?ServerRequestInterface $request = null): void
    {
        $this->delegateServicesApplicationDidLoadAsRouter();
        parent::run($request);
    }

    public function setContainer(?ContainerInterface $container): void
    {
        $container1 = $this->acclimator->acclimate($container);
        $container2 = $this->acclimator->acclimate($this->container);
        $this->container = new CompositeContainer([$container1, $container2]);
    }

    // MARK: - Private methods

    private function delegateServicesApplicationDidLoadAsRouter(): void
    {
        foreach ($this->services as $serviceName) {
            /** @var null|Service $service */
            $service = InitHelper::init($serviceName);

            if(is_null($service)) {
                return;
            }

            $service->routerDidLoadRequest($this);
        }
    }
}