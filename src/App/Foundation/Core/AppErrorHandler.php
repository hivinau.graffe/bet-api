<?php

namespace App\Foundation\Core;

use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Response;
use Throwable;

final class AppErrorHandler {

    // MARK: - Private constants

    private const ERROR = 'error';

    // MARK: - Public methods

    public function __invoke(ServerRequestInterface $request, Throwable $exception): ResponseInterface
    {
        $payload = [self::ERROR => $exception->getMessage()];

        $response = new Response();
        $response->getBody()->write(json_encode($payload));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(StatusCodeInterface::STATUS_BAD_REQUEST);
    }
}