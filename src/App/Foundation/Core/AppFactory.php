<?php

namespace App\Foundation\Core;

use Slim\Psr7\Factory\ResponseFactory;

final class AppFactory {

    // MARK: - Public methods

    public static function create(): Application
    {
        $responseFactory = new ResponseFactory();
        return new Application($responseFactory);
    }
}