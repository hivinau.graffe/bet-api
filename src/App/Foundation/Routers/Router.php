<?php

namespace App\Foundation\Routers;

use Psr\Container\ContainerInterface;
use Slim\Interfaces\RouteCollectorInterface;
use Slim\Interfaces\RouteInterface;

interface Router {
    function get(string $pattern, callable $callable): RouteInterface;
    function post(string $pattern, callable $callable): RouteInterface;
    function setContainer(?ContainerInterface $container): void;
    function getContainer(): ?ContainerInterface;
    function getRouteCollector(): RouteCollectorInterface;
}