<?php

namespace App\Foundation\Helpers;

use ReflectionClass;

final class InitHelper {

    // MARK: - Public methods

    /**
     * @param object|string $className
     * @return null|object
     */
    public static function init($className): ?object
    {
        if(is_object($className)) {
            return $className;
        }

        if(is_string($className) && method_exists($className,  '__construct')) {
            return new $className();
        }

        try {
            $class = new ReflectionClass($className);
            return $class->newInstanceArgs();
        } catch (\ReflectionException $ignored) { }

        return null;
    }
}