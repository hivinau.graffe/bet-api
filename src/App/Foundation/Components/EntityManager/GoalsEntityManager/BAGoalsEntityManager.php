<?php

namespace App\Foundation\Components\EntityManager\GoalsEntityManager;

use App\Foundation\Repositories\GoalsRepository\GoalsRepository;
use App\Foundation\Repositories\GoalsRepository\Models\BAGoal;
use App\Foundation\Repositories\GoalsRepository\Models\Goal;
use Doctrine\ORM\EntityManagerInterface;

final class BAGoalsEntityManager implements GoalsEntityManager {

    // MARK: - Private properties

    /**
     * @var GoalsRepository
     */
    private $goalsRepository;

    // MARK: - Init

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->goalsRepository = $entityManager->getRepository(BAGoal::class);
    }

    // MARK: - GoalsEntityManager methods

    public function findMatchGoals(string $matchId): array
    {
        return $this->goalsRepository->findMatchGoals($matchId);
    }

    public function addGoal(Goal $goal): ?Goal
    {
        return $this->goalsRepository->addGoal($goal);
    }
}