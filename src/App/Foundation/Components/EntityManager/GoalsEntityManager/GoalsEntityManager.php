<?php

namespace App\Foundation\Components\EntityManager\GoalsEntityManager;

use App\Foundation\Repositories\GoalsRepository\Models\Goal;

interface GoalsEntityManager {

    /**
     * @param string $matchId
     * @return Goal[]
     */
    function findMatchGoals(string $matchId): array;
    function addGoal(Goal $goal): ?Goal;
}