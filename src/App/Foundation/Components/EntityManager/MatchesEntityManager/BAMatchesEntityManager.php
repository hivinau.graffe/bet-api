<?php

namespace App\Foundation\Components\EntityManager\MatchesEntityManager;

use App\Foundation\Repositories\MatchesRepository\MatchesRepository;
use App\Foundation\Repositories\MatchesRepository\Models\BAMatch;
use App\Foundation\Repositories\MatchesRepository\Models\Match;
use Doctrine\ORM\EntityManagerInterface;

final class BAMatchesEntityManager implements MatchesEntityManager {

    // MARK: - Private properties

    /**
     * @var MatchesRepository
     */
    private $matchesRepository;

    // MARK: - Init

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->matchesRepository = $entityManager->getRepository(BAMatch::class);
    }

    // MARK: - MatchsEntityManager methods

    public function findBetMatches(string $betId): array
    {
        return $this->matchesRepository->findBetMatches($betId);
    }

    public function getMatchesByIds(array $matchesIds): array
    {
        return $this->matchesRepository->getMatchesByIds($matchesIds);
    }

    public function getMatchById(string $matchId): ?Match
    {
        return $this->matchesRepository->getMatchById($matchId);
    }

    public function addMatch(Match $match): ?Match
    {
        return $this->matchesRepository->addMatch($match);
    }
}