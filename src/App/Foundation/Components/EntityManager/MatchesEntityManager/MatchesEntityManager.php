<?php

namespace App\Foundation\Components\EntityManager\MatchesEntityManager;

use App\Foundation\Repositories\MatchesRepository\Models\Match;

interface MatchesEntityManager {

    /**
     * @param string $betId
     * @return Match[]
     */
    function findBetMatches(string $betId): array;

    /**
     * @param string[] $matchIds
     * @return Match[]
     */
    function getMatchesByIds(array $matchesIds): array;
    function getMatchById(string $matchId): ?Match;
    function addMatch(Match $match): ?Match;
}