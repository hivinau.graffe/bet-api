<?php

namespace App\Foundation\Components\EntityManager;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Components\EntityManager\BetsEntityManager\BABetsEntityManager;
use App\Foundation\Components\EntityManager\BetsEntityManager\BetsEntityManager;
use App\Foundation\Components\EntityManager\GoalsEntityManager\BAGoalsEntityManager;
use App\Foundation\Components\EntityManager\GoalsEntityManager\GoalsEntityManager;
use App\Foundation\Components\EntityManager\MatchesEntityManager\BAMatchesEntityManager;
use App\Foundation\Components\EntityManager\MatchesEntityManager\MatchesEntityManager;
use App\Foundation\Components\EntityManager\PlayersEntityManager\BAPlayersEntityManager;
use App\Foundation\Components\EntityManager\PlayersEntityManager\PlayersEntityManager;
use App\Foundation\Components\EntityManager\ProsEntityManager\BAProsEntityManager;
use App\Foundation\Components\EntityManager\ProsEntityManager\ProsEntityManager;
use App\Foundation\Components\EntityManager\TeamsEntityManager\BATeamsEntityManager;
use App\Foundation\Components\EntityManager\TeamsEntityManager\TeamsEntityManager;
use App\Foundation\Routers\Router;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\PhpFileCache;
use Doctrine\DBAL\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Setup;
use Psr\Container\ContainerInterface;
use function DI\factory;
use function DI\get;
use function DI\create;

final class EntityManagerAssembly extends DiAssembly {

    // MARK: - Constants

    private const MODELS = [
        '/var/www/html/src/App/Foundation/Repositories/BetsRepository/Models',
        '/var/www/html/src/App/Foundation/Repositories/GoalsRepository/Models',
        '/var/www/html/src/App/Foundation/Repositories/MatchesRepository/Models',
        '/var/www/html/src/App/Foundation/Repositories/PlayersRepository/Models',
        '/var/www/html/src/App/Foundation/Repositories/ProsRepository/Models',
        '/var/www/html/src/App/Foundation/Repositories/TeamsRepository/Models'
    ];

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $container = $router->getContainer();
        $this->builder->addDefinitions([
            EntityManagerInterface::class => factory([EntityManager::class, 'create'])
                ->parameter('connection', $this->createConnectionFromContainer($container))
                ->parameter('config', $this->createConfig($container)),
            BetsEntityManager::class => create(BABetsEntityManager::class)
                ->constructor(get(EntityManagerInterface::class)),
            TeamsEntityManager::class => create(BATeamsEntityManager::class)
                ->constructor(get(EntityManagerInterface::class)),
            ProsEntityManager::class => create(BAProsEntityManager::class)
                ->constructor(get(EntityManagerInterface::class)),
            GoalsEntityManager::class => create(BAGoalsEntityManager::class)
                ->constructor(get(EntityManagerInterface::class)),
            MatchesEntityManager::class => create(BAMatchesEntityManager::class)
                ->constructor(get(EntityManagerInterface::class)),
            PlayersEntityManager::class => create(BAPlayersEntityManager::class)
                ->constructor(get(EntityManagerInterface::class))
        ]);

        parent::injectDependenciesToRouter($router);
    }

    // MARK: - Private methods

    private function createConnectionFromContainer(ContainerInterface $container): array {
        return [
            'driver'  => 'mysqli',
            'dbname' => $container->get('MYSQL_DATABASE'),
            'user' => $container->get('MYSQL_USER'),
            'password' => $container->get('MYSQL_PASSWORD'),
            'host' => $container->get('MYSQL_HOST')
        ];
    }

    private function createConfig(ContainerInterface $container): Configuration {
        $isDevMode = boolval($container->get('IS_DEV_MODE') ?? 0);

        $config = Setup::createConfiguration($isDevMode);

        $config->setMetadataDriverImpl(
            new AnnotationDriver(new AnnotationReader(), self::MODELS)
        );

        $directoryPath = $container->get('DOCTRINE_PATH');
        $cacheDirectory = sprintf('%s/cache', $directoryPath);

        $cache = $isDevMode ? new ArrayCache(): new PhpFileCache($cacheDirectory);
        $config->setMetadataCacheImpl($cache);

        $cache = $isDevMode ? new ArrayCache(): new PhpFileCache($cacheDirectory);
        $config->setQueryCacheImpl($cache);
        $config->setResultCacheImpl($cache);

        $proxyDirectory = sprintf('%s/proxy', $directoryPath);
        $config->setProxyDir($proxyDirectory);

        return $config;
    }
}