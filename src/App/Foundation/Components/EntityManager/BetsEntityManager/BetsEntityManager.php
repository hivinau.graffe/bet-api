<?php

namespace App\Foundation\Components\EntityManager\BetsEntityManager;

use App\Foundation\Repositories\BetsRepository\Models\Bet;

interface BetsEntityManager {

    /**
     * @return Bet[]
     */
    function findBets(): array;
    function addBet(Bet $bet): ?Bet;
}