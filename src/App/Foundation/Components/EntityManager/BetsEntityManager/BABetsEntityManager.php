<?php

namespace App\Foundation\Components\EntityManager\BetsEntityManager;

use App\Foundation\Repositories\BetsRepository\BetsRepository;
use App\Foundation\Repositories\BetsRepository\Models\BABet;
use App\Foundation\Repositories\BetsRepository\Models\Bet;
use Doctrine\ORM\EntityManagerInterface;

final class BABetsEntityManager implements BetsEntityManager {

    // MARK: - Private properties

    /**
     * @var BetsRepository
     */
    private $betsRepository;

    // MARK: - Init

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->betsRepository = $entityManager->getRepository(BABet::class);
    }

    // MARK: - BetsEntityManager methods

    public function findBets(): array
    {
        return $this->betsRepository->findBets();
    }

    public function addBet(Bet $bet): ?Bet
    {
        return $this->betsRepository->addBet($bet);
    }
}