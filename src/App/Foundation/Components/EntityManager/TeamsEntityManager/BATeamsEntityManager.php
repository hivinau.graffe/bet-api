<?php

namespace App\Foundation\Components\EntityManager\TeamsEntityManager;

use App\Foundation\Repositories\TeamsRepository\Models\BATeam;
use App\Foundation\Repositories\TeamsRepository\Models\Team;
use App\Foundation\Repositories\TeamsRepository\TeamsRepository;
use Doctrine\ORM\EntityManagerInterface;

final class BATeamsEntityManager implements TeamsEntityManager {

    // MARK: - Private properties

    /**
     * @var TeamsRepository
     */
    private $teamsRepository;

    // MARK: - Init

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->teamsRepository = $entityManager->getRepository(BATeam::class);
    }

    // MARK: - TeamsEntityManager methods

    public function getTeamsByIds(array $teamsIds): array
    {
        return $this->teamsRepository->getTeamsByIds($teamsIds);
    }

    public function getTeamById(string $teamId): ?Team
    {
        return $this->teamsRepository->getTeamById($teamId);
    }
    public function addTeam(Team $team): ?Team
    {
        return $this->teamsRepository->addTeam($team);
    }
}