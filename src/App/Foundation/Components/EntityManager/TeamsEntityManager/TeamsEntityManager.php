<?php

namespace App\Foundation\Components\EntityManager\TeamsEntityManager;

use App\Foundation\Repositories\TeamsRepository\Models\Team;

interface TeamsEntityManager {

    /**
     * @param string[] $teamsIds
     * @return Team[]
     */
    function getTeamsByIds(array $teamsIds): array;
    function getTeamById(string $teamId): ?Team;
    function addTeam(Team $team): ?Team;
}