<?php

namespace App\Foundation\Components\EntityManager\ProsEntityManager;

use App\Foundation\Repositories\ProsRepository\Models\Pro;

interface ProsEntityManager {

    /**
     * @param string $matchId
     * @return Pro[]
     */
    function findMatchPros(string $matchId): array;

    /**
     * @param string[] $prosIds
     * @return Pro[]
     */
    function getProsByIds(array $prosIds): array;
    function addPro(Pro $pro): ?Pro;
}