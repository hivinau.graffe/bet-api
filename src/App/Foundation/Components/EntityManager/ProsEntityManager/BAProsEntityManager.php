<?php

namespace App\Foundation\Components\EntityManager\ProsEntityManager;

use App\Foundation\Repositories\ProsRepository\Models\BAPro;
use App\Foundation\Repositories\ProsRepository\Models\Pro;
use App\Foundation\Repositories\ProsRepository\ProsRepository;
use Doctrine\ORM\EntityManagerInterface;

final class BAProsEntityManager implements ProsEntityManager {

    // MARK: - Private properties

    /**
     * @var ProsRepository
     */
    private $prosRepository;

    // MARK: - Init

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->prosRepository = $entityManager->getRepository(BAPro::class);
    }

    // MARK: - ProsEntityManager methods

    public function findMatchPros(string $matchId): array
    {
        return $this->prosRepository->findMatchPros($matchId);
    }

    public function getProsByIds(array $prosIds): array
    {
        return $this->prosRepository->getProsByIds($prosIds);
    }
    public function addPro(Pro $pro): ?Pro
    {
        return $this->prosRepository->addPro($pro);
    }
}