<?php

namespace App\Foundation\Components\EntityManager\PlayersEntityManager;

use App\Foundation\Repositories\PlayersRepository\Models\Player;

interface PlayersEntityManager {
    function addPlayer(Player $player): ?Player;
}