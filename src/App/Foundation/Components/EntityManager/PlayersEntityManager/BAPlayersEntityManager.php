<?php

namespace App\Foundation\Components\EntityManager\PlayersEntityManager;

use App\Foundation\Repositories\PlayersRepository\Models\BAPlayer;
use App\Foundation\Repositories\PlayersRepository\Models\Player;
use App\Foundation\Repositories\PlayersRepository\PlayersRepository;
use Doctrine\ORM\EntityManagerInterface;

final class BAPlayersEntityManager implements PlayersEntityManager {

    // MARK: - Private properties

    /**
     * @var PlayersRepository
     */
    private $repository;

    // MARK: - Init

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(BAPlayer::class);
    }

    // MARK: - PlayersEntityManager methods

    public function addPlayer(Player $player): ?Player
    {
        return $this->repository->addPlayer($player);
    }
}