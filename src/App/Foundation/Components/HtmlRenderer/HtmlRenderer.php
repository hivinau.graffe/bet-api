<?php

namespace App\Foundation\Components\HtmlRenderer;

use Psr\Http\Message\ResponseInterface;

interface HtmlRenderer {
    function renderResponse(ResponseInterface $response, string $name, array $context): ?ResponseInterface;
}