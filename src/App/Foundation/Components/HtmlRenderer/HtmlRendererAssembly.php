<?php

namespace App\Foundation\Components\HtmlRenderer;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Components\HtmlRenderer\Twig\BAView;
use App\Foundation\Components\HtmlRenderer\Twig\View;
use App\Foundation\Routers\Router;
use function DI\create;
use function DI\get;

final class HtmlRendererAssembly extends DiAssembly {

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $container = $router->getContainer();

        $this->builder->addDefinitions([
            View::class => create(BAView::class)
                ->constructor(
                    $container->get('HTML_TEMPLATES_PATH'),
                    $container->get('CACHE_VIEWS_PATH')
                ),
            HtmlRenderer::class => create(BAHtmlRenderer::class)
                ->constructor(get(View::class))
        ]);

        parent::injectDependenciesToRouter($router);
    }
}