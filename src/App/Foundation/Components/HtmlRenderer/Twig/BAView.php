<?php

namespace App\Foundation\Components\HtmlRenderer\Twig;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Slim\Views\Twig;
use Twig\Loader\FilesystemLoader;

final class BAView implements View {

    // MARK: - Private properties

    private ?Twig $twig;

    // MARK: - Init

    public function __construct(string $templatesPath, $cacheViewsPath)
    {
        $loader = new FilesystemLoader($templatesPath);
        $this->twig = new Twig($loader, ['cache' => $cacheViewsPath]);
    }

    // MARK: - Environment methods

    public function renderResponse(ResponseInterface $response, string $name, array $context): ?ResponseInterface
    {
        if(is_null($this->twig)) {
            return null;
        }

        try {
            return $this->twig->render($response, $name, $context);
        } catch (Exception $ignored) {}

        return null;
    }
}