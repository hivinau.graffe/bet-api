<?php

namespace App\Foundation\Components\HtmlRenderer\Twig;

use Psr\Http\Message\ResponseInterface;

interface View {
    function renderResponse(ResponseInterface $response, string $name, array $context): ?ResponseInterface;
}