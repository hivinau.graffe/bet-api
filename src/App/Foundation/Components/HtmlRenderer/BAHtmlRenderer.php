<?php

namespace App\Foundation\Components\HtmlRenderer;

use App\Foundation\Components\HtmlRenderer\Twig\View;
use Psr\Http\Message\ResponseInterface;

final class BAHtmlRenderer implements HtmlRenderer {

    // MARK: - Private properties

    private View $view;

    // MARK: - Init

    public function __construct(View $view)
    {
        $this->view = $view;
    }

    // MARK: - HtmlRenderer methods

    public function renderResponse(ResponseInterface $response, string $name, array $context): ?ResponseInterface
    {
        return $this->view->renderResponse($response, $name, $context);
    }
}