<?php

namespace App\Foundation\Components\ValidatorManager;

use App\Foundation\Components\ValidatorManager\Validators\Validator;

interface ValidatorManager extends Validator {
    function addValidator(Validator $validator): self;
}