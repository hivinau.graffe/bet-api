<?php

namespace App\Foundation\Components\ValidatorManager\Validators;

final class DateValidator extends RegexValidator {

    // MARK: - Constants

    const PATTERN = '^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$';

    // MARK: - Init

    public function __construct()
    {
        parent::__construct(self::PATTERN);
    }
}