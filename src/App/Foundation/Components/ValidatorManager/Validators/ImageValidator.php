<?php

namespace App\Foundation\Components\ValidatorManager\Validators;

final class ImageValidator extends RegexValidator {

    // MARK: - Constants

    const PATTERN = '.+\.(gif|jpe?g|tiff?|png|webp|bmp)$';

    // MARK: - Init

    public function __construct()
    {
        parent::__construct(self::PATTERN);
    }
}