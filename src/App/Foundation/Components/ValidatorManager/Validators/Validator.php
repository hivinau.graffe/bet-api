<?php

namespace App\Foundation\Components\ValidatorManager\Validators;

interface Validator {
    function validate(string $content): bool;
}