<?php

namespace App\Foundation\Components\ValidatorManager\Validators;

final class TimeValidator extends RegexValidator {

    // MARK: - Constants

    const PATTERN = '^(?:2[0-3]|[01][0-9]):[0-5][0-9]$';

    // MARK: - Init

    public function __construct()
    {
        parent::__construct(self::PATTERN);
    }
}