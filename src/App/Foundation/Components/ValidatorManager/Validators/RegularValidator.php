<?php

namespace App\Foundation\Components\ValidatorManager\Validators;

final class RegularValidator extends RegexValidator {

    // MARK: - Init

    public function __construct(int $minLength)
    {
        parent::__construct(sprintf('.{%d,}', $minLength));
    }
}