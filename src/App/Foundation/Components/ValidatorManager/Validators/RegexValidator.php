<?php

namespace App\Foundation\Components\ValidatorManager\Validators;

class RegexValidator implements Validator {

    // MARK: - Private properties

    /**
     * @var string
     */
    private $pattern;

    // MARK: - Init

    public function __construct(string $pattern)
    {
        $this->pattern = $pattern;
    }

    // MARK: - Validator

    public function validate(string $content): bool
    {
        return preg_match(sprintf('/%s/', $this->pattern), $content);
    }
}