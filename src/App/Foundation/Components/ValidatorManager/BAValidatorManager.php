<?php

namespace App\Foundation\Components\ValidatorManager;

use App\Foundation\Components\ValidatorManager\Validators\Validator;

final class BAValidatorManager implements ValidatorManager {

    private $validators;

    public function __construct()
    {
        $this->validators = [];
    }

    public function addValidator(Validator $validator): ValidatorManager
    {
        $this->validators[] = $validator;
        return $this;
    }

    public function validate(string $content): bool
    {
        $isValid = false;
        foreach ($this->validators as $validator)  {
            if(!$validator->validate($content)) {
                continue;
            }

            $isValid = true;
        };

        return $isValid;
    }
}