<?php

namespace App\Foundation\Components\ImagesManager;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Components\ImagesManager\ColorExtractor\BAColorExtractor;
use App\Foundation\Components\ImagesManager\ColorExtractor\ColorExtractor;
use App\Foundation\Components\ImagesManager\Server\BAServer;
use App\Foundation\Components\ImagesManager\Server\Server;
use App\Foundation\Routers\Router;
use function DI\create;
use function DI\get;

final class ImagesManagerAssembly extends DiAssembly {

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $container = $router->getContainer();
        $imagePath = $container->get('IMAGES_PATH');
        $cacheImagePath = $container->get('CACHE_IMAGES_PATH');
        $this->builder->addDefinitions([
            Server::class => create(BAServer::class)
                ->constructor(
                    $imagePath,
                    $cacheImagePath
                ),
            ColorExtractor::class => create(BAColorExtractor::class),
            ImagesManager::class => create(BAImagesManager::class)
                ->constructor(
                    $imagePath,
                    get(Server::class),
                    get(ColorExtractor::class)
                )
        ]);

        parent::injectDependenciesToRouter($router);
    }
}