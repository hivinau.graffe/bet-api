<?php

namespace App\Foundation\Components\ImagesManager\Server;

use League\Glide\Responses\PsrResponseFactory;
use League\Glide\ServerFactory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Slim\Psr7\Response;
use Slim\Psr7\Stream;

final class BAServer implements Server {

    // MARK: - Private properties

    /**
     * @var \League\Glide\Server
     */
    private $server;

    // MARK: - Init

    public function __construct(string $sourceDirectory, string $cacheDirectory)
    {
        $this->server = ServerFactory::create([
            'driver' => 'imagick',
            'source' => $sourceDirectory,
            'cache' => $cacheDirectory,
            'response' => new PsrResponseFactory(new Response(), function ($stream): StreamInterface {
                return new Stream($stream);
            }),
        ]);
    }

    // MARK: - Server methods

    public function getImageResponse(string $path, array $params): ResponseInterface
    {
        return $this->server->getImageResponse($path, $params);
    }
}