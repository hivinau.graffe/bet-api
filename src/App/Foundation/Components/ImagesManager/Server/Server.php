<?php

namespace App\Foundation\Components\ImagesManager\Server;

use Psr\Http\Message\ResponseInterface;

interface Server {
    public function getImageResponse(string $path, array $params): ResponseInterface;
}