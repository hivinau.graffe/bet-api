<?php

namespace App\Foundation\Components\ImagesManager;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UploadedFileInterface;

interface ImagesManager {
    function uploadImage(UploadedFileInterface $file): array;
    function getImage(string $filename, int $width, int $height): ?ResponseInterface;
}