<?php

namespace App\Foundation\Components\ImagesManager;

use App\Foundation\Components\ImagesManager\ColorExtractor\ColorExtractor;
use App\Foundation\Components\ImagesManager\Server\Server;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UploadedFileInterface;

final class BAImagesManager implements ImagesManager {

    // MARK: Private properties

    /**
     * @var Server
     */
    private $server;

    /**
     * @var string
     */
    private $imagesDirectory;

    /**
     * @var ColorExtractor
     */
    private $colorExtractor;

    // MARK: Init

    public function __construct(string $imagesDirectory,
                                Server $server,
                                ColorExtractor $colorExtractor)
    {
        $this->imagesDirectory = $imagesDirectory;
        $this->server = $server;
        $this->colorExtractor = $colorExtractor;
    }

    // MARK: ImagesManager methods

    public function uploadImage(UploadedFileInterface $file): array
    {
        try {
            $extension = pathinfo($file->getClientFilename(), PATHINFO_EXTENSION);
            $basename = bin2hex(random_bytes(8));
            $filename = sprintf('%s.%0.8s', $basename, $extension);

            $filePath = $this->imagesDirectory . DIRECTORY_SEPARATOR . $filename;
            $file->moveTo($filePath);

            $colors = $this->colorExtractor->extractHexColorFromImagePath($filePath);
            return [
                'filename' => $filename,
                'colors' => $colors
            ];

        } catch (Exception $exception) {}

        return [];
    }

    public function getImage(string $filename, int $width, int $height): ?ResponseInterface
    {
        $options = [];
        if($width !== -1) {
            $options['w'] = $width;
        }
        if($height !== -1) {
            $options['h'] = $height;
        }

        try {
            return $this->server->getImageResponse($filename, $options);
        } catch (Exception $exception) {}

        return null;

    }
}