<?php

namespace App\Foundation\Components\ImagesManager\ColorExtractor;

use League\ColorExtractor\Color;
use League\ColorExtractor\ColorExtractor as Extractor;
use League\ColorExtractor\Palette;

final class BAColorExtractor implements ColorExtractor {

    // MARK: - Constants

    private const BACKGROUND = 'background';
    private const PRIMARY = 'primary';
    private const SECONDARY = 'secondary';
    private const DETAIL = 'detail';

     // MARK: - ColorExtractor methods

    public function extractHexColorFromImagePath(string $imagePath): array
    {
        $colors = $this->extractFromFilename($imagePath, 4);
        $hexColors = [];

        if(count($colors) > 0) {
            $hexColors[self::BACKGROUND] = Color::fromIntToHex($colors[0]);
        }

        if(count($colors) > 1) {
            $hexColors[self::PRIMARY] = Color::fromIntToHex($colors[1]);
        }

        if(count($colors) > 2) {
            $hexColors[self::SECONDARY] = Color::fromIntToHex($colors[2]);
        }

        if(count($colors) > 3) {
            $hexColors[self::DETAIL] = Color::fromIntToHex($colors[3]);
        }

        return $hexColors;
    }

    // MARK: - Private methods

    private function extractFromFilename(string $filename, int $index): array
    {
        $extractor = new Extractor(Palette::fromFilename($filename));
        return $extractor->extract($index);
    }
}