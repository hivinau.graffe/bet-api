<?php

namespace App\Foundation\Components\ImagesManager\ColorExtractor;

interface ColorExtractor {
    function extractHexColorFromImagePath(string $imagePath): array;
}