<?php

namespace App\Foundation\Components\StringExploder;

final class BAStringExploder implements StringExploder {

    private const PATTERN = '/(\s*,*\s*)*,+(\s*,*\s*)*/';

    public function explode(string $subject): array
    {
        return preg_split(self::PATTERN, $subject);
    }
}