<?php

namespace App\Foundation\Components\StringExploder;

interface StringExploder {
    function explode(string $subject): array;
}