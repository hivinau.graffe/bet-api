<?php

namespace App\Foundation\Components;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Components\CertificateInspector\CertificateInspectorAssembly;
use App\Foundation\Components\DatetimeManager\BADatetimeManager;
use App\Foundation\Components\DatetimeManager\DatetimeManager;
use App\Foundation\Components\EntityManager\EntityManagerAssembly;
use App\Foundation\Components\HtmlRenderer\HtmlRendererAssembly;
use App\Foundation\Components\ImagesManager\ImagesManagerAssembly;
use App\Foundation\Components\StringExploder\BAStringExploder;
use App\Foundation\Components\StringExploder\StringExploder;
use App\Foundation\Components\TokenGenerator\TokenGeneratorAssembly;
use App\Foundation\Components\ValidatorManager\BAValidatorManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Routers\Router;
use function DI\create;

final class ComponentsAssembly extends DiAssembly {

    // MARK: - Constants

    private const ASSEMBLIES = [
        EntityManagerAssembly::class,
        ImagesManagerAssembly::class,
        TokenGeneratorAssembly::class,
        CertificateInspectorAssembly::class,
        HtmlRendererAssembly::class
    ];

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $this->injectSubAssembliesToRouter(self::ASSEMBLIES, $router);

        $this->builder->addDefinitions([
            ValidatorManager::class => create(BAValidatorManager::class),
            DatetimeManager::class => create(BADatetimeManager::class),
            StringExploder::class => create(BAStringExploder::class)
        ]);

        parent::injectDependenciesToRouter($router);
    }
}