<?php

namespace App\Foundation\Components\CertificateInspector;

final class BACertificateInspector implements CertificateInspector {

    // MARK: - Constants

    private const CERTIFICATE_PATH = '/var/www/html/uploads';

    // MARK: - Private properties

    /**
     * @var string
     */
    private $teamId;

    /**
     * @var string
     */
    private $bundleId;

    // MARK: - Init

    public function __construct(string $teamId, string $bundleId)
    {
        $this->teamId = utf8_encode($teamId);
        $this->bundleId = utf8_encode($bundleId);
    }

    // MARK: - CertificateInspector methods

    public function verifyAppleSignatureUrl(string $url, string $signature, string $salt, string $timestamp): bool
    {
        $pem = $this->loadCertificate($url);
        $keyId = openssl_pkey_get_public($pem);

        if($keyId === false) {
            return false;
        }

        $signature = base64_decode($signature);
        $salt = base64_decode($salt);

        $payload = sprintf("%s%s%s%s", $this->teamId, $this->bundleId, $this->toBigEndian($timestamp), $salt);

        $verified = boolval(openssl_verify($payload, $signature, $keyId,OPENSSL_ALGO_SHA256));

        openssl_free_key($keyId);

        return $verified;
    }

    private function loadCertificate(string $url)
    {
        $filename = basename($url);
        $path = sprintf('%s/%s', self::CERTIFICATE_PATH, $filename);
        $file = fopen($path, 'w+');

        $resource = curl_init($url);
        curl_setopt($resource, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($resource, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($resource, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($resource, CURLOPT_FILE, $file);

        curl_exec($resource);

        fclose($file);
        curl_close($resource);

        return $this->encodeCertificate($path);
    }

    private function encodeCertificate(string $path)
    {
        $file = file_get_contents($path);
        $pem = chunk_split(base64_encode($file), 64, "\n");
        return "-----BEGIN CERTIFICATE-----\n{$pem}-----END CERTIFICATE-----\n";
    }

    private function toBigEndian($timestamp)
    {
        if (PHP_INT_SIZE === 4) {
            $hex = '';

            do {
                $last = bcmod($timestamp, 16);
                $hex = dechex($last) . $hex;
                $timestamp = bcdiv(bcsub($timestamp, $last), 16);
            } while ($timestamp > 0);

            return hex2bin(str_pad($hex, 16, '0', STR_PAD_LEFT));
        }

        $highMap = 0xffffffff00000000;
        $lowMap = 0x00000000ffffffff;
        $higher = ($timestamp & $highMap) >>32;
        $lower = $timestamp & $lowMap;

        return pack('N2', $higher, $lower);
    }
}