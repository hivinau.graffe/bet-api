<?php

namespace App\Foundation\Components\CertificateInspector;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Routers\Router;
use function DI\create;

final class CertificateInspectorAssembly extends DiAssembly {

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $container = $router->getContainer();
        $mockCertificateInspector = boolval($container->get('MOCK_CERTIFICATE_INSPECTOR') ?? 0);
        if($mockCertificateInspector) {
            $certificateInspector = create(MockCertificateInspector::class);
        } else {
            $certificateInspector = create(BACertificateInspector::class)
            ->constructor(
                $container->get('TEAM_ID'),
                $container->get('BUNDLE_ID')
            );
        }

        $this->builder->addDefinitions([
            CertificateInspector::class => $certificateInspector
        ]);

        parent::injectDependenciesToRouter($router);
    }
}