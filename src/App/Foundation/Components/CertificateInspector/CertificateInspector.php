<?php

namespace App\Foundation\Components\CertificateInspector;

interface CertificateInspector {
    function verifyAppleSignatureUrl(string $url, string $signature, string $salt, string $timestamp): bool;
}