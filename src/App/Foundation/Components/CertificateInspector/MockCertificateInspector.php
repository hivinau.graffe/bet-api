<?php

namespace App\Foundation\Components\CertificateInspector;

final class MockCertificateInspector implements CertificateInspector {

    // MARK: - CertificateInspector methods

    public function verifyAppleSignatureUrl(string $url, string $signature, string $salt, string $timestamp): bool
    {
        return true;
    }
}