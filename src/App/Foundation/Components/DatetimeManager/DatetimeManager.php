<?php

namespace App\Foundation\Components\DatetimeManager;

use DateTimeInterface;

interface DatetimeManager {
    function createDate(string $date): DateTimeInterface;
    function createTime(string $time): DateTimeInterface;
}