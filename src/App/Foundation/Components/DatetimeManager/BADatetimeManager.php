<?php

namespace App\Foundation\Components\DatetimeManager;

use DateTime;
use DateTimeInterface;

final class BADatetimeManager implements DatetimeManager {

    // MARK: - Constants

    private const DATE_FORMAT = 'Y-m-d';
    private const TIME_FORMAT = 'H:i';

    // MARK: - DatetimeManager methods

    public function createDate(string $date): DateTimeInterface
    {
        return DateTime::createFromFormat(self::DATE_FORMAT, $date);
    }

    public function createTime(string $time): DateTimeInterface
    {
        return DateTime::createFromFormat(self::TIME_FORMAT, $time);
    }
}