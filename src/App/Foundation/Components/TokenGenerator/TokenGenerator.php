<?php

namespace App\Foundation\Components\TokenGenerator;

interface TokenGenerator {
    function tokenizeSignature(string $signature, string $salt, string$timestamp): ?string;
}