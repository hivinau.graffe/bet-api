<?php

namespace App\Foundation\Components\TokenGenerator;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Components\TokenGenerator\Jwt\BAJwt;
use App\Foundation\Components\TokenGenerator\Jwt\Jwt;
use App\Foundation\Routers\Router;
use function DI\create;
use function DI\get;

final class TokenGeneratorAssembly extends DiAssembly {

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $container = $router->getContainer();

        $this->builder->addDefinitions([
            Jwt::class => create(BAJwt::class)
                ->constructor($container->get('JWT_KEY')),
            TokenGenerator::class => create(BATokenGenerator::class)
                ->constructor(get(Jwt::class))
        ]);

        parent::injectDependenciesToRouter($router);
    }
}