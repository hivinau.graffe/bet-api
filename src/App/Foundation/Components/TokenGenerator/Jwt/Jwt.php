<?php

namespace App\Foundation\Components\TokenGenerator\Jwt;

interface Jwt {
    function encode(array $payload): string;
}