<?php

namespace App\Foundation\Components\TokenGenerator\Jwt;

use Firebase\JWT\JWT as FireBaseJwt;

final class BAJwt implements Jwt {

    // MARK: - Private properties

    /**
     * @var string
     */
    private $jwtKey;

    // MARK: - Init

    public function __construct(string $jwtKey)
    {
        $this->jwtKey = $jwtKey;
    }

    // MARK: - Jwt methods

    public function encode(array $payload): string
    {
        return FireBaseJwt::encode($payload, $this->jwtKey);
    }
}