<?php

namespace App\Foundation\Components\TokenGenerator;

use App\Foundation\Components\TokenGenerator\Jwt\Jwt;

final class BATokenGenerator implements TokenGenerator {

    // MARK: - Private properties

    /**
     * @var Jwt
     */
    private $jwt;

    // MARK: - Init

    public function __construct(Jwt $jwt)
    {
        $this->jwt = $jwt;
    }

    // MARK: - TokenGenerator methods

    public function tokenizeSignature(string $signature, string $salt, string $timestamp): ?string
    {
        $payload = [
            "signature" => $signature,
            "salt" => $salt,
            "timestamp" => $timestamp
        ];

        return $this->jwt->encode($payload);
    }
}