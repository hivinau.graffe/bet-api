<?php

namespace App\Foundation\Repositories\BetsRepository\Models;

use App\Foundation\Repositories\MatchesRepository\Models\Match;
use JsonSerializable;

interface Bet extends JsonSerializable {
    /**
     * @return Match[]
     */
    function getMatches(): array;
    function getId(): ?string;
    function isLocked(): bool;
    function getPriceToUnlock(): float;
    function isPosted(): bool;
    function addMatch(Match $match): Bet;
}