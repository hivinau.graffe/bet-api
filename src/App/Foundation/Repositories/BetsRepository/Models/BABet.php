<?php

namespace App\Foundation\Repositories\BetsRepository\Models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Id\UuidGenerator;
use App\Foundation\Repositories\BetsRepository\BABetsRepository;
use App\Foundation\Repositories\MatchesRepository\Models\Match;

/**
 * @ORM\Entity(repositoryClass=BABetsRepository::class)
 * @ORM\Table(name="bets")
 */
class BABet implements Bet {

    // MARK: - Private properties

    /**
     * @ORM\ManyToMany(targetEntity="App\Foundation\Repositories\MatchesRepository\Models\BAMatch", inversedBy="bets")
     * @ORM\JoinTable(name="bets_matches",
     *     joinColumns={@ORM\JoinColumn(name="bet_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="match_id", referencedColumnName="id")})
     * @var PersistentCollection
     */
    private $matches;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @var string
     */
    private $id;

    /**
     * @ORM\Column(name="locked", type="boolean", options={"default": false})
     * @var bool
     */
    private $locked = false;

    /**
     * @ORM\Column(name="price_to_unlock", type="float", options={"default": 0})
     * @var float
     */
    private $priceToUnlock = 0;

    /**
     * @ORM\Column(name="posted", type="boolean", options={"default": false})
     * @var bool
     */
    private $posted = false;

    // MARK: - Init

    public function __construct()
    {
        $this->matches = new ArrayCollection();
    }

    // MARK: - Bet methods

    public function addMatch(Match $match): Bet
    {
        $this->matches[] = $match;
        return $this;
    }

    public function getMatches(): array
    {
        return $this->matches->getValues();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function isLocked(): bool
    {
        return $this->locked;
    }

    public function getPriceToUnlock(): float
    {
        return $this->priceToUnlock;
    }

    public function isPosted(): bool
    {
        return $this->posted;
    }

    // MARK: - JsonSerializable methods

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'is_locked' => $this->locked,
            'price_to_unlock' => $this->priceToUnlock,
            'matches_count' => count($this->getMatches())
        ];
    }
}