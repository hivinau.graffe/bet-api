<?php

namespace App\Foundation\Repositories\BetsRepository;

use App\Foundation\Repositories\BetsRepository\Models\Bet;

interface BetsRepository {

    /**
     * @return Bet[]
     */
    function findBets(): array;
    function addBet(Bet $bet): ?Bet;
}