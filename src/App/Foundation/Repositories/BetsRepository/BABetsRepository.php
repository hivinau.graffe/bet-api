<?php

namespace App\Foundation\Repositories\BetsRepository;

use App\Foundation\Repositories\BaseRepository;
use App\Foundation\Repositories\BetsRepository\Models\BABet;
use App\Foundation\Repositories\BetsRepository\Models\Bet;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;

final class BABetsRepository extends BaseRepository implements BetsRepository {

    // MARK: - Constants

    private const CACHE_LIFETIME = 120;

    // MARK: - Init

    public function __construct(EntityManagerInterface $entityManager)
    {
        $class = $entityManager->getClassMetadata(BABet::class);
        parent::__construct($entityManager, $class);
    }

    // MARK: - BetsRepository methods

    public function findBets(): array
    {
        $builder = $this->createQueryBuilder('bet');

        return $builder
            ->where($builder->expr()->like('bet.posted', ':posted'))
            ->setParameter('posted', true)
            ->setCacheable(true)
            ->getQuery()
            ->setHint(Query::HINT_READ_ONLY, true)
            ->setResultCacheLifetime(self::CACHE_LIFETIME)
            ->setResultCacheId('bets')
            ->getResult();
    }

    public function addBet(Bet $bet): ?Bet
    {
        return $this->addEntity($bet) ? $bet : null;
    }
}