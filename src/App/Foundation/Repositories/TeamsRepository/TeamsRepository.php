<?php

namespace App\Foundation\Repositories\TeamsRepository;

use App\Foundation\Repositories\TeamsRepository\Models\Team;

interface TeamsRepository {

    /**
     * @param string[] $teamsIds
     * @return Team[]
     */
    function getTeamsByIds(array $teamsIds): array;
    function getTeamById(string $teamId): ?Team;
    function addTeam(Team $team): ?Team;
}