<?php

namespace App\Foundation\Repositories\TeamsRepository;

use App\Foundation\Repositories\BaseRepository;
use App\Foundation\Repositories\TeamsRepository\Models\BATeam;
use App\Foundation\Repositories\TeamsRepository\Models\Team;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;

final class BATeamsRepository extends BaseRepository implements TeamsRepository {

    // MARK: - Constants

    private const CACHE_LIFETIME = 120;

    // MARK: - Init

    public function __construct(EntityManagerInterface $entityManager)
    {
        $class = $entityManager->getClassMetadata(BATeam::class);
        parent::__construct($entityManager, $class);
    }

    // MARK: - TeamsRepository methods

    public function getTeamsByIds(array $teamsIds): array
    {
        $builder = $this->createQueryBuilder('team');

        return $builder
            ->where($builder->expr()->in('team.id', ':teamsIds'))
            ->setParameter('teamsIds', $teamsIds)
            ->setCacheable(true)
            ->getQuery()
            ->setHint(Query::HINT_READ_ONLY, true)
            ->setResultCacheLifetime(self::CACHE_LIFETIME)
            ->setResultCacheId(implode(',', $teamsIds))
            ->getResult();
    }

    public function getTeamById(string $teamId): ?Team
    {
        try {
            $builder = $this->createQueryBuilder('team');

            return $builder
                ->where($builder->expr()->like('team.id', ':teamId'))
                ->setParameter('teamId', $teamId)
                ->setCacheable(true)
                ->getQuery()
                ->setHint(Query::HINT_READ_ONLY, true)
                ->setResultCacheLifetime(self::CACHE_LIFETIME)
                ->setResultCacheId($teamId)
                ->getSingleResult();
        } catch (NoResultException | NonUniqueResultException $ignored) { }

        return null;
    }

    public function addTeam(Team $team): ?Team
    {
        return $this->addEntity($team) ? $team : null;
    }
}