<?php

namespace App\Foundation\Repositories\TeamsRepository\Models;

use App\Foundation\Repositories\GoalsRepository\Models\Goal;
use App\Foundation\Repositories\MatchesRepository\Models\Match;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Id\UuidGenerator;
use App\Foundation\Repositories\TeamsRepository\BATeamsRepository;

/**
 * @ORM\Entity(repositoryClass=BATeamsRepository::class)
 * @ORM\Table(name="teams")
 */
class BATeam implements Team {

    // MARK: - Private properties

    /**
     * @ORM\Column(name="name", type="string")
     * @var string
     */
    private $name;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @var string
     */
    private $id;

    /**
     * @ORM\Column(name="image", type="string")
     * @var string|null
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity="App\Foundation\Repositories\MatchesRepository\Models\BAMatch", mappedBy="teams", fetch="EXTRA_LAZY")
     * @var PersistentCollection
     */
    private $matches;

    /**
     * @ORM\OneToMany(targetEntity="App\Foundation\Repositories\GoalsRepository\Models\BAGoal", mappedBy="team")
     * @var PersistentCollection
     */
    private $goals;

    /**
     * @ORM\Column(name="image_background_color", type="string")
     * @var string|null
     */
    private $imageBackgroundColor;

    /**
     * @ORM\Column(name="image_primary_color", type="string")
     * @var string|null
     */
    private $imagePrimaryColor;

    /**
     * @ORM\Column(name="image_secondary_color", type="string")
     * @var string|null
     */
    private $imageSecondaryColor;

    /**
     * @ORM\Column(name="image_detail_color", type="string")
     * @var string|null
     */
    private $imageDetailColor;

    // MARK: - Init

    public function __construct()
    {
        $this->matches = new ArrayCollection();
        $this->goals = new ArrayCollection();
    }

    // MARK: - Team methods

    public function addMatch(Match $match): Team
    {
        $this->matches[] = $match;
        return $this;
    }

    public function addGoal(Goal $goal): Team
    {
        $this->goals[] = $goal;
        return $this;
    }

    public function setName(string $name): Team
    {
        $this->name = $name;
        return $this;
    }

    public function setImage(?string $image): Team
    {
        $this->image = $image;
        return $this;
    }

    function setImageBackgroundColor(?string $color): Team
    {
        $this->imageBackgroundColor = $color;
        return $this;
    }

    function setImagePrimaryColor(?string $color): Team
    {
        $this->imagePrimaryColor = $color;
        return $this;
    }

    function setImageSecondaryColor(?string $color): Team
    {
        $this->imageSecondaryColor = $color;
        return $this;
    }

    function setImageDetailColor(?string $color): Team
    {
        $this->imageDetailColor = $color;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getMatches(): array
    {
        return $this->matches->getValues();
    }

    public function getGoals(): array
    {
        return $this->goals->getValues();
    }

    function getImageBackgroundColor(): ?string
    {
        return $this->imageBackgroundColor;
    }

    function getImagePrimaryColor(): ?string
    {
        return $this->imagePrimaryColor;
    }

    function getImageSecondaryColor(): ?string
    {
        return $this->imageSecondaryColor;
    }

    function getImageDetailColor(): ?string
    {
        return $this->imageDetailColor;
    }

    // MARK: - JsonSerializable methods

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'colors' => [
                'background' => $this->imageBackgroundColor,
                'primary' => $this->imagePrimaryColor,
                'secondary' => $this->imageSecondaryColor,
                'detail' => $this->imageDetailColor,
            ]
        ];
    }
}