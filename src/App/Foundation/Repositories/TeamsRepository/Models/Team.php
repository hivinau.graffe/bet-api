<?php

namespace App\Foundation\Repositories\TeamsRepository\Models;

use App\Foundation\Repositories\GoalsRepository\Models\Goal;
use App\Foundation\Repositories\MatchesRepository\Models\Match;
use JsonSerializable;

interface Team extends JsonSerializable {
    /**
     * @return Match[]
     */
    function getMatches(): array;

    /**
     * @return Goal[]
     */
    function getGoals(): array;
    function getName(): ?string;
    function getId(): ?string;
    function setImage(?string $image): Team;
    function getImage(): ?string;
    function addMatch(Match $match): Team;
    function addGoal(Goal $goal): Team;
    function setName(string $name): Team;
    function getImageBackgroundColor(): ?string;
    function getImagePrimaryColor(): ?string;
    function getImageSecondaryColor(): ?string;
    function getImageDetailColor(): ?string;
    function setImageBackgroundColor(?string $color): Team;
    function setImagePrimaryColor(?string $color): Team;
    function setImageSecondaryColor(?string $color): Team;
    function setImageDetailColor(?string $color): Team;
}