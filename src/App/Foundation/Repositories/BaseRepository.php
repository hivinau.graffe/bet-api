<?php

namespace App\Foundation\Repositories;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;

abstract class BaseRepository extends EntityRepository {

    /**
     * @param object $entity
     * @return bool
     */
    public function addEntity($entity): bool {
        try {
            $this->_em->persist($entity);
            $this->_em->flush($entity);

            return true;
        } catch (ORMException $ignored) { }

        return false;
    }
}