<?php

namespace App\Foundation\Repositories\PlayersRepository\Models;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Id\UuidGenerator;
use App\Foundation\Repositories\PlayersRepository\BAPlayersRepository;

/**
 * @ORM\Entity(repositoryClass=BAPlayersRepository::class)
 * @ORM\Table(name="players")
 */
class BAPlayer implements Player {

    // MARK: - Private properties

    /**
     * @ORM\Column(name="token", type="string")
     * @var string
     */
    private $token;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @var string
     */
    private $id;

    /**
     * @ORM\Column(name="revoked", type="boolean", options={"default": false})
     * @var bool
     */
    private $revoked = false;

    // MARK: - Public methods

    public function setToken(string $token): self
    {
        $this->token = $token;
        return $this;
    }

    public function setRevoked(bool $revoked): self
    {
        $this->revoked = $revoked;
        return $this;
    }

    // MARK: - Pro methods

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function isRevoked(): bool
    {
        return $this->revoked;
    }

    // MARK: - JsonSerializable methods

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'token' => $this->token,
            'is_revoked' => $this->revoked,
        ];
    }
}