<?php

namespace App\Foundation\Repositories\PlayersRepository\Models;

use JsonSerializable;

interface Player extends JsonSerializable {
    function getToken(): ?string;
    function getId(): ?string;
    function isRevoked(): bool;
}