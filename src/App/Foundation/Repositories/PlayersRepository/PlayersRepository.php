<?php

namespace App\Foundation\Repositories\PlayersRepository;

use App\Foundation\Repositories\PlayersRepository\Models\Player;

interface PlayersRepository {
    function addPlayer(Player $player): ?Player;
}