<?php

namespace App\Foundation\Repositories\PlayersRepository;

use App\Foundation\Repositories\BaseRepository;
use App\Foundation\Repositories\PlayersRepository\Models\BAPlayer;
use App\Foundation\Repositories\PlayersRepository\Models\Player;
use Doctrine\ORM\EntityManagerInterface;

final class BAPlayersRepository extends BaseRepository implements PlayersRepository {

    // MARK: - Init

    public function __construct(EntityManagerInterface $entityManager)
    {
        $class = $entityManager->getClassMetadata(BAPlayer::class);
        parent::__construct($entityManager, $class);
    }

    // MARK: - PlayersRepository methods

    public function addPlayer(Player $player): ?Player
    {
        return $this->addEntity($player) ? $player : null;
    }
}