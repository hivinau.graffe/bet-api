<?php

namespace App\Foundation\Repositories\MatchesRepository\Models;

use App\Foundation\Repositories\BetsRepository\Models\Bet;
use App\Foundation\Repositories\GoalsRepository\Models\Goal;
use App\Foundation\Repositories\ProsRepository\Models\Pro;
use App\Foundation\Repositories\TeamsRepository\Models\Team;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Id\UuidGenerator;
use App\Foundation\Repositories\MatchesRepository\BAMatchesRepository;

/**
 * @ORM\Entity(repositoryClass=BAMatchesRepository::class)
 * @ORM\Table(name="matches")
 */
class BAMatch implements Match {

    // MARK: - Private properties

    /**
     * @ORM\ManyToMany(targetEntity="App\Foundation\Repositories\ProsRepository\Models\BAPro", inversedBy="matches")
     * @ORM\JoinTable(name="matches_pros",
     *     joinColumns={@ORM\JoinColumn(name="match_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="pro_id", referencedColumnName="id")})
     * @var PersistentCollection
     */
    private $pros;

    /**
     * @ORM\ManyToMany(targetEntity="App\Foundation\Repositories\BetsRepository\Models\BABet", mappedBy="matches", fetch="EXTRA_LAZY")
     * @var PersistentCollection
     */
    private $bets;

    /**
     * @ORM\OneToMany(targetEntity="App\Foundation\Repositories\GoalsRepository\Models\BAGoal", mappedBy="match")
     * @var PersistentCollection
     */
    private $goals;

    /**
     * @ORM\Column(name="live", type="boolean", options={"default": false})
     * @var bool
     */
    private $live = false;

    /**
     * @ORM\Column(name="started", type="boolean", options={"default": false})
     * @var bool
     */
    private $started = false;

    /**
     * @ORM\Column(name="date", type="date")
     * @var DateTimeInterface
     */
    private $date;

    /**
     * @ORM\Column(name="time", type="time")
     * @var DateTimeInterface
     */
    private $time;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @var string
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Foundation\Repositories\TeamsRepository\Models\BATeam", inversedBy="matches", fetch="EAGER")
     * @ORM\JoinTable(name="matches_teams",
     *     joinColumns={@ORM\JoinColumn(name="match_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="team_id", referencedColumnName="id")})
     * @var PersistentCollection
     */
    private $teams;

    // MARK: - Init

    public function __construct()
    {
        $this->bets = new ArrayCollection();
        $this->pros = new ArrayCollection();
        $this->teams = new ArrayCollection();
        $this->goals = new ArrayCollection();
    }

    // MARK: - Match methods

    public function addTeam(Team $team): Match
    {
        $this->teams[] = $team;
        return $this;
    }

    public function addPro(Pro $pro): Match
    {
        $this->pros[] = $pro;
        return $this;
    }

    public function addGoal(Goal $goal): Match
    {
        $this->goals[] = $goal;
        return $this;
    }

    public function setLive(bool $live): Match
    {
        $this->live = $live;
        return $this;
    }

    public function setStarted(bool $started): Match
    {
        $this->started = $started;
        return $this;
    }

    public function setDate(DateTimeInterface $date): Match
    {
        $this->date = $date;
        return $this;
    }

    public function setTime(DateTimeInterface $time): Match
    {
        $this->time = $time;
        return $this;
    }

    public function setHome(Team $home): Match
    {
        $this->home = $home;
        return $this;
    }

    public function setVisitor(Team $visitor): Match
    {
        $this->visitor = $visitor;
        return $this;
    }

    public function addBet(Bet $bet): Match
    {
        $this->bets[] = $bet;
        return $this;
    }

    public function getPros(): array
    {
        return $this->pros->getValues();
    }

    public function isLive(): bool
    {
        return $this->live;
    }

    public function isStarted(): bool
    {
        return $this->started;
    }

    public function getDate(): DateTimeInterface
    {
        return $this->date;
    }

    public function getTime(): DateTimeInterface
    {
        return $this->time;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTeams(): array
    {
        return $this->teams->getValues();
    }

    public function getBets(): array
    {
        return $this->bets->getValues();
    }

    public function getGoals(): array
    {
        return $this->goals->getValues();
    }

    // MARK: - JsonSerializable methods

    public function jsonSerialize(): array
    {
        return [
            'is_live' => $this->live,
            'is_started' => $this->started,
            'date' => $this->date->format('Y-m-d'),
            'time' => $this->time->format('H:i'),
            'id' => $this->id,
            'teams' => $this->getTeams(),
            'pros_count' => count($this->getPros())
        ];
    }
}