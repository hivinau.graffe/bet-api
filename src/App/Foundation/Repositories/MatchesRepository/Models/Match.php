<?php

namespace App\Foundation\Repositories\MatchesRepository\Models;

use App\Foundation\Repositories\BetsRepository\Models\Bet;
use App\Foundation\Repositories\GoalsRepository\Models\Goal;
use App\Foundation\Repositories\ProsRepository\Models\Pro;
use App\Foundation\Repositories\TeamsRepository\Models\Team;
use DateTimeInterface;
use JsonSerializable;

interface Match extends JsonSerializable {
    /**
     * @return Pro[]
     */
    function getPros(): array;

    /**
     * @return Bet[]
     */
    function getBets(): array;

    /**
     * @return Team[]
     */
    function getTeams(): array;
    function isLive(): bool;
    function isStarted(): bool;
    function getDate(): DateTimeInterface;
    function getTime(): DateTimeInterface;
    function getId(): ?string;
    function addTeam(Team $team): Match;
    function addPro(Pro $pro): Match;
    function addGoal(Goal $goal): Match;
    function setLive(bool $live): Match;
    function setStarted(bool $started): Match;
    function setDate(DateTimeInterface $date): Match;
    function setTime(DateTimeInterface $time): Match;
    function setHome(Team $home): Match;
    function setVisitor(Team $visitor): Match;
    function addBet(Bet $bet): Match;
}