<?php

namespace App\Foundation\Repositories\MatchesRepository;

use App\Foundation\Repositories\BaseRepository;
use App\Foundation\Repositories\MatchesRepository\Models\BAMatch;
use App\Foundation\Repositories\MatchesRepository\Models\Match;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;

final class BAMatchesRepository extends BaseRepository implements MatchesRepository {

    // MARK: - Constants

    private const CACHE_LIFETIME = 120;

    // MARK: - Init

    public function __construct(EntityManagerInterface $entityManager)
    {
        $class = $entityManager->getClassMetadata(BAMatch::class);
        parent::__construct($entityManager, $class);
    }

    // MARK: - MatchesRepository methods

    public function findBetMatches(string $betId): array
    {
        $builder = $this->createQueryBuilder('match');

        return $builder
            ->leftJoin('match.bets', 'bet')
            ->where($builder->expr()->like('bet.id', ':betId'))
            ->setParameter('betId', $betId)
            ->setCacheable(true)
            ->getQuery()
            ->setHint(Query::HINT_READ_ONLY, true)
            ->setResultCacheLifetime(self::CACHE_LIFETIME)
            ->setResultCacheId("$betId::matches")
            ->getResult();
    }

    public function getMatchesByIds(array $matchesIds): array
    {
        $builder = $this->createQueryBuilder('match');

        return $builder
            ->where($builder->expr()->in('match.id', ':matchesIds'))
            ->setParameter('matchesIds', $matchesIds)
            ->setCacheable(true)
            ->getQuery()
            ->setHint(Query::HINT_READ_ONLY, true)
            ->setResultCacheLifetime(self::CACHE_LIFETIME)
            ->setResultCacheId(implode(',', $matchesIds))
            ->getResult();
    }

    public function getMatchById(string $matchId): ?Match
    {
        try {
            $builder = $this->createQueryBuilder('match');

            return $builder
                ->where($builder->expr()->like('match.id', ':matchId'))
                ->setParameter('matchId', $matchId)
                ->setCacheable(true)
                ->getQuery()
                ->setHint(Query::HINT_READ_ONLY, true)
                ->setResultCacheLifetime(self::CACHE_LIFETIME)
                ->setResultCacheId($matchId)
                ->getSingleResult();
        } catch (NoResultException | NonUniqueResultException $ignored) { }

        return null;
    }

    public function addMatch(Match $match): ?Match
    {
        return $this->addEntity($match) ? $match : null;
    }
}