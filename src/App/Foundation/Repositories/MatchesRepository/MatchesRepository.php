<?php

namespace App\Foundation\Repositories\MatchesRepository;

use App\Foundation\Repositories\MatchesRepository\Models\Match;

interface MatchesRepository {

    /**
     * @param string $betId
     * @return Match[]
     */
    function findBetMatches(string $betId): array;

    /**
     * @param string[] $matchIds
     * @return Match[]
     */
    function getMatchesByIds(array $matchesIds): array;
    function getMatchById(string $matchId): ?Match;
    function addMatch(Match $match): ?Match;
}