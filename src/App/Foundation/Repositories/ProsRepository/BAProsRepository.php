<?php

namespace App\Foundation\Repositories\ProsRepository;

use App\Foundation\Repositories\BaseRepository;
use App\Foundation\Repositories\ProsRepository\Models\BAPro;
use App\Foundation\Repositories\ProsRepository\Models\Pro;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;

final class BAProsRepository extends BaseRepository implements ProsRepository {

    // MARK: - Constants

    private const CACHE_LIFETIME = 120;

    // MARK: - Init

    public function __construct(EntityManagerInterface $entityManager)
    {
        $class = $entityManager->getClassMetadata(BAPro::class);
        parent::__construct($entityManager, $class);
    }

    // MARK: - ProsRepository methods

    public function findMatchPros(string $matchId): array
    {
        $builder = $this->createQueryBuilder('pro');

        return $builder
            ->leftJoin('pro.matches', 'match')
            ->where($builder->expr()->like('match.id', ':matchId'))
            ->setParameter('matchId', $matchId)
            ->setCacheable(true)
            ->getQuery()
            ->setHint(Query::HINT_READ_ONLY, true)
            ->setResultCacheLifetime(self::CACHE_LIFETIME)
            ->setResultCacheId("$matchId::pros")
            ->getResult();
    }

    public function getProsByIds(array $prosIds): array
    {
        $builder = $this->createQueryBuilder('pro');

        return $builder
            ->where($builder->expr()->in('pro.id', ':prosIds'))
            ->setParameter('prosIds', $prosIds)
            ->setCacheable(true)
            ->getQuery()
            ->setHint(Query::HINT_READ_ONLY, true)
            ->setResultCacheLifetime(self::CACHE_LIFETIME)
            ->setResultCacheId(implode(',', $prosIds))
            ->getResult();
    }

    public function addPro(Pro $pro): ?Pro
    {
        return $this->addEntity($pro) ? $pro : null;
    }
}