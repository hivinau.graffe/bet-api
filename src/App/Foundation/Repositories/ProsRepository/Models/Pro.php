<?php

namespace App\Foundation\Repositories\ProsRepository\Models;

use App\Foundation\Repositories\MatchesRepository\Models\Match;
use JsonSerializable;

interface Pro extends JsonSerializable {
    /**
     * @return Match[]
     */
    function getMatches(): array;
    function getType(): ?string;
    function getLabel(): ?string;
    function getId(): ?string;
    function isWon(): bool;
    function addMatch(Match $match): Pro;
    function setLabel(string $label): Pro;
    function setType(string $type): Pro;
    function setWon(bool $won): Pro;
}