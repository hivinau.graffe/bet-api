<?php

namespace App\Foundation\Repositories\ProsRepository\Models;

use App\Foundation\Repositories\MatchesRepository\Models\Match;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Id\UuidGenerator;
use App\Foundation\Repositories\ProsRepository\BAProsRepository;

/**
 * @ORM\Entity(repositoryClass=BAProsRepository::class)
 * @ORM\Table(name="pros")
 */
class BAPro implements Pro {

    // MARK: - Constants

    public const GOAL_DIFFERENCE = 'GOAL_DIFFERENCE';
    public const DOUBLE_CHANCE = 'DOUBLE_CHANCE';
    public const BOTH_TEAMS_TO_SCORE = 'BOTH_TEAMS_TO_SCORE';
    public const RESULT = 'RESULT';
    public const SCORERS = 'SCORERS';
    public const TOTAL_GOALS = 'TOTAL_GOALS';

    // MARK: - Private properties

    /**
     * @ORM\Column(name="label", type="string")
     * @var string
     */
    private $label;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @var string
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Foundation\Repositories\MatchesRepository\Models\BAMatch", mappedBy="pros", fetch="EXTRA_LAZY")
     * @var PersistentCollection
     */
    private $matches;

    /**
     * @ORM\Column(name="won", type="boolean", options={"default": false})
     * @var bool
     */
    private $won = false;

    /**
     * @ORM\Column(name="type", type="string")
     * @var string
     */
    private $type;

    // MARK: - Init

    public function __construct()
    {
        $this->matches = new ArrayCollection();
    }

    // MARK: - Pro methods

    public function addMatch(Match $match): Pro
    {
        $this->matches[] = $match;
        return $this;
    }

    public function setLabel(string $label): Pro
    {
        $this->label = $label;
        return $this;
    }

    public function setType(string $type): Pro
    {
        $this->type = $type;
        return $this;
    }

    public function setWon(bool $won): Pro
    {
        $this->won = $won;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function getMatches(): array
    {
        return $this->matches->getValues();
    }

    public function isWon(): bool
    {
        return $this->won;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    // MARK: - JsonSerializable methods

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'label' => $this->label,
            'is_won' => $this->won,
            'type' => $this->type
        ];
    }
}