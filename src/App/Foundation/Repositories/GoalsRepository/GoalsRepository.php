<?php

namespace App\Foundation\Repositories\GoalsRepository;

use App\Foundation\Repositories\GoalsRepository\Models\Goal;

interface GoalsRepository {

    /**
     * @param string $matchId
     * @return Goal[]
     */
    function findMatchGoals(string $matchId): array;
    function addGoal(Goal $goal): ?Goal;
}