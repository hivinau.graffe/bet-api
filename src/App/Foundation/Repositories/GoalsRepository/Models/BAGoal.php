<?php

namespace App\Foundation\Repositories\GoalsRepository\Models;

use App\Foundation\Repositories\MatchesRepository\Models\BAMatch;
use App\Foundation\Repositories\MatchesRepository\Models\Match;
use App\Foundation\Repositories\TeamsRepository\Models\BATeam;
use App\Foundation\Repositories\TeamsRepository\Models\Team;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Id\UuidGenerator;
use App\Foundation\Repositories\GoalsRepository\BAGoalsRepository;

/**
 * @ORM\Entity(repositoryClass=BAGoalsRepository::class)
 * @ORM\Table(name="goals")
 */
class BAGoal implements Goal {

    // MARK: - Private properties

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @var string
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Foundation\Repositories\MatchesRepository\Models\BAMatch", inversedBy="goals")
     * @ORM\JoinColumn(name="match_id", referencedColumnName="id")
     * @var BAMatch
     */
    private $match;

    /**
     * @ORM\ManyToOne(targetEntity="App\Foundation\Repositories\TeamsRepository\Models\BATeam", inversedBy="goals")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     * @var BATeam
     */
    private $team;

    /**
     * @ORM\Column(name="scorer", type="string")
     * @var string
     */
    private $scorer;

    /**
     * @ORM\Column(name="time", type="float")
     * @var float
     */
    private $time;

    // MARK: - Goal methods

    public function setMatch(Match $match): Goal
    {
        $this->match = $match;
        return $this;
    }

    public function setTeam(Team $team): Goal
    {
        $this->team = $team;
        return $this;
    }

    public function setScorer(string $scorer): Goal
    {
        $this->scorer = $scorer;
        return $this;
    }

    public function setTime(float $time): Goal
    {
        $this->time = $time;
        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getScorer(): ?string
    {
        return $this->scorer;
    }

    public function getMatch(): ?Match
    {
        return $this->match;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function getTime(): float
    {
        return $this->time;
    }

    // MARK: - JsonSerializable methods

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'scorer' => $this->scorer,
            'time' => $this->time,
            'team_id' => $this->getTeam()->getId()
        ];
    }
}