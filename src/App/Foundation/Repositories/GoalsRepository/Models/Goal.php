<?php

namespace App\Foundation\Repositories\GoalsRepository\Models;

use App\Foundation\Repositories\MatchesRepository\Models\Match;
use App\Foundation\Repositories\TeamsRepository\Models\Team;
use JsonSerializable;

interface Goal extends JsonSerializable {
    function getMatch(): ?Match;
    function getTeam(): ?Team;
    function getScorer(): ?string;
    function getId(): ?string;
    function getTime(): float;
    function setMatch(Match $match): Goal;
    function setTeam(Team $team): Goal;
    function setScorer(string $scorer): Goal;
    function setTime(float $time): Goal;
}