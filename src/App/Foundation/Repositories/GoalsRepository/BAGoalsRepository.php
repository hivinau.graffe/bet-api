<?php

namespace App\Foundation\Repositories\GoalsRepository;

use App\Foundation\Repositories\BaseRepository;
use App\Foundation\Repositories\GoalsRepository\Models\BAGoal;
use App\Foundation\Repositories\GoalsRepository\Models\Goal;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;

final class BAGoalsRepository extends BaseRepository implements GoalsRepository {

    // MARK: - Constants

    private const CACHE_LIFETIME = 120;

    // MARK: - Init

    public function __construct(EntityManagerInterface $entityManager)
    {
        $class = $entityManager->getClassMetadata(BAGoal::class);
        parent::__construct($entityManager, $class);
    }

    // MARK: - GoalsRepository methods

    public function findMatchGoals(string $matchId): array
    {
        $builder = $this->createQueryBuilder('goal');

        return $builder
                ->leftJoin('goal.match', 'match')
                ->where($builder->expr()->like('match.id', ':matchId'))
                ->setParameter('matchId', $matchId)
                ->setCacheable(true)
                ->getQuery()
                ->setHint(Query::HINT_READ_ONLY, true)
                ->setResultCacheLifetime(self::CACHE_LIFETIME)
                ->setResultCacheId("$matchId::goals")
                ->getResult();
    }

    public function addGoal(Goal $goal): ?Goal
    {
        return $this->addEntity($goal) ? $goal : null;
    }
}