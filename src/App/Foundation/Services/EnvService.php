<?php

namespace App\Foundation\Services;

use App\Foundation\Routers\Router;
use DI\ContainerBuilder;
use Exception;
use Symfony\Component\Dotenv\Dotenv;

final class EnvService implements Service {

    // MARK: Private properties

    /**
     * @var Dotenv
     */
    private $dotenv;

    /**
     * @var ContainerBuilder
     */
    private $builder;

    // MARK: Init

    /**
     * EnvService constructor.
     */
    public function __construct()
    {
        $this->dotenv = new Dotenv();
        $this->builder = new ContainerBuilder();
    }

    // MARK: Service methods

    public function routerDidLoadRequest(Router $router): void
    {
        $file = dirname(dirname(dirname(dirname(__DIR__)))) . DIRECTORY_SEPARATOR . '.env';
        $environment = $this->loadEnvironmentFromFile($file);
        $this->injectEnvironmentToRouter($environment, $router);
    }

    // MARK: Private methods

    private function loadEnvironmentFromFile(string $file): array
    {
        $this->dotenv->load($file);
        return $_ENV;
    }

    private function injectEnvironmentToRouter(array $environment, Router $router): void
    {
        $this->builder = new ContainerBuilder();
        $this->builder->addDefinitions($environment);

        try {
            $router->setContainer($this->builder->build());
        } catch (Exception $ignored) {}
    }
}