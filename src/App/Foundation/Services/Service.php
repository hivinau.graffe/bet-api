<?php

namespace App\Foundation\Services;

use App\Foundation\Routers\Router;

interface Service {
    function routerDidLoadRequest(Router $router): void;
}