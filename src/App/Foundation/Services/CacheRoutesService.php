<?php

namespace App\Foundation\Services;

use App\Foundation\Routers\Router;

final class CacheRoutesService implements Service {

    // MARK: - Constants

    private const ROUTES_CACHE_FILE = '/var/www/html/.routes.php';

    // MARK: - Service methods

    public function routerDidLoadRequest(Router $router): void
    {
        $routeCollector = $router->getRouteCollector();
        $routeCollector->setCacheFile(self::ROUTES_CACHE_FILE);
    }
}