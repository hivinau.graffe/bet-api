<?php

namespace App\Foundation\Services;

use App\Foundation\Assemblies\Assembly;
use App\Foundation\Components\ComponentsAssembly;
use App\Foundation\Helpers\InitHelper;
use App\Foundation\Routers\Router;
use App\Scenes\AppAssembly;
use App\Scenes\AppCoordinator;

final class BAService implements Service {

    /**
     * @var Assembly[]
     */
    private $assemblies = [
        ComponentsAssembly::class,
        AppAssembly::class
    ];

    // MARK: - Service methods

    public function routerDidLoadRequest(Router $router): void
    {
        $this->injectDependenciesToRouter($router);

        $this->propagateRouter($router);
    }

    // MARK: - Private methods

    private function injectDependenciesToRouter(Router $router): void
    {
        foreach ($this->assemblies as $assemblyName) {
            /** @var null|Assembly $assembly */
            $assembly = InitHelper::init($assemblyName);

            if(is_null($assembly)) {
                return;
            }

            $assembly->injectDependenciesToRouter($router);
        }
    }

    private function propagateRouter(Router $router): void
    {
        $container = $router->getContainer();

        $appCoordinator = $container->get(AppCoordinator::class);
        $appCoordinator->startRouter($router);
    }
}