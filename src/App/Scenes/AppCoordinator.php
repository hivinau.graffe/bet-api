<?php

namespace App\Scenes;

use App\Foundation\Coordinators\Coordinator;
use App\Foundation\Routers\Router;

final class AppCoordinator implements Coordinator {

    // MARK: - Private properties

    /**
     * @var Coordinator[]
     */
    private $childCoordinators;

    // MARK: - Init

    public function __construct(array $childCoordinators)
    {
        $this->childCoordinators = $childCoordinators;
    }

    // MARK: - Coordinator methods

    public function startRouter(Router $router): void
    {
        foreach ($this->childCoordinators as $coordinator) {
            $coordinator->startRouter($router);
        }
    }
}