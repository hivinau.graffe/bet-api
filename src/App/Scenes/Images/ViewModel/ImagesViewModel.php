<?php

namespace App\Scenes\Images\ViewModel;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UploadedFileInterface;

interface ImagesViewModel {
    function uploadImage(?UploadedFileInterface $image): array;
    function getImage(?string $filename, ?string $width, ?string $height): ?ResponseInterface;
}