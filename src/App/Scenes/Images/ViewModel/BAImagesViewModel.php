<?php

namespace App\Scenes\Images\ViewModel;

use App\Foundation\Components\ImagesManager\ImagesManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Components\ValidatorManager\Validators\ImageValidator;
use App\Foundation\Components\VibrantColorExtractor\VibrantColorExtractor;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UploadedFileInterface;

final class BAImagesViewModel implements ImagesViewModel {

    // MARK: - Private properties

    /**
     * @var ImagesManager
     */
    private $imagesManager;

    /**
     * @var ValidatorManager
     */
    private $validatorManager;

    // MARK: - Init

    public function __construct(ImagesManager $imagesManager,
                                ValidatorManager $validatorManager)
    {
        $this->imagesManager = $imagesManager;
        $this->validatorManager = $validatorManager;

        $this->setupValidators();
    }

    // MARK: - ImagesViewModel methods

    public function uploadImage(?UploadedFileInterface $image): array
    {
        if(!$this->isFileValid($image)) {
            return [];
        }

        return $this->imagesManager->uploadImage($image);
    }

    public function getImage(?string $filename, ?string $width, ?string $height): ?ResponseInterface
    {
        if(is_null($filename)) {
            return null;
        }

        return $this->imagesManager->getImage($filename, intval($width ?? -1), intval($height ?? -1));
    }

    // MARK: - Private methods

    private function setupValidators() {
        $this->validatorManager
            ->addValidator(new ImageValidator());
    }

    private function isFileValid(?UploadedFileInterface $file): bool
    {
        if(is_null($file)) {
            return false;
        }

        $filename = $file->getClientFilename();
        return !is_null($filename) && $this->validatorManager->validate($filename);
    }
}