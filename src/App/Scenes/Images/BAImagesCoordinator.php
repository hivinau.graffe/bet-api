<?php

namespace App\Scenes\Images;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class BAImagesCoordinator extends ImagesCoordinator {

    // MARK: - ImagesCoordinator methods

    public function uploadImage(?ServerRequestInterface $request): ResponseInterface
    {
        return $this->imagesView->uploadImage($request);
    }

    public function getImage(?ServerRequestInterface $request): ResponseInterface
    {
        return $this->imagesView->getImage($request);
    }
}