<?php

namespace App\Scenes\Images;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Components\ImagesManager\ImagesManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Routers\Router;
use App\Scenes\Images\ViewModel\BAImagesViewModel;
use App\Scenes\Images\ViewModel\ImagesViewModel;
use function DI\create;
use function DI\get;

final class ImagesAssembly extends DiAssembly {

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $container = $router->getContainer();
        $this->builder->addDefinitions([
            ImagesViewModel::class => create(BAImagesViewModel::class)
                ->constructor(
                    $container->get(ImagesManager::class),
                    $container->get(ValidatorManager::class)
                ),
            ImagesCoordinator::class => create(BAImagesCoordinator::class)
                ->constructor(get(ImagesViewModel::class))
        ]);

        parent::injectDependenciesToRouter($router);
    }
}