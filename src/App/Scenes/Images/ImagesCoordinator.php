<?php

namespace App\Scenes\Images;

use App\Foundation\Coordinators\Coordinator;
use App\Foundation\Routers\Router;
use App\Scenes\Bets\View\BABetsView;
use App\Scenes\Bets\ViewModel\BetsViewModel;
use App\Scenes\Images\View\BAImagesView;
use App\Scenes\Images\ViewModel\ImagesViewModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

abstract class ImagesCoordinator implements Coordinator {

    // MARK: - Protected properties

    /**
     * @var BAImagesView
     */
    protected $imagesView;

    // MARK: - Init

    public function __construct(ImagesViewModel $viewModel)
    {
        $this->imagesView = new BAImagesView($viewModel);
    }

    // MARK: - Coordinator methods

    public function startRouter(Router $router): void
    {
        $router->post('/images/upload', [$this, 'uploadImage']);
        $router->get('/images/{image}', [$this, 'getImage']);
    }

    // MARK: - Abstract methods

    abstract function uploadImage(?ServerRequestInterface $request): ResponseInterface;
    abstract function getImage(?ServerRequestInterface $request): ResponseInterface;
}