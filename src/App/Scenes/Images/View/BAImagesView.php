<?php

namespace App\Scenes\Images\View;

use App\Scenes\Images\ViewModel\ImagesViewModel;
use Fig\Http\Message\StatusCodeInterface;
use function PHPUnit\Framework\isEmpty;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Response;

final class BAImagesView  {

    // MARK: - Constants

    private const IMAGE = 'image';
    private const WIDTH = 'w';
    private const HEIGHT = 'h';

    // MARK: - Private properties
    /**
     * @var ImagesViewModel
     */
    private $imagesViewModel;

    // MARK: - Init

    public function __construct(ImagesViewModel $imagesViewModel)
    {
        $this->imagesViewModel = $imagesViewModel;
    }

    // MARK: - Public methods

    public function uploadImage(ServerRequestInterface $request): ResponseInterface {
        $response = new Response();
        $parameters = $request->getUploadedFiles();

        $imageObject = $this->imagesViewModel->uploadImage($parameters[self::IMAGE]);

        if(count($imageObject) == 0) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
        }

        $response->getBody()->write(json_encode($imageObject));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(StatusCodeInterface::STATUS_CREATED);
    }

    public function getImage(ServerRequestInterface $request): ResponseInterface {
        $route = $request->getAttribute('__route__');
        $parameters = $route->getArguments() ?? [];
        $dimensions = $request->getQueryParams();

        $image = $this->imagesViewModel->getImage(
            $parameters[self::IMAGE],
            $dimensions[self::WIDTH],
            $dimensions[self::HEIGHT]
        );

        if(is_null($image)) {
            return (new Response())->withStatus(StatusCodeInterface::STATUS_NOT_FOUND);
        }

        return $image;
    }
}