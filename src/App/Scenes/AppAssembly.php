<?php

namespace App\Scenes;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Routers\Router;
use App\Scenes\Auth\AuthAssembly;
use App\Scenes\Auth\AuthCoordinator;
use App\Scenes\Bets\BetsAssembly;
use App\Scenes\Bets\BetsCoordinator;
use App\Scenes\Goals\GoalsAssembly;
use App\Scenes\Goals\GoalsCoordinator;
use App\Scenes\Home\HomeAssembly;
use App\Scenes\Home\HomeCoordinator;
use App\Scenes\Matches\MatchesAssembly;
use App\Scenes\Matches\MatchesCoordinator;
use App\Scenes\Pros\ProsAssembly;
use App\Scenes\Pros\ProsCoordinator;
use App\Scenes\Teams\TeamsAssembly;
use App\Scenes\Teams\TeamsCoordinator;
use App\Scenes\Images\ImagesAssembly;
use App\Scenes\Images\ImagesCoordinator;
use App\Scenes\Players\PlayersAssembly;
use App\Scenes\Players\PlayersCoordinator;
use function DI\create;

final class AppAssembly extends DiAssembly {

    // MARK: - Constants

    private const ASSEMBLIES = [
        HomeAssembly::class,
        BetsAssembly::class,
        MatchesAssembly::class,
        TeamsAssembly::class,
        ProsAssembly::class,
        GoalsAssembly::class,
        ImagesAssembly::class,
        PlayersAssembly::class,
        AuthAssembly::class,
    ];

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $this->injectSubAssembliesToRouter(self::ASSEMBLIES, $router);

        $container = $router->getContainer();
        $this->builder->addDefinitions([
            AppCoordinator::class => create()
                ->constructor(
                    [
                        $container->get(BetsCoordinator::class),
                        $container->get(MatchesCoordinator::class),
                        $container->get(TeamsCoordinator::class),
                        $container->get(ProsCoordinator::class),
                        $container->get(GoalsCoordinator::class),
                        $container->get(ImagesCoordinator::class),
                        $container->get(AuthCoordinator::class),
                        $container->get(PlayersCoordinator::class),
                        $container->get(HomeCoordinator::class)
                    ]
                )
        ]);

        parent::injectDependenciesToRouter($router);
    }
}