<?php

namespace App\Scenes\Bets\ViewModel;

use App\Foundation\Components\EntityManager\BetsEntityManager\BetsEntityManager;
use App\Foundation\Components\EntityManager\MatchesEntityManager\MatchesEntityManager;
use App\Foundation\Components\StringExploder\StringExploder;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Components\ValidatorManager\Validators\UuidValidator;
use App\Foundation\Repositories\BetsRepository\Models\BABet;
use App\Foundation\Repositories\BetsRepository\Models\Bet;

final class BABetsViewModel implements BetsViewModel {

    // MARK: - Private properties

    /**
     * @var BetsEntityManager
     */
    private $betsEntityManager;

    /**
     * @var MatchesEntityManager
     */
    private $matchesEntityManager;

    /**
     * @var ValidatorManager
     */
    private $validatorManager;

    /**
     * @var StringExploder
     */
    private $stringExploder;

    // MARK: - Init

    public function __construct(BetsEntityManager $betsEntityManager,
                                MatchesEntityManager $matchesEntityManager,
                                ValidatorManager $validatorManager,
                                StringExploder $stringExploder)
    {
        $this->betsEntityManager = $betsEntityManager;
        $this->matchesEntityManager = $matchesEntityManager;
        $this->validatorManager = $validatorManager;
        $this->stringExploder = $stringExploder;

        $this->setupValidators();
    }

    // MARK: - BetsViewModel methods

    public function getBets(): array
    {
        return $this->betsEntityManager->findBets();
    }

    public function addBetMatches(?string $matchesIds): ?Bet
    {
        $matchesIds = $this->explode($matchesIds);
        if(count($matchesIds) == 0) {
            return null;
        }

        $matches = $this->matchesEntityManager->getMatchesByIds($matchesIds);
        $bet = new BABet();

        foreach ($matches as $match) {
            $bet->addMatch($match);
            $match->addBet($bet);
        }

        return $this->betsEntityManager->addBet($bet);
    }

    // MARK: - Private methods

    private function setupValidators() {
        $this->validatorManager
            ->addValidator(new UuidValidator());
    }

    private function isContentValid(?string $content): bool
    {
        return is_string($content) && $this->validatorManager->validate($content);
    }

    /**
     * @param string|null $subject
     * @return string[]
     */
    private function explode(?string $subject): array
    {
        if(!$this->isContentValid($subject)) {
            return [];
        }

        $elements = $this->stringExploder->explode($subject);
        if(count($elements) == 0) {
            return [];
        }

        $invalidElements = array_filter($elements, function (string $element) {
            return !$this->isContentValid($element);
        });

        return count($invalidElements) == 0 ? $elements : [];
    }
}