<?php

namespace App\Scenes\Bets\ViewModel;

use App\Foundation\Repositories\BetsRepository\Models\Bet;
interface BetsViewModel {
    /**
     * @return Bet[]
     */
    function getBets(): array;
    function addBetMatches(?string $matchesIds): ?Bet;
}