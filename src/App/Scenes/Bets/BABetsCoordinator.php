<?php

namespace App\Scenes\Bets;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class BABetsCoordinator extends BetsCoordinator {

    // MARK: - BetsCoordinator methods

    public function listBets(): ResponseInterface
    {
        return $this->betsView->listBets();
    }

    public function addBet(?ServerRequestInterface $request): ResponseInterface
    {
        return $this->betsView->addBet($request);
    }
}