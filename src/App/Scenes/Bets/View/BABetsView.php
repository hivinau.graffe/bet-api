<?php

namespace App\Scenes\Bets\View;

use App\Scenes\Bets\ViewModel\BetsViewModel;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Response;

final class BABetsView
{

    // MARK: - Constants

    private const MATCHES = 'matches';

    // MARK: - Private properties

    /**
     * @var BetsViewModel
     */
    private $betsViewModel;

    // MARK: - Init

    public function __construct(BetsViewModel $betsViewModel)
    {
        $this->betsViewModel = $betsViewModel;
    }

    // MARK: - Public methods

    public function listBets(): ResponseInterface
    {
        $bets = $this->betsViewModel->getBets();

        $response = new Response();
        $response->getBody()->write(json_encode($bets));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(empty($bets) ? StatusCodeInterface::STATUS_NOT_FOUND : StatusCodeInterface::STATUS_OK);
    }

    public function addBet(ServerRequestInterface $request): ResponseInterface
    {
        $response = new Response();
        $parameters = $request->getParsedBody();
        $bet = $this->betsViewModel->addBetMatches($parameters[self::MATCHES]);

        if (is_null($bet)) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
        }

        $response->getBody()->write(json_encode($bet));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(StatusCodeInterface::STATUS_CREATED);
    }
}