<?php

namespace App\Scenes\Bets;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Components\EntityManager\BetsEntityManager\BetsEntityManager;
use App\Foundation\Components\EntityManager\MatchesEntityManager\MatchesEntityManager;
use App\Foundation\Components\StringExploder\StringExploder;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Routers\Router;
use App\Scenes\Bets\ViewModel\BABetsViewModel;
use App\Scenes\Bets\ViewModel\BetsViewModel;
use function DI\create;
use function DI\get;

final class BetsAssembly extends DiAssembly {

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $container = $router->getContainer();
        $this->builder->addDefinitions([
            BetsViewModel::class => create(BABetsViewModel::class)
                ->constructor(
                    $container->get(BetsEntityManager::class),
                    $container->get(MatchesEntityManager::class),
                    $container->get(ValidatorManager::class),
                    $container->get(StringExploder::class)
                ),
            BetsCoordinator::class => create(BABetsCoordinator::class)
                ->constructor(get(BetsViewModel::class))
        ]);

        parent::injectDependenciesToRouter($router);
    }
}