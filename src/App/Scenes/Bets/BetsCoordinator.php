<?php

namespace App\Scenes\Bets;

use App\Foundation\Coordinators\Coordinator;
use App\Foundation\Routers\Router;
use App\Scenes\Bets\View\BABetsView;
use App\Scenes\Bets\ViewModel\BetsViewModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

abstract class BetsCoordinator implements Coordinator {

    // MARK: - Protected properties

    /**
     * @var BABetsView
     */
    protected $betsView;

    // MARK: - Init

    public function __construct(BetsViewModel $viewModel)
    {
        $this->betsView = new BABetsView($viewModel);
    }

    // MARK: - Coordinator methods

    public function startRouter(Router $router): void
    {
        $router->get('/bets', [$this, 'listBets']);
        $router->post('/bets/add', [$this, 'addBet']);
    }

    // MARK: - Abstract methods

    abstract function listBets(): ResponseInterface;
    abstract function addBet(?ServerRequestInterface $request): ResponseInterface;
}