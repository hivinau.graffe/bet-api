<?php

namespace App\Scenes\Matches\ViewModel;

use App\Foundation\Components\DatetimeManager\DatetimeManager;
use App\Foundation\Components\EntityManager\MatchesEntityManager\MatchesEntityManager;
use App\Foundation\Components\EntityManager\ProsEntityManager\ProsEntityManager;
use App\Foundation\Components\EntityManager\TeamsEntityManager\TeamsEntityManager;
use App\Foundation\Components\StringExploder\StringExploder;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Components\ValidatorManager\Validators\DateValidator;
use App\Foundation\Components\ValidatorManager\Validators\RegularValidator;
use App\Foundation\Components\ValidatorManager\Validators\TimeValidator;
use App\Foundation\Components\ValidatorManager\Validators\UuidValidator;
use App\Foundation\Repositories\MatchesRepository\Models\BAMatch;
use App\Foundation\Repositories\MatchesRepository\Models\Match;

final class BAMatchesViewModel implements MatchesViewModel {

    // MARK: - Constants

    private const MIN_STRING_LENGTH = 2;

    // MARK: - Private properties

    /**
     * @var MatchesEntityManager
     */
    private $matchesEntityManager;

    /**
     * @var TeamsEntityManager
     */
    private $teamsEntityManager;

    /**
     * @var ProsEntityManager
     */
    private $prosEntityManager;

    /**
     * @var ValidatorManager
     */
    private $validatorManager;

    /**
     * @var DatetimeManager
     */
    private $datetimeManager;

    /**
     * @var StringExploder
     */
    private $stringExploder;

    // MARK: - Init

    public function __construct(MatchesEntityManager $matchesEntityManager,
                                TeamsEntityManager $teamsEntityManager,
                                ProsEntityManager $prosEntityManager,
                                ValidatorManager $validatorManager,
                                DatetimeManager $datetimeManager,
                                StringExploder $stringExploder)
    {
        $this->matchesEntityManager = $matchesEntityManager;
        $this->teamsEntityManager = $teamsEntityManager;
        $this->prosEntityManager = $prosEntityManager;
        $this->validatorManager = $validatorManager;
        $this->datetimeManager = $datetimeManager;
        $this->stringExploder = $stringExploder;

        $this->setupValidators();
    }

    // MARK: - BetsViewModel methods

    public function getBetMatches(?string $betId): array
    {
        if(!$this->isContentValid($betId)) {
            return [];
        }

        return $this->matchesEntityManager->findBetMatches($betId);
    }

    public function addMatchTeams(?string $teamsIds, ?string $prosIds, ?string $date, ?string $time): ?Match
    {
        $teamsIds = $this->explode($teamsIds);
        $prosIds = $this->explode($prosIds);
        if(count($teamsIds) == 0 ||
            count($prosIds) == 0 ||
            !$this->isContentValid($date) ||
            !$this->isContentValid($time)) {
            return null;
        }

        $date = $this->datetimeManager->createDate($date);
        $time = $this->datetimeManager->createTime($time);
        $teams = $this->teamsEntityManager->getTeamsByIds($teamsIds);
        $pros = $this->prosEntityManager->getProsByIds($prosIds);

        $match = (new BAMatch())
            ->setDate($date)
            ->setTime($time);

        foreach ($teams as $team) {
            $match->addTeam($team);
            $team->addMatch($match);
        }

        foreach ($pros as $pro) {
            $match->addPro($pro);
            $pro->addMatch($match);
        }

        return $this->matchesEntityManager->addMatch($match);
    }

    // MARK: - Private methods

    private function setupValidators() {
        $this->validatorManager
            ->addValidator(new DateValidator())
            ->addValidator(new TimeValidator())
            ->addValidator(new UuidValidator())
            ->addValidator(new RegularValidator(self::MIN_STRING_LENGTH));
    }

    private function isContentValid(?string $content): bool
    {
        return is_string($content) && $this->validatorManager->validate($content);
    }

    /**
     * @param string|null $subject
     * @return string[]
     */
    private function explode(?string $subject): array
    {
        if(!$this->isContentValid($subject)) {
            return [];
        }

        $elements = $this->stringExploder->explode($subject);
        if(count($elements) == 0) {
            return [];
        }

        $invalidElements = array_filter($elements, function (string $element) {
            return !$this->isContentValid($element);
        });

        return count($invalidElements) == 0 ? $elements : [];
    }
}