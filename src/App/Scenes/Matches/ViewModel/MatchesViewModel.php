<?php

namespace App\Scenes\Matches\ViewModel;

use App\Foundation\Repositories\MatchesRepository\Models\Match;

interface MatchesViewModel {

    /**
     * @param null|string $betId
     * @return Match[]
     */
    function getBetMatches(?string $betId): array;
    function addMatchTeams(?string $teamsIds, ?string $prosIds, ?string $date, ?string $time): ?Match;
}