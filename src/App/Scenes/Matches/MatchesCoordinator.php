<?php

namespace App\Scenes\Matches;

use App\Foundation\Coordinators\Coordinator;
use App\Foundation\Routers\Router;
use App\Scenes\Matches\View\BAMatchesView;
use App\Scenes\Matches\ViewModel\MatchesViewModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

abstract class MatchesCoordinator implements Coordinator {

    // MARK: - Protected properties

    /**
     * @var BAMatchesView
     */
    protected $matchesView;

    // MARK: - Init

    public function __construct(MatchesViewModel $viewModel)
    {
        $this->matchesView = new BAMatchesView($viewModel);
    }

    // MARK: - Coordinator methods

    public function startRouter(Router $router): void
    {
        $router->get('/bets/{bet}/matches', [$this, 'listBetMatches']);
        $router->post('/matches/add', [$this, 'addMatch']);
    }

    // MARK: - Abstract methods

    abstract function listBetMatches(?ServerRequestInterface $request): ResponseInterface;
    abstract function addMatch(?ServerRequestInterface $request): ResponseInterface;
}