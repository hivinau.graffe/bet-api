<?php

namespace App\Scenes\Matches;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Components\DatetimeManager\DatetimeManager;
use App\Foundation\Components\EntityManager\MatchesEntityManager\MatchesEntityManager;
use App\Foundation\Components\EntityManager\ProsEntityManager\ProsEntityManager;
use App\Foundation\Components\EntityManager\TeamsEntityManager\TeamsEntityManager;
use App\Foundation\Components\StringExploder\StringExploder;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Routers\Router;
use App\Scenes\Matches\ViewModel\BAMatchesViewModel;
use App\Scenes\Matches\ViewModel\MatchesViewModel;
use function DI\create;
use function DI\get;

final class MatchesAssembly extends DiAssembly {

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $container = $router->getContainer();
        $this->builder->addDefinitions([
            MatchesViewModel::class => create(BAMatchesViewModel::class)
                ->constructor(
                    $container->get(MatchesEntityManager::class),
                    $container->get(TeamsEntityManager::class),
                    $container->get(ProsEntityManager::class),
                    $container->get(ValidatorManager::class),
                    $container->get(DatetimeManager::class),
                    $container->get(StringExploder::class)
                ),
            MatchesCoordinator::class => create(BAMatchesCoordinator::class)
                ->constructor(get(MatchesViewModel::class))
        ]);

        parent::injectDependenciesToRouter($router);
    }
}