<?php

namespace App\Scenes\Matches;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class BAMatchesCoordinator extends MatchesCoordinator {

    // MARK: - MatchesCoordinator methods

    public function listBetMatches(?ServerRequestInterface $request): ResponseInterface
    {
        return $this->matchesView->listBetMatches($request);
    }

    public function addMatch(?ServerRequestInterface $request): ResponseInterface
    {
        return $this->matchesView->addMatch($request);
    }
}