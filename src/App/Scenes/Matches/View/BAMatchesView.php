<?php

namespace App\Scenes\Matches\View;

use App\Scenes\Matches\ViewModel\MatchesViewModel;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Response;

final class BAMatchesView  {

    // MARK: - Constants

    private const DATE = 'date';
    private const TIME = 'time';
    private const BET = 'bet';
    private const TEAMS = 'teams';
    private const PROS = 'pros';

    // MARK: - Private properties
    /**
     * @var MatchesViewModel
     */
    private $matchesViewModel;

    // MARK: - Init

    public function __construct(MatchesViewModel $matchesViewModel)
    {
        $this->matchesViewModel = $matchesViewModel;
    }

    // MARK: - Public methods

    public function listBetMatches(ServerRequestInterface $request): ResponseInterface {
        $route = $request->getAttribute('__route__');
        $parameters = $route->getArguments() ?? [];
        $matches = $this->matchesViewModel->getBetMatches($parameters[self::BET]);

        $response = new Response();
        $response->getBody()->write(json_encode($matches));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(empty($matches) ? StatusCodeInterface::STATUS_NOT_FOUND : StatusCodeInterface::STATUS_OK);
    }

    public function addMatch(ServerRequestInterface $request): ResponseInterface {
        $response = new Response();
        $parameters = $request->getParsedBody();

        $match = $this->matchesViewModel->addMatchTeams(
            $parameters[self::TEAMS],
            $parameters[self::PROS],
            $parameters[self::DATE],
            $parameters[self::TIME]
        );

        if(is_null($match)) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
        }

        $response->getBody()->write(json_encode($match));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(StatusCodeInterface::STATUS_CREATED);
    }
}