<?php

namespace App\Scenes\Goals\ViewModel;

use App\Foundation\Repositories\GoalsRepository\Models\Goal;

interface GoalsViewModel {

    /**
     * @param null|string $matchId
     * @return Goal[]
     */
    function getMatchGoals(?string $matchId): array;
    function addGoalMatchId(?string $matchId, ?string $teamId, ?string $scorer, ?string $time): ?Goal;
}