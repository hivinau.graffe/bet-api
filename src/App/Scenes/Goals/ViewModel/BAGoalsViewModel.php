<?php

namespace App\Scenes\Goals\ViewModel;

use App\Foundation\Components\EntityManager\GoalsEntityManager\GoalsEntityManager;
use App\Foundation\Components\EntityManager\MatchesEntityManager\MatchesEntityManager;
use App\Foundation\Components\EntityManager\TeamsEntityManager\TeamsEntityManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Components\ValidatorManager\Validators\RegularValidator;
use App\Foundation\Components\ValidatorManager\Validators\UuidValidator;
use App\Foundation\Repositories\GoalsRepository\Models\BAGoal;
use App\Foundation\Repositories\GoalsRepository\Models\Goal;

final class BAGoalsViewModel implements GoalsViewModel {

    // MARK: - Constants

    private const MIN_STRING_LENGTH = 1;

    // MARK: - Private properties

    /**
     * @var GoalsEntityManager
     */
    private $goalsEntityManager;

    /**
     * @var TeamsEntityManager
     */
    private $teamsEntityManager;

    /**
     * @var MatchesEntityManager
     */
    private $matchesEntityManager;

    /**
     * @var ValidatorManager
     */
    private $validatorManager;

    // MARK: - Init

    public function __construct(GoalsEntityManager $goalsEntityManager,
                                TeamsEntityManager $teamsEntityManager,
                                MatchesEntityManager $matchesEntityManager,
                                ValidatorManager $validatorManager)
    {
        $this->goalsEntityManager = $goalsEntityManager;
        $this->teamsEntityManager = $teamsEntityManager;
        $this->matchesEntityManager = $matchesEntityManager;
        $this->validatorManager = $validatorManager;

        $this->setupValidators();
    }

    // MARK: - GoalsViewModel methods

    public function getMatchGoals(?string $matchId): array
    {
        if(!$this->isContentValid($matchId)) {
            return [];
        }

        return $this->goalsEntityManager->findMatchGoals($matchId);
    }

    public function addGoalMatchId(?string $matchId,
                                   ?string $teamId,
                                   ?string $scorer,
                                   ?string $time): ?Goal
    {
        if(!$this->isContentValid($matchId) ||
            !$this->isContentValid($teamId) ||
            !$this->isContentValid($scorer) ||
            !$this->isContentValid($time)) {
            return null;
        }

        $match = $this->matchesEntityManager->getMatchById($matchId);
        $team = $this->teamsEntityManager->getTeamById($teamId);

        if(is_null($match) || is_null($team)) {
            return null;
        }

        $goal = (new BAGoal())
            ->setScorer($scorer)
            ->setTime($time)
            ->setMatch($match)
            ->setTeam($team);

        $team->addGoal($goal);
        $match->addGoal($goal);

        return $this->goalsEntityManager->addGoal($goal);
    }

    // MARK: - Private methods

    private function setupValidators() {
        $this->validatorManager
            ->addValidator(new UuidValidator())
            ->addValidator(new RegularValidator(self::MIN_STRING_LENGTH));
    }

    private function isContentValid(?string $content): bool
    {
        return is_string($content) && $this->validatorManager->validate($content);
    }
}