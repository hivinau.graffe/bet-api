<?php

namespace App\Scenes\Goals\View;

use App\Scenes\Goals\ViewModel\GoalsViewModel;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Response;

final class BAGoalsView  {

    // MARK: - Constants

    private const TIME = 'time';
    private const MATCH = 'match';
    private const TEAM = 'team';
    private const SCORER = 'scorer';

    // MARK: - Private properties
    /**
     * @var GoalsViewModel
     */
    private $goalsViewModel;

    // MARK: - Init

    public function __construct(GoalsViewModel $goalsViewModel)
    {
        $this->goalsViewModel = $goalsViewModel;
    }

    // MARK: - Public methods

    public function listMatchGoals(ServerRequestInterface $request): ResponseInterface {
        $route = $request->getAttribute('__route__');
        $parameters = $route->getArguments() ?? [];
        $goals = $this->goalsViewModel->getMatchGoals($parameters[self::MATCH]);

        $response = new Response();
        $response->getBody()->write(json_encode($goals));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(empty($goals) ? StatusCodeInterface::STATUS_NOT_FOUND : StatusCodeInterface::STATUS_OK);
    }

    public function addGoal(ServerRequestInterface $request): ResponseInterface {
        $response = new Response();
        $parameters = $request->getParsedBody();

        $goal = $this->goalsViewModel->addGoalMatchId(
            $parameters[self::MATCH],
            $parameters[self::TEAM],
            $parameters[self::SCORER],
            $parameters[self::TIME]);

        if(is_null($goal)) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
        }

        $response->getBody()->write(json_encode($goal));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(StatusCodeInterface::STATUS_CREATED);
    }
}