<?php

namespace App\Scenes\Goals;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Components\EntityManager\GoalsEntityManager\GoalsEntityManager;
use App\Foundation\Components\EntityManager\MatchesEntityManager\MatchesEntityManager;
use App\Foundation\Components\EntityManager\TeamsEntityManager\TeamsEntityManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Routers\Router;
use App\Scenes\Goals\ViewModel\BAGoalsViewModel;
use App\Scenes\Goals\ViewModel\GoalsViewModel;
use function DI\create;
use function DI\get;

final class GoalsAssembly extends DiAssembly {

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $container = $router->getContainer();
        $this->builder->addDefinitions([
            GoalsViewModel::class => create(BAGoalsViewModel::class)
                ->constructor(
                    $container->get(GoalsEntityManager::class),
                    $container->get(TeamsEntityManager::class),
                    $container->get(MatchesEntityManager::class),
                    $container->get(ValidatorManager::class)
                ),
            GoalsCoordinator::class => create(BAGoalsCoordinator::class)
                ->constructor(get(GoalsViewModel::class))
        ]);

        parent::injectDependenciesToRouter($router);
    }
}