<?php

namespace App\Scenes\Goals;

use App\Foundation\Coordinators\Coordinator;
use App\Foundation\Routers\Router;
use App\Scenes\Goals\View\BAGoalsView;
use App\Scenes\Goals\ViewModel\GoalsViewModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

abstract class GoalsCoordinator implements Coordinator {

    // MARK: - Protected properties

    /**
     * @var BAGoalsView
     */
    protected $goalsView;

    // MARK: - Init

    public function __construct(GoalsViewModel $viewModel)
    {
        $this->goalsView = new BAGoalsView($viewModel);
    }

    // MARK: - Coordinator methods

    public function startRouter(Router $router): void
    {
        $router->get('/matches/{match}/goals', [$this, 'listMatchGoals']);
        $router->post('/goals/add', [$this, 'addGoal']);
    }

    // MARK: - Abstract methods

    abstract function listMatchGoals(?ServerRequestInterface $request): ResponseInterface;
    abstract function addGoal(?ServerRequestInterface $request): ResponseInterface;
}