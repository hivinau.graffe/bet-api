<?php

namespace App\Scenes\Goals;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class BAGoalsCoordinator extends GoalsCoordinator {

    // MARK: - GoalsCoordinator methods

    public function listMatchGoals(?ServerRequestInterface $request): ResponseInterface
    {
        return $this->goalsView->listMatchGoals($request);
    }

    public function addGoal(?ServerRequestInterface $request): ResponseInterface
    {
        return $this->goalsView->addGoal($request);
    }
}