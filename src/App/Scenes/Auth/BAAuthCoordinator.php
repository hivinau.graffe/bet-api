<?php

namespace App\Scenes\Auth;

use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class BAAuthCoordinator extends AuthCoordinator {

    // MARK: - Constants

    private const TOKEN = 'token';

    // MARK: - AuthCoordinator methods

    public function authenticateIosPlayer(?ServerRequestInterface $request): ResponseInterface
    {
        $response = $this->authView->authenticateIosPlayer($request);

        if($response->getStatusCode() === StatusCodeInterface::STATUS_NOT_ACCEPTABLE) {
            return $response;
        }

        $token = (string) $response->getBody();
        $request = $request->withParsedBody([self::TOKEN =>$token]);
        return $this->playersCoordinator->addPlayer($request);
    }
}