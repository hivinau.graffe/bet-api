<?php

namespace App\Scenes\Auth\ViewModel;

interface AuthViewModel {
    function authenticateUrl(?string $url, ?string $signature, ?string $salt, ?string $timestamp): ?string;
}