<?php

namespace App\Scenes\Auth\ViewModel;

use App\Foundation\Components\CertificateInspector\CertificateInspector;
use App\Foundation\Components\TokenGenerator\TokenGenerator;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Components\ValidatorManager\Validators\RegularValidator;

final class BAAuthViewModel implements AuthViewModel {

    // MARK: - Constants

    private const MIN_STRING_LENGTH = 3;

    // MARK: - Private properties

    /**
     * @var ValidatorManager
     */
    private $validatorManager;

    /**
     * @var CertificateInspector
     */
    private $certificateInspector;

    /**
     * @var TokenGenerator
     */
    private $tokenGenerator;

    // MARK: - Init

    public function __construct(ValidatorManager $validatorManager,
                                CertificateInspector $certificateInspector,
                                TokenGenerator $tokenGenerator)
    {
        $this->validatorManager = $validatorManager;
        $this->certificateInspector = $certificateInspector;
        $this->tokenGenerator = $tokenGenerator;

        $this->setupValidators();
    }

    // MARK: - AuthViewModel methods

    public function authenticateUrl(?string $url, ?string $signature, ?string $salt, ?string $timestamp): ?string
    {
        if(!$this->isContentValid($url) ||
            !$this->isContentValid($signature) ||
            !$this->isContentValid($salt) ||
            !$this->isContentValid($timestamp)) {
            return null;
        }

        $verified = $this->certificateInspector->verifyAppleSignatureUrl($url, $signature, $salt, $timestamp);

        return $verified ? $this->tokenGenerator->tokenizeSignature($signature, $salt, $timestamp) : null;
    }

    // MARK: - Private methods

    private function setupValidators() {
        $this->validatorManager
            ->addValidator(new RegularValidator(self::MIN_STRING_LENGTH));
    }

    private function isContentValid(?string $content): bool
    {
        return is_string($content) && $this->validatorManager->validate($content);
    }
}