<?php

namespace App\Scenes\Auth;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Components\CertificateInspector\CertificateInspector;
use App\Foundation\Components\TokenGenerator\TokenGenerator;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Routers\Router;
use App\Scenes\Auth\ViewModel\AuthViewModel;
use App\Scenes\Auth\ViewModel\BAAuthViewModel;
use App\Scenes\Players\PlayersCoordinator;
use function DI\create;
use function DI\get;

final class AuthAssembly extends DiAssembly {

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $container = $router->getContainer();
        $this->builder->addDefinitions([
            AuthViewModel::class => create(BAAuthViewModel::class)
                ->constructor(
                    $container->get(ValidatorManager::class),
                    $container->get(CertificateInspector::class),
                    $container->get(TokenGenerator::class)
                ),
            AuthCoordinator::class => create(BAAuthCoordinator::class)
                ->constructor(
                    get(AuthViewModel::class),
                    $container->get(PlayersCoordinator::class)
                )
        ]);

        parent::injectDependenciesToRouter($router);
    }
}