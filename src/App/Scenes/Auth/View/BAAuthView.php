<?php

namespace App\Scenes\Auth\View;

use App\Scenes\Auth\ViewModel\AuthViewModel;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Response;

final class BAAuthView  {

    // MARK: - Constants

    private const URL = 'url';
    private const SIGNATURE = 'signature';
    private const SALT = 'salt';
    private const TIMESTAMP = 'timestamp';

    // MARK: - Private properties
    /**
     * @var AuthViewModel
     */
    private $authViewModel;

    // MARK: - Init

    public function __construct(AuthViewModel $authViewModel)
    {
        $this->authViewModel = $authViewModel;
    }

    // MARK: - Public methods

    public function authenticateIosPlayer(ServerRequestInterface $request): ResponseInterface
    {
        $response = new Response();
        $parameters = $request->getParsedBody();

        $sessionToken = $this->authViewModel->authenticateUrl(
            $parameters[self::URL],
            $parameters[self::SIGNATURE],
            $parameters[self::SALT],
            $parameters[self::TIMESTAMP]
        );

        if(is_null($sessionToken)) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
        }

        $response->getBody()->write($sessionToken);
        return $response
            ->withHeader('Content-Type', 'text/plain')
            ->withStatus(StatusCodeInterface::STATUS_ACCEPTED);
    }
}