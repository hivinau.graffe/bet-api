<?php

namespace App\Scenes\Auth;

use App\Foundation\Coordinators\Coordinator;
use App\Foundation\Routers\Router;
use App\Scenes\Auth\View\BAAuthView;
use App\Scenes\Auth\ViewModel\AuthViewModel;
use App\Scenes\Players\PlayersCoordinator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

abstract class AuthCoordinator implements Coordinator {

    // MARK: - Protected properties

    /**
     * @var BAAuthView
     */
    protected $authView;

    /**
     * @var PlayersCoordinator
     */
    protected $playersCoordinator;

    // MARK: - Init

    public function __construct(AuthViewModel $viewModel, PlayersCoordinator $playersCoordinator)
    {
        $this->authView = new BAAuthView($viewModel);
        $this->playersCoordinator = $playersCoordinator;
    }

    // MARK: - Coordinator methods

    public function startRouter(Router $router): void
    {
        $router->post('/auth/player/ios', [$this, 'authenticateIosPlayer']);
    }

    // MARK: - Abstract methods

    abstract function authenticateIosPlayer(?ServerRequestInterface $request): ResponseInterface;
}