<?php

namespace App\Scenes\Home\View;

use App\Scenes\Home\ViewModel\HomeViewModel;
use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\Response;

final class BAHomeView  {

    // MARK: - Private properties

    private HomeViewModel $homeViewModel;

    // MARK: - Init

    public function __construct(HomeViewModel $homeViewModel)
    {
        $this->homeViewModel = $homeViewModel;
    }

    // MARK: - Public methods

    public function index(): ResponseInterface {
        return $this->homeViewModel->renderHomePage();
    }
}