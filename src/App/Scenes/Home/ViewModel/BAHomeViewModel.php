<?php

namespace App\Scenes\Home\ViewModel;

use App\Foundation\Components\HtmlRenderer\HtmlRenderer;
use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\Response;

final class BAHomeViewModel implements HomeViewModel {

    // MARK: - Constants

    private const HOME_HTML_PATH = 'parts/home.html.twig';

    // MARK: - Private properties

    private HtmlRenderer $htmlRenderer;

    // MARK: - Init

    public function __construct(HtmlRenderer $htmlRenderer)
    {
        $this->htmlRenderer = $htmlRenderer;
    }

    // MARK: - HomeViewModel methods

    public function renderHomePage(): ResponseInterface
    {
        $response = new Response();
        $newResponse = $this->htmlRenderer->renderResponse($response,
            self::HOME_HTML_PATH,
            [
                'title' => 'Hivinau'
            ]);
        return $newResponse ?? $response;
    }
}