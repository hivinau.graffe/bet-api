<?php

namespace App\Scenes\Home\ViewModel;

use Psr\Http\Message\ResponseInterface;

interface HomeViewModel {
    function renderHomePage(): ResponseInterface;
}