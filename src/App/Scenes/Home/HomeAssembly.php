<?php

namespace App\Scenes\Home;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Components\HtmlRenderer\HtmlRenderer;
use App\Foundation\Routers\Router;
use App\Scenes\Home\ViewModel\BAHomeViewModel;
use App\Scenes\Home\ViewModel\HomeViewModel;
use function DI\create;
use function DI\get;

final class HomeAssembly extends DiAssembly {

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $container = $router->getContainer();
        $this->builder->addDefinitions([
            HomeViewModel::class => create(BAHomeViewModel::class)
                ->constructor(
                    $container->get(HtmlRenderer::class)
                ),
            HomeCoordinator::class => create(BAHomeCoordinator::class)
                ->constructor(get(HomeViewModel::class))
        ]);

        parent::injectDependenciesToRouter($router);
    }
}