<?php

namespace App\Scenes\Home;

use App\Foundation\Coordinators\Coordinator;
use App\Foundation\Routers\Router;
use App\Scenes\Home\View\BAHomeView;
use App\Scenes\Home\ViewModel\HomeViewModel;
use Psr\Http\Message\ResponseInterface;

abstract class HomeCoordinator implements Coordinator {

    // MARK: - Protected properties

    /**
     * @var BAHomeView
     */
    protected $homeView;

    // MARK: - Init

    public function __construct(HomeViewModel $homeViewModel)
    {
        $this->homeView = new BAHomeView($homeViewModel);
    }

    // MARK: - Coordinator methods

    public function startRouter(Router $router): void
    {
        $router->get('/', [$this, 'index']);
    }

    // MARK: - Abstract methods

    abstract function index(): ResponseInterface;
}