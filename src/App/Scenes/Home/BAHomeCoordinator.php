<?php

namespace App\Scenes\Home;

use Psr\Http\Message\ResponseInterface;

final class BAHomeCoordinator extends HomeCoordinator {

    // MARK: - HomeCoordinator methods

    public function index(): ResponseInterface
    {
        return $this->homeView->index();
    }
}