<?php

namespace App\Scenes\Pros\ViewModel;

use App\Foundation\Repositories\ProsRepository\Models\Pro;

interface ProsViewModel {

    /**
     * @param null|string $matchId
     * @return Pro[]
     */
    function getMatchPros(?string $matchId): array;
    function addProLabel(?string $label, ?string $type): ?Pro;
}