<?php

namespace App\Scenes\Pros\ViewModel;

use App\Foundation\Components\EntityManager\ProsEntityManager\ProsEntityManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Components\ValidatorManager\Validators\RegularValidator;
use App\Foundation\Repositories\ProsRepository\Models\BAPro;
use App\Foundation\Repositories\ProsRepository\Models\Pro;

final class BAProsViewModel implements ProsViewModel {

    // MARK: - Constants

    private const MIN_STRING_LENGTH = 5;

    // MARK: - Private properties

    /**
     * @var ProsEntityManager
     */
    private $entityManager;

    /**
     * @var ValidatorManager
     */
    private $validatorManager;

    // MARK: - Init

    public function __construct(ProsEntityManager $entityManager,
                                ValidatorManager $validatorManager)
    {
        $this->entityManager = $entityManager;
        $this->validatorManager = $validatorManager;

        $this->setupValidators();
    }

    // MARK: - BetsViewModel methods

    public function getMatchPros(?string $matchId): array
    {
        if(!$this->isContentValid($matchId)) {
            return [];
        }

        return $this->entityManager->findMatchPros($matchId);
    }

    public function addProLabel(?string $label, ?string $type): ?Pro
    {
        if(!$this->isContentValid($label) ||
            !$this->isContentValid($type)) {
            return null;
        }

        $pro = (new BAPro())
            ->setLabel($label)
            ->setType($type);

        return $this->entityManager->addPro($pro);
    }

    // MARK: - Private methods

    private function setupValidators() {
        $this->validatorManager
            ->addValidator(new RegularValidator(self::MIN_STRING_LENGTH));
    }

    private function isContentValid(?string $content): bool
    {
        return is_string($content) && $this->validatorManager->validate($content);
    }
}