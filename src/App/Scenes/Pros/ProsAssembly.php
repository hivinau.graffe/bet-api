<?php

namespace App\Scenes\Pros;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Components\EntityManager\ProsEntityManager\ProsEntityManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Routers\Router;
use App\Scenes\Pros\ViewModel\BAProsViewModel;
use App\Scenes\Pros\ViewModel\ProsViewModel;
use function DI\create;
use function DI\get;

final class ProsAssembly extends DiAssembly {

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $container = $router->getContainer();
        $this->builder->addDefinitions([
            ProsViewModel::class => create(BAProsViewModel::class)
                ->constructor(
                    $container->get(ProsEntityManager::class),
                    $container->get(ValidatorManager::class)
                ),
            ProsCoordinator::class => create(BAProsCoordinator::class)
                ->constructor(get(ProsViewModel::class))
        ]);

        parent::injectDependenciesToRouter($router);
    }
}