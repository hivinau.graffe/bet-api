<?php

namespace App\Scenes\Pros\View;

use App\Scenes\Pros\ViewModel\ProsViewModel;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Response;

final class BAProsView  {

    // MARK: - Constants

    private const LABEL = 'label';
    private const TYPE = 'type';
    private const MATCH = 'match';

    // MARK: - Private properties

    /**
     * @var ProsViewModel
     */
    private $prosViewModel;

    // MARK: - Init

    public function __construct(ProsViewModel $prosViewModel)
    {
        $this->prosViewModel = $prosViewModel;
    }

    // MARK: - Public methods

    public function listMatchPros(ServerRequestInterface $request): ResponseInterface {
        $route = $request->getAttribute('__route__');
        $parameters = $route->getArguments() ?? [];
        $matches = $this->prosViewModel->getMatchPros($parameters[self::MATCH]);

        $response = new Response();
        $response->getBody()->write(json_encode($matches));

        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(empty($matches) ? StatusCodeInterface::STATUS_NOT_FOUND : StatusCodeInterface::STATUS_OK);
    }

    public function addPro(ServerRequestInterface $request): ResponseInterface {
        $response = new Response();
        $parameters = $request->getParsedBody();

        $pro = $this->prosViewModel->addProLabel($parameters[self::LABEL], $parameters[self::TYPE]);

        if(is_null($pro)) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
        }

        $response->getBody()->write(json_encode($pro));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(StatusCodeInterface::STATUS_CREATED);
    }
}