<?php

namespace App\Scenes\Pros;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class BAProsCoordinator extends ProsCoordinator {

    // MARK: - ProsCoordinator methods

    public function listMatchPros(?ServerRequestInterface $request): ResponseInterface
    {
        return $this->prosView->listMatchPros($request);
    }

    public function addPro(?ServerRequestInterface $request): ResponseInterface
    {
        return $this->prosView->addPro($request);
    }
}