<?php

namespace App\Scenes\Pros;

use App\Foundation\Coordinators\Coordinator;
use App\Foundation\Routers\Router;
use App\Scenes\Pros\View\BAProsView;
use App\Scenes\Pros\ViewModel\ProsViewModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

abstract class ProsCoordinator implements Coordinator {

    // MARK: - Protected properties

    /**
     * @var BAProsView
     */
    protected $prosView;

    // MARK: - Init

    public function __construct(ProsViewModel $viewModel)
    {
        $this->prosView = new BAProsView($viewModel);
    }

    // MARK: - Coordinator methods

    public function startRouter(Router $router): void
    {
        $router->get('/matches/{match}/pros', [$this, 'listMatchPros']);
        $router->post('/pros/add', [$this, 'addPro']);
    }

    // MARK: - Abstract methods

    abstract function listMatchPros(?ServerRequestInterface $request): ResponseInterface;
    abstract function addPro(?ServerRequestInterface $request): ResponseInterface;
}