<?php

namespace App\Scenes\Teams;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Components\EntityManager\TeamsEntityManager\TeamsEntityManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Routers\Router;
use App\Scenes\Teams\ViewModel\BATeamsViewModel;
use App\Scenes\Teams\ViewModel\TeamsViewModel;
use function DI\create;
use function DI\get;

final class TeamsAssembly extends DiAssembly {

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $container = $router->getContainer();
        $this->builder->addDefinitions([
            TeamsViewModel::class => create(BATeamsViewModel::class)
                ->constructor(
                    $container->get(TeamsEntityManager::class),
                    $container->get(ValidatorManager::class)
                ),
            TeamsCoordinator::class => create(BATeamsCoordinator::class)
                ->constructor(get(TeamsViewModel::class))
        ]);

        parent::injectDependenciesToRouter($router);
    }
}