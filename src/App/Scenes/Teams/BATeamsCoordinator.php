<?php

namespace App\Scenes\Teams;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class BATeamsCoordinator extends TeamsCoordinator {

    // MARK: - TeamsCoordinator methods

    public function addTeam(?ServerRequestInterface $request): ResponseInterface
    {
        return $this->teamsView->addTeam($request);
    }
}