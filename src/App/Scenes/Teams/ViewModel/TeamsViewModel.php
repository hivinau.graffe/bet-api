<?php

namespace App\Scenes\Teams\ViewModel;

use App\Foundation\Repositories\TeamsRepository\Models\Team;

interface TeamsViewModel {
    function addTeamName(?string $name,
                         ?string $image,
                         ?string $backgroundColor,
                         ?string $primaryColor,
                         ?string $secondaryColor,
                         ?string $detailColor): ?Team;
}