<?php

namespace App\Scenes\Teams\ViewModel;

use App\Foundation\Components\EntityManager\TeamsEntityManager\TeamsEntityManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Components\ValidatorManager\Validators\ImageValidator;
use App\Foundation\Components\ValidatorManager\Validators\RegularValidator;
use App\Foundation\Repositories\TeamsRepository\Models\BATeam;
use App\Foundation\Repositories\TeamsRepository\Models\Team;

final class BATeamsViewModel implements TeamsViewModel {

    // MARK: - Constants

    private const MIN_STRING_LENGTH = 2;

    // MARK: - Private properties

    /**
     * @var TeamsEntityManager
     */
    private $entityManager;

    /**
     * @var ValidatorManager
     */
    private $validatorManager;

    // MARK: - Init

    public function __construct(TeamsEntityManager $entityManager,
                                ValidatorManager $validatorManager)
    {
        $this->entityManager = $entityManager;
        $this->validatorManager = $validatorManager;

        $this->setupValidators();
    }

    // MARK: - TeamsViewModel methods

    public function addTeamName(?string $name,
                                ?string $image,
                                ?string $backgroundColor,
                                ?string $primaryColor,
                                ?string $secondaryColor,
                                ?string $detailColor): ?Team
    {
        if(!$this->isContentValid($name) ||
            !$this->isContentValid($image) ||
            !$this->isContentValid($backgroundColor) ||
            !$this->isContentValid($primaryColor) ||
            !$this->isContentValid($secondaryColor) ||
            !$this->isContentValid($detailColor)) {
            return null;
        }

        $team = (new BATeam())
            ->setName($name)
            ->setImage($image)
            ->setImageBackgroundColor($backgroundColor)
            ->setImagePrimaryColor($primaryColor)
            ->setImageSecondaryColor($secondaryColor)
            ->setImageDetailColor($detailColor);

        return $this->entityManager->addTeam($team);
    }

    // MARK: - Private methods

    private function setupValidators() {
        $this->validatorManager
            ->addValidator(new ImageValidator())
            ->addValidator(new RegularValidator(self::MIN_STRING_LENGTH));
    }

    private function isContentValid(?string $content): bool
    {
        return is_string($content) && $this->validatorManager->validate($content);
    }
}