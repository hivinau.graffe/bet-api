<?php

namespace App\Scenes\Teams\View;

use App\Scenes\Teams\ViewModel\TeamsViewModel;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Response;

final class BATeamsView  {

    // MARK: - Constants

    private const IMAGE = 'image';
    private const NAME = 'name';
    private const BACKGROUND_COLOR = 'background_color';
    private const PRIMARY_COLOR = 'primary_color';
    private const SECONDARY_COLOR = 'secondary_color';
    private const DETAIL_COLOR = 'detail_color';

    // MARK: - Private properties

    /**
     * @var TeamsViewModel
     */
    private $teamsViewModel;

    // MARK: - Init

    public function __construct(TeamsViewModel $teamsViewModel)
    {
        $this->teamsViewModel = $teamsViewModel;
    }

    // MARK: - Public methods

    public function addTeam(ServerRequestInterface $request): ResponseInterface {
        $response = new Response();
        $parameters = $request->getParsedBody();

        $team = $this->teamsViewModel->addTeamName(
            $parameters[self::NAME],
            $parameters[self::IMAGE],
            $parameters[self::BACKGROUND_COLOR],
            $parameters[self::PRIMARY_COLOR],
            $parameters[self::SECONDARY_COLOR],
            $parameters[self::DETAIL_COLOR]
        );

        if(is_null($team)) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
        }

        $response->getBody()->write(json_encode($team));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(StatusCodeInterface::STATUS_CREATED);
    }
}