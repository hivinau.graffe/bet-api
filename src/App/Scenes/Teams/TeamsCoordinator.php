<?php

namespace App\Scenes\Teams;

use App\Foundation\Coordinators\Coordinator;
use App\Foundation\Routers\Router;
use App\Scenes\Teams\View\BATeamsView;
use App\Scenes\Teams\ViewModel\TeamsViewModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

abstract class TeamsCoordinator implements Coordinator {

    // MARK: - Protected properties

    /**
     * @var BATeamsView
     */
    protected $teamsView;

    // MARK: - Init

    public function __construct(TeamsViewModel $teamsViewModel)
    {
        $this->teamsView = new BATeamsView($teamsViewModel);
    }

    // MARK: - Coordinator methods

    public function startRouter(Router $router): void
    {
        $router->post('/teams/add', [$this, 'addTeam']);
    }

    // MARK: - Abstract methods

    abstract function addTeam(?ServerRequestInterface $request): ResponseInterface;
}