<?php

namespace App\Scenes\Players;

use App\Foundation\Coordinators\Coordinator;
use App\Foundation\Routers\Router;
use App\Scenes\Bets\View\BABetsView;
use App\Scenes\Bets\ViewModel\BetsViewModel;
use App\Scenes\Players\View\BAPlayersView;
use App\Scenes\Players\ViewModel\PlayersViewModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

abstract class PlayersCoordinator implements Coordinator {

    // MARK: - Protected properties

    /**
     * @var BAPlayersView
     */
    protected $playersView;

    // MARK: - Init

    public function __construct(PlayersViewModel $viewModel)
    {
        $this->playersView = new BAPlayersView($viewModel);
    }

    // MARK: - Coordinator methods

    public function startRouter(Router $router): void
    {
        $router->post('/players/add', [$this, 'addPlayer']);
    }

    // MARK: - Abstract methods

    abstract function addPlayer(?ServerRequestInterface $request): ResponseInterface;
}