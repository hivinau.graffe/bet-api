<?php

namespace App\Scenes\Players\View;

use App\Scenes\Players\ViewModel\PlayersViewModel;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Response;

final class BAPlayersView  {

    // MARK: - Constants

    private const TOKEN = 'token';

    // MARK: - Private properties
    /**
     * @var PlayersViewModel
     */
    private $playersViewModel;

    // MARK: - Init

    public function __construct(PlayersViewModel $playersViewModel)
    {
        $this->playersViewModel = $playersViewModel;
    }

    // MARK: - Public methods

    public function addPlayer(ServerRequestInterface $request): ResponseInterface {
        $response = new Response();
        $parameters = $request->getParsedBody();

        $player = $this->playersViewModel->addPlayerToken($parameters[self::TOKEN]);

        if(is_null($player)) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
        }

        $response->getBody()->write(json_encode($player));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(StatusCodeInterface::STATUS_CREATED);
    }
}