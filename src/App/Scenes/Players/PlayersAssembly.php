<?php

namespace App\Scenes\Players;

use App\Foundation\Assemblies\DiAssembly;
use App\Foundation\Components\EntityManager\PlayersEntityManager\PlayersEntityManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Routers\Router;
use App\Scenes\Players\ViewModel\BAPlayersViewModel;
use App\Scenes\Players\ViewModel\PlayersViewModel;
use function DI\create;
use function DI\get;

final class PlayersAssembly extends DiAssembly {

    // MARK: - DiAssembly methods

    public function injectDependenciesToRouter(Router $router): void
    {
        $container = $router->getContainer();
        $this->builder->addDefinitions([
            PlayersViewModel::class => create(BAPlayersViewModel::class)
                ->constructor(
                    $container->get(PlayersEntityManager::class),
                    $container->get(ValidatorManager::class)
                ),
            PlayersCoordinator::class => create(BAPlayersCoordinator::class)
                ->constructor(get(PlayersViewModel::class))
        ]);

        parent::injectDependenciesToRouter($router);
    }
}