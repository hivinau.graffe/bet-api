<?php

namespace App\Scenes\Players;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class BAPlayersCoordinator extends PlayersCoordinator {

    // MARK: - PlayersCoordinator methods

    public function addPlayer(?ServerRequestInterface $request): ResponseInterface
    {
        return $this->playersView->addPlayer($request);
    }
}