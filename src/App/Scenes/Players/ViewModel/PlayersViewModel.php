<?php

namespace App\Scenes\Players\ViewModel;

use App\Foundation\Repositories\PlayersRepository\Models\Player;

interface PlayersViewModel {
    function addPlayerToken(?string $token): ?Player;
}