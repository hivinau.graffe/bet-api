<?php

namespace App\Scenes\Players\ViewModel;

use App\Foundation\Components\EntityManager\PlayersEntityManager\PlayersEntityManager;
use App\Foundation\Components\ValidatorManager\ValidatorManager;
use App\Foundation\Components\ValidatorManager\Validators\RegularValidator;
use App\Foundation\Repositories\PlayersRepository\Models\BAPlayer;
use App\Foundation\Repositories\PlayersRepository\Models\Player;

final class BAPlayersViewModel implements PlayersViewModel {

    // MARK: - Constants

    private const MIN_STRING_LENGTH = 3;

    // MARK: - Private properties

    /**
     * @var PlayersEntityManager
     */
    private $entityManager;

    /**
     * @var ValidatorManager
     */
    private $validatorManager;

    // MARK: - Init

    public function __construct(PlayersEntityManager $entityManager, ValidatorManager $validatorManager)
    {
        $this->entityManager = $entityManager;
        $this->validatorManager = $validatorManager;

        $this->setupValidators();
    }

    // MARK: - BetsViewModel methods

    public function addPlayerToken(?string $token): ?Player
    {
        if(!$this->isContentValid($token)) {
            return null;
        }

        $player = (new BAPlayer())
            ->setToken($token);

        return $this->entityManager->addPlayer($player);
    }

    // MARK: - Private methods

    private function setupValidators() {
        $this->validatorManager
            ->addValidator(new RegularValidator(self::MIN_STRING_LENGTH));
    }

    private function isContentValid(?string $content): bool
    {
        return is_string($content) && $this->validatorManager->validate($content);
    }
}