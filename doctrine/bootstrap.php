<?php

use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\Common\Cache\PhpFileCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Setup;
use Symfony\Component\Dotenv\Dotenv;

const DOCTRINE_DIRECTORY = __DIR__;
const REPOSITORIES_DIRECTORY = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'App' . DIRECTORY_SEPARATOR . 'Foundation' . DIRECTORY_SEPARATOR . 'Repositories';
const MODELS = [
    REPOSITORIES_DIRECTORY . DIRECTORY_SEPARATOR . 'BetsRepository' . DIRECTORY_SEPARATOR . 'Models',
    REPOSITORIES_DIRECTORY . DIRECTORY_SEPARATOR . 'GoalsRepository' . DIRECTORY_SEPARATOR . 'Models',
    REPOSITORIES_DIRECTORY . DIRECTORY_SEPARATOR . 'MatchesRepository' . DIRECTORY_SEPARATOR . 'Models',
    REPOSITORIES_DIRECTORY . DIRECTORY_SEPARATOR . 'PlayersRepository' . DIRECTORY_SEPARATOR . 'Models',
    REPOSITORIES_DIRECTORY . DIRECTORY_SEPARATOR . 'ProsRepository' . DIRECTORY_SEPARATOR . 'Models',
    REPOSITORIES_DIRECTORY . DIRECTORY_SEPARATOR . 'TeamsRepository' . DIRECTORY_SEPARATOR . 'Models'
];

(new Dotenv())->load(sprintf('%s/.env', dirname(__DIR__)));

$connection = [
    'driver'  => 'mysqli',
    'dbname' => $_ENV['MYSQL_DATABASE'],
    'user' => $_ENV['MYSQL_USER'],
    'password' => $_ENV['MYSQL_PASSWORD'],
    'host' => $_ENV['MYSQL_HOST']
];

$isDevMode = boolval($_ENV['IS_DEV_MODE'] ?? 0);

$config = Setup::createConfiguration($isDevMode);

try {
    $config->setMetadataDriverImpl(
        new AnnotationDriver(new AnnotationReader(), MODELS)
    );
} catch (AnnotationException $e) {
}

$cacheDirectory = sprintf('%s/cache', DOCTRINE_DIRECTORY);

$config->setMetadataCacheImpl(
    new FilesystemCache($cacheDirectory)
);

$proxyDirectory = sprintf('%s/proxy', DOCTRINE_DIRECTORY);
$config->setProxyDir($proxyDirectory);

$cache = new PhpFileCache($cacheDirectory);
$config->setQueryCacheImpl($cache);
$config->setResultCacheImpl($cache);

try {
    return EntityManager::create($connection, $config);
} catch (\Doctrine\ORM\ORMException $e) { }

return null;