#!/bin/bash

changelog__append_content () {
    local filename='CHANGELOG.md'
    local content="$@"

    local search='##\ \[WIP\]'
    local replace="## [WIP]<br><br>${content}"

    sed -i -e "s@${search}@${replace}@" ${filename}
    sed -i -e "s@\<br\>@\\n@g" ${filename}

    rm *-e

    git add ${filename}
}