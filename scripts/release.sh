#!/bin/bash

source "$PWD/scripts/git.sh"
source "$PWD/scripts/changelog.sh"
source "$PWD/scripts/composer.sh"

bump="$1"
version="$(composer__bump_version ${bump})"
message="[Prepare] - Release ${version} is ready to deploy 🚀"
last_commits="$(git__format_last_commits)"
content="## [${version}]<br><br> ${last_commits}"
current_branch=$(git rev-parse --abbrev-ref HEAD)

if [[ "$current_branch" -ne "develop" ]]
then
    git checkout "develop"
fi

changelog__append_content ${content}
composer__update_version ${version}

git commit --amend -m "${message}"
git push --force origin "develop"

git checkout "master"
git rebase "develop"
git merge "develop" --no-commit --no-ff
git push --force origin "master"

git tag ${version}
git push --tags