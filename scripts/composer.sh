#!/bin/bash

composer__update_version () {
    local filename='composer.json'
    local version="$1"

    local search='\"version\":\ \"([0-9\.]+)\"'
    local replace="\\\"version\\\":\\ \\\"${version}\\\""

    local output='output.json'

    sed -r -e "s/${search}/${replace}/" ${filename} > ${output}

    cat ${output} > ${filename}

    rm ${output}

    git add ${filename}
}

composer__bump_version () {
    local bump="$1"
    local version=$(grep 'version' composer.json | egrep -o '[[:digit:]]{1,3}\.[[:digit:]]{1,3}\.[[:digit:]]{1,3}')
    local regex='[^0-9]*\([0-9]*\)[.]\([0-9]*\)[.]\([0-9]*\)\([0-9A-Za-z-]*\)'

    local major=$(echo ${version} | sed -e "s#${regex}#\1#")
    local minor=$(echo ${version} | sed -e "s#${regex}#\2#")
    local patch=$(echo ${version} | sed -e "s#${regex}#\3#")

    case ${bump} in
        major)
            major="$((${major} + 1))"
            minor=0
            patch=0
            ;;

        minor | Moldova)
            minor="$((${minor} + 1))"
            patch=0
            ;;

        patch)
            patch="$((${patch} + 1))"
            ;;

        *)
            ;;
    esac

    echo "${major}.${minor}.${patch}"
}