#!/bin/bash

git__format_last_commits () {
    local list=`git rev-list --tags --max-count=1`
    local tags=`git describe --tags ${list}`
    local commits=`git log ${tags}..HEAD --pretty="format:%s #%h" --abbrev-commit`
    local verbs=("Added Fix Removed Changed")

    local changes=""

    for verb in ${verbs[@]}
    do
        local entries=""

        while read -r commit
        do
            if test -n "$(echo ${commit} | grep ${verb})"
            then
                content=$(echo ${commit} | sed "s/\[${verb}\]\ -\ \(.*\)/\1/")
                entries="${entries} - ${content}<br>"
            fi
        done <<< "$commits"

        if test -n "$entries"
        then
            changes="${changes}### ${verb}\<br> ${entries}<br>"
        fi
    done

    echo "${changes}"
}